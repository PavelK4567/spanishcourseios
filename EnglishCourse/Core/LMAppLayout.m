//  AppLayout.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMAppLayout.h"
#import "UIView+Extras.h"


@implementation AppLayout

+ (void)place:(UIView *)view1 below:(UIView *)view2 padding:(CGFloat)padding {
  [view1 placeBelowView:view2 withPadding:padding];
}

+ (void)place:(UIView *)view1 above:(UIView *)view2 padding:(CGFloat)padding {
  [view1 placeAboveView:view2 withPadding:padding];
}

+ (void)place:(UIView *)view1 rightOf:(UIView *)view2 padding:(CGFloat)padding {
  [view1 placeRightFromView:view1 withPadding:padding];
}

+ (void)placeLabel:(UILabel *)label below:(UIView *)view padding:(CGFloat)padding {
  [(UIView *)label placeBelowView:view withPadding:padding];
  [AppLayout resize:label maxSize:(CGSize) { label.width, MAXFLOAT }];
}

+ (void)placeLabel:(UILabel *)label rightOf:(UIView *)view padding:(CGFloat)padding {
  [(UIView *)label placeRightFromView:view withPadding:padding];
  [AppLayout resize:label];
}

+ (void)resize:(UIView *)parent toFit:(UIView *)view padding:(CGFloat)padding {
  parent.frame = (CGRect) { parent.origin, { parent.width, view.height + view.y + padding } };
  [parent setNeedsDisplay];
}

+ (void)resize:(UILabel *)label {
  [AppLayout resize:label maxSize:(CGSize) { MAXFLOAT, MAXFLOAT }];
}

+ (void)resize:(UILabel *)label maxSize:(CGSize)size {
  CGRect labelRect = label.frame;
  CGSize labelSize = [label.text sizeWithFont:label.font constrainedToSize:size lineBreakMode:0];
  labelRect.size.width = 1 + labelSize.width;
  labelRect.size.height = 1 + labelSize.height;
  label.frame = labelRect;
}

@end
