//
//  LMParameterList.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 1/14/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

//#ifndef EnglishCourse_LMParameterList_h
//#define EnglishCourse_LMParameterList_h

#define kParam001 [[DATA_SERVICE dataModel].courseData.unitsCreditsGuest integerValue]//2
#define kParam002 [[DATA_SERVICE dataModel].courseData.unitsCreditsVivo integerValue]//100
#define kParam003 [[DATA_SERVICE dataModel].courseData.unitsCreditsApple integerValue]//100

//Scoring
#define kParam010 [[DATA_SERVICE dataModel].courseData.scoreFirstTime integerValue]  //10
#define kParam011 [[DATA_SERVICE dataModel].courseData.scoreSecondTime integerValue] //5
#define kParam012 [[DATA_SERVICE dataModel].courseData.scoreWrongAnswer integerValue]//2

//Course
#define kParam020 0
#define kParam021 0
#define kParam022 0

//Automatic hide/show of content elements
#define kParam030 2
#define kParam031 10
#define kParam032 0.5
#define kParam033 5
#define kParam034 0.5
#define kParam035 0.5

//Misc.
#define kParam040 [[DATA_SERVICE dataModel].courseData.unitPerformaceThreshold integerValue]//80
#define kParam041 [[DATA_SERVICE dataModel].courseData.usageTime integerValue]//70
#define kParam042 0
#define kParam043 [[DATA_SERVICE dataModel].courseData.scoreDiploma integerValue]//5000
#define kParam044 3
#define kParam045 6
#define kParam046 30
#define kParam047 10
#define kParam048 15.0
#define kParam049 7
#define kParam050 3

//#endif
