//  AppConstants.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

//#ifdef __OBJC__

/* General */
#define kSuffixSelected @"_Selected"
#define kSuffixDisabled @"_Disabled"
#define SERVER_DATETIME_FORMAT @"yyyy/MM/dd" /*2014\/04\/29*/
#define STAGING 1

/* Flurry */
#define kFlurryKey @"J4WNQQSRDHSZGG82KQSR"
#define kOnGetLoginToken @"onGetLoginToken"
#define kOnAppleSuccessfulSubscribe @"onAppleSuccessfulSubscribe"
#define kOnAppleUnsuccessfulSubscribe @"onAppleUnsuccessfulSubscribe"
#define kOnAppleSubscriptionTerms @"onAppleSubscriptionTerms"

/* Singletons */

#define AUDIO_SINGLETON 1
#define COUNTRY_SINGLETON 1
#define ITEM_SINGLETON 1
#define REQUEST_SINGLETON 1
#define API_REQUEST_SINGLETON 1
#define USER_SINGLETON 1
#define PROGRESS_SINGLETON 1
#define ANALYTICS_SINGLETON 1

/* Paths */

#define kCountriesPath @"Countries"
#define kDemoPath @"demo"

#define kPreferencesFolderPictionaryItems @"Items"
#define kPreferencesFileMapping @"mapping"
#define kPreferencesFileItems @"items"
#define kPreferencesFileTags @"tags"
#define kPreferencesFileUrls @"urls"
#define kPreferencesFileTypePlist @"plist"
#define kPreferencesFilePng @"png"
#define kPreferencesFileJpg @"jpg"
#define kPreferencesCheckedList @"CheckedList"
#define kPreferencesflag @"flag"
/* Parser */

#define kParserTagUUID @"uuid"
#define kParserItems @"items"
#define kParserTagImageThumb @"thumb"
#define kParserTagImage @"image"
#define kParserTagTextNative @"native"
#define kParserTagTextLocalized @"localized"
#define kParserTagTextTranslate @"translate"
#define kParserTagUserGenerated @"user"
#define kParserTagCanShare @"share"
#define kParserTagMSISDN @"msisdn"
#define kParserTagDefaultImage @"default"
#define kParserTagDateTaken @"date"
#define kParserTagMSISDN @"msisdn"
#define kParserTagImageOrientation @"msisdn"

#define kParserTagImage @"image"
#define kParserTagThumbnail @"thumbnail"
#define kParserTagOriginalImage @"originalImage"


/* Constants */

#define kCamfindSearchesForGuest 5
#define kCamfindSearchesForUser 25
#define kCamfindSearchesForOther 0

#define kUserStatusGuestString @"Guest"
#define kUserStatusNotRegistered @"NotRegistered"
#define kUserStatusActiveString @"Active"
#define kUserStatusToBeDeactivatedString @"ToBeCanceled"
#define kUserStatusDeactivatedString @"Cancelled"
#define kUserStatusDisabledString @"Disabled"
#define kUserStatusIAP @"IAP"

/* Preferences */

#define kUserOperatorSiteID @"siteID"
#define kUserOperatorProductID @"productID"

#define kUserMSISDN       @"msisdn"
#define kUserCode         @"code"
#define kUserDeviceId     @"device_id"
#define kSearchWords      @"words"
#define kLevelId          @"levelID"
#define kLessonId         @"lessonID"
#define kGroupId          @"groupID"
#define kSalutation       @"salutation"
#define kFirstName        @"firstName"
#define kLastName         @"lastName"
#define kEmail            @"email"
#define kResend           @"resend"
#define kPhoneModel       @"phoneModel"
#define kOsType           @"osType"
#define kSkipped          @"skipped"
#define kCorrect          @"correct"
#define kinCorrect        @"incorrect"
#define kOsVersion        @"osVersion"
#define kStartDate        @"start"
#define kEndDate          @"end"
#define kDeviceId         @"deviceID"
#define kCompleted        @"completed"
#define kGrade            @"grade"
#define kRecommendedLevel @"recommendedLevel"
#define kDateTime         @"datetime"
#define kSiteID           @"siteID"
#define kproductID        @"productID"
#define kSource           @"source"
#define kToken            @"token"

#define kUserManagerInitialized @"user_manager_initialized"
#define kUserManagerDefault @"000000000"
#define kLastValidMSISDNPath @"last_msisdn"
#define kLastValidLoginUser @"last_valid_login_user"
#define kLastValidMSISDN @"last_valid_msisdn"
#define kLastValidUniqueID @"last_valid_unique_ID"
#define kLastValidUserStatus @"last_valid_user_status"
#define kLastValidBillingDate @"last_valid_user_billing_date"
#define kLastTimeActiveUserLoginDate @"lastTimeActiveUserLogin"
#define kStartAplicationDate @"startAplicationDate"
#define kLastSaveDateDate @"lastSaveDateDate"
#define kLastValidContentVersion @"last_valid_content_version"
#define kLastValidLevelData @"last_valid_level_data"
#define kLastValidCurrentLevel @"last_valid_current_level"

#define kUserPath @"last_user"
#define kGuestPath @"last_guest"
#define kUserTags @"last_tags"
#define kGuestTags @"guest_tags"

#define kGroupID @"groupID"
#define kContentID @"contentID"
#define kUnitID @"unitID"
#define kUnitScore @"unitScore"
#define kUnitAchieved @"unitAchieved"
#define KProgress @"progress"

#define kUserOperatorSplashIos @"operatorSplash"
#define kUserOperatorLogoIos @"operatorLogo"
/* UI Constants */

#define __SELECTED__ @"_Selected"

/* Sizes */

#define kCornerRadius 2.
#define kImageNormalSize ((CGSize){ 640, 640 })
#define kImageThumbSize ((CGSize){ 200, 200 })

#define kHomeExpandedInset ((UIEdgeInsets){ 0, 0, 0, 0 })
#define kHomeCollapesedInset ((UIEdgeInsets){ -210, 0, 0, 0 })
#define kHomePhotoCellSize ((CGSize){ 310, 200 })
#define kHomePhoto480CellSize ((CGSize){ 310, 140 })
#define kHomeSearchCellSize ((CGSize){ 310, 52 })
#define kHomeSearchActiveStateCellSize ((CGSize){ 310, 72 })
#define kHomeSuggestionCellSize ((CGSize){ 310, 32 })
#define kHomeResultCellSize ((CGSize){ 310, 100 })
#define kHomeItemCellSize ((CGSize){ 100, 100 })
#define kHeaderSize ((CGSize){ 310, 1 })

#define kNavbarButtonSize ((CGSize){ 32, 32 })
#define kShortButtonSize ((CGSize){ 50, 32 })
#define kLongButtonSize ((CGSize){ 280, 32 })

/* Sounds */

#define kSoundTypeBackground @"SFX_Background"
#define kSoundTypeButtonTap @"SFX_ButtonTap"
#define kSoundTypeSwipe @"SFX_Swipe"
#define kSoundTypeError @"SFX_Error"
#define kSoundTypeSuccess @"SFX_Success"

#define kSoundTypeResults @"ButtonClickFX"
#define kSoundTypeDeletePicture @"PicDeletedFX"
#define kSoundTypeTakePicture @"TakePicFX"

#define kSoundFileType @"mp3"

/* URL */
#define kSubscriptionString @"false"
//#define kApiBaseUrl @"http://la-mark-il.com:8081/LMWeb/pictionary"
//#define kApiBaseBillingUrl @"http://la-mark-il.com:8081/LMWeb/Kantoo1000NativeApp"
//#define kApiBaseUrl @"http://kantoo.com/LMWeb/pictionary"
//#define kApiBaseBillingUrl @"http://kantoo.com/LMWeb/Kantoo1000NativeApp"
#define kApiPriceCode @"com.kfoto.weekly" //@"com.completo.montly"

//#define kApiCamFindUrl @"http://54.226.173.227:8080/PitionaryProxy/DoCamFindRequest"
//#define kApiCamFindUrl @"http://192.168.9.168:8080/PitionaryProxy/DoCamFindRequest"
//#define kApiGoogleResultsUrl @"http://54.226.173.227:8080/PitionaryProxy/DoImageSearchRequest"

#if STAGING == 0
#define kApiBaseUrl @"http://la-mark-il.com:8081/LMWeb/GroupServices"
#define kApiECBaseUrl @"http://la-mark-il.com:8081/LMWeb/EnglishCourse"
#define kApiBaseBillingUrl @"http://la-mark-il.com:8081/LMWeb/Kantoo1000NativeApp"
#define kApiNuanceUrl @"http://speech-13.la-mark-il.com:8080/SpeechProxy/EC/SpeechZip"
#define kUpdateProgressUrl @"%@/updateProgress?msisdn=%@&groupID=%@&contentID=%@&unitID=%@&unitScore=%@&unitAchieved=%@&progress=%@&deviceID=%@"
#define kSendDiplomaUrl @"%@/diploma?msisdn=%@&groupID=%@&contentID=%@&levelID=%@&salutation=%@&firstName=%@&lastName=%@&email=%@&resend=%@&deviceID=%@"
#define kDiplomaDetailsUrl @"%@/diplomaDetails?msisdn=%@&groupID=%@&contentID=%@"
#define kSessionStatisticUrl @"%@/sessionStatistic?msisdn=%@&groupID=%@&contentID=%@&deviceID=%@&start=%@&end=%@"
#define kUserProgressionUrl @"%@/userProgression?msisdn=%@&contentID=%@&phoneModel=%@&osType=%@&osVersion=%@&levelID=%@&groupID=%@&lessonID=%@&skipped=%@&correct=%@&incorrect=%@&deviceID=%@"
#define kPlacementtestUrl @"%@/placementTest?msisdn=%@&contentID=%@&groupID=%@&phoneModel=%@&osType=%@&osVersion=%@&completed=%@&grade=%@&recommendedLevel=%@&dateTime=%@&deviceID=%@"
#define kCountryUrl @"%@/initialization?groupID=%@&contentID=%@"

#define kCallCourseInfo @"http://cdn.kantoo.com/ecbTest/%@/course.json"
#define kCallDownloadCourseData @"http://cdn.kantoo.com/ecbTest/%@/course.zip"
#define kCallDownloadCharactersData @"http://cdn.kantoo.com/ecbTest/%@/characters.zip"
#define kCallLevelInfo @"http://cdn.kantoo.com/ecbTest/%ld/%@/level.json"
#define kCallDownloadLevelData @"http://cdn.kantoo.com/ecbTest/%ld/%@/levelData.zip"
#define kCallDownloadInstnaceData @"http://cdn.kantoo.com/ecbTest/%ld/%@/%ld.zip"


#endif

#if STAGING == 1
#define kApiBaseUrl @"http://kantoo.com/LMWeb/GroupServices"
#define kApiECBaseUrl @"http://kantoo.com/LMWeb/EnglishCourse"
#define kApiBaseBillingUrl @"http://kantoo.com/LMWeb/Kantoo1000NativeApp"
//#define kApiNuanceUrl @"http://192.168.9.168:8080/SpeechProxy/EC/SpeechZip"
#define kApiNuanceUrl @"http://sr-1.kantoo.com:8001/SpeechProxy/ES/SpeechZip"
#define kUpdateProgressUrl @"%@/updateProgress?msisdn=%@&groupID=%@&contentID=%@&unitID=%@&unitScore=%@&unitAchieved=%@&progress=%@"
#define kSendDiplomaUrl @"%@/diploma?msisdn=%@&groupID=%@&contentID=%@&levelID=%@&salutation=%@&firstName=%@&lastName=%@&email=%@&resend=%@"
#define kDiplomaDetailsUrl @"%@/diplomaDetails?msisdn=%@&groupID=%@&contentID=%@"
#define kSessionStatisticUrl @"%@/sessionStatistic?msisdn=%@&groupID=%@&contentID=%@&deviceID=%@&start=%@&end=%@"
#define kUserProgressionUrl @"%@/userProgression?msisdn=%@&contentID=%@&phoneModel=%@&osType=%@&osVersion=%@&levelID=%@&groupID=%@&lessonID=%@&skipped=%@&correct=%@&incorrect=%@"
#define kPlacementtestUrl @"%@/placementTest?msisdn=%@&contentID=%@&groupID=%@&phoneModel=%@&osType=%@&osVersion=%@&completed=%@&grade=%@&recommendedLevel=%@&dateTime=%@"
#define kCountryUrl @"%@/initialization?groupID=%@&contentID=%@"

#define kCallCourseInfo @"http://cdn.kantoo.com/ecb/%@/course.json"
#define kCallDownloadCourseData @"http://cdn.kantoo.com/ecb/%@/course.zip"
#define kCallDownloadCharactersData @"http://cdn.kantoo.com/ecb/%@/characters.zip"
#define kCallLevelInfo @"http://cdn.kantoo.com/ecb/%ld/%@/level.json"
#define kCallDownloadLevelData @"http://cdn.kantoo.com/ecb/%ld/%@/levelData.zip"
#define kCallDownloadInstnaceData @"http://cdn.kantoo.com/ecb/%ld/%@/%ld.zip"

#endif

/* Purchases */

#define kMonthlyPurchaseId @"com.lamark.EnglishCourseSpanish.monthly"
#define kMonthlyPurchaseIdShort @"com.lamark.EnglishCourseSpanish.monthly"
#define kKeywordPurchaseIdShort @"ios"

/* API */
/*#define kGroupIDNumber @"1"
#define kContentIDNumber @"1"*/
#define kApiStatus @"status"
#define kApiUserStatus @"userStatus"
#define kApiNextBillingDate @"nextBillingDate"//@"validUntil"
#define kApiLevelData @"levelData"
#define kApiUserCurrentLevel @"currentLevel"
#define kApiUserContentVersion @"contentVersion"


#define kApiTokenCode @"code"
#define kApiToken @"token"
#define kApiUserUniqueID @"userUniqueID"

#define kApiUserNumberOfCredits @"numberOfCredits"


/*  Photos */
#define kPathForCameraPhoto @"path"

/*  Background Notification */
#define kApplicationDidEnterBackground @"applicationDidEnterBackgroundNotification"
#define kApplicationWillEnterForeground @"applicationWillEnterForegroundNotification"

/* UI constants */
#define kButtomLinePadding 2.0

/* First View Controller */
#define kViewsPadding 5.0
//#endif

/* Server Error codes */
#define ERROR_CODE_SUCCESS 0


/* Achievements codes */
#define kLevelDiploma @"LevelDiploma"
#define kModulSertifikat @"ModulSertifikat"
