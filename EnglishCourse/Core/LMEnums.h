//
//  LMEnums.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/26/14.
//  Copyright (c) 2014 LaMark. All rights reserved..
//

#ifndef EnglishCourse_LMEnums_h
#define EnglishCourse_LMEnums_h

typedef enum {
    LEFT,
    RIGHT
} IconPosition;

typedef enum {
    UserStatisticType = 0,
    LevelStatisticType = 1,
    SectionStatisticType = 2,
    UnitStatisticType = 3,
    InstanceStatisticType = 4,
    PageStatisticType = 5
} StatisticType;

typedef enum {
    SIMPLE_CHAT,
    PROGRESSIVE_CHAT
} ChatType;

typedef enum {
    REQUEST_LEVEL,
    REQUEST_LEVEL_DATA,
    REQUEST_INSTANCE,
    REQUEST_COURSE_INFO,
    REQUEST_COURSE,
    REQUEST_CHARACTERS
} RequestType;

typedef enum {
    BUBBLE_CHAT,
    BUBBLE_SUMMARY
} BubbleType;

#endif
