//  UIViewController+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface UIViewController (Extras)

- (void)presentModalViewController:(UIViewController *)modalViewController withPushDirection:(NSString *)direction;
- (void)dismissModalViewControllerWithPushDirection:(NSString *)direction;

- (BOOL)isModal;
- (void)autorotate;

@end
