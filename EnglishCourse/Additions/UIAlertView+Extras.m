//  UIAlertView+Extras.m
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "UIAlertView+Extras.h"
#import <objc/runtime.h>


#define ALERT_ERROR @"Alert.ErrorTitle"
#define ALERT_ERROR_MSG @"Alert.ErrorMessage"
#define ALERT_EXCEPTION @"Alert.ExceptionTitle"
#define ALERT_EXCEPTION_MSG @"Alert.ExceptionMessage"
#define ALERT_CONFIGMISSING @"Alert.ConfigMissingTitle"
#define ALERT_CONFIGMISSING_MSG @"Alert.ConfigMissingMessage"
#define ALERT_NOCONNECTION @"Alert.NoConnectionTitle"
#define ALERT_NOCONNECTION_MSG @"Alert.NoConnectionMessage"
#define ALERT_SERVERUNREACHABLE @"Alert.ServerUnreachableTitle"
#define ALERT_SERVERUNREACHABLE_MSG @"Alert.ServerUnreachableMessage"
#define ALERT_NOLOCATION @"Alert.NoLocationTitle"
#define ALERT_NOLOCATION_MSG @"Alert.NoLocationMessage"
#define ALERT_LOCATIONBATTERY @"Alert.LocationBatteryTitle"
#define ALERT_LOCATIONBATTERY_MSG @"Alert.LocationBatteryMessage"
#define ALERT_NOCALLS @"Alert.CantCallPhoneTitle"
#define ALERT_NOCALLS_MSG @"Alert.CantCallPhoneMessage"
#define ALERT_NOSMS @"Alert.CantSendSMSTitle"
#define ALERT_NOSMS_MSG @"Alert.CantSendSMSMessage"
#define ALERT_NOEMAIL @"Alert.CantSendMailTitle"
#define ALERT_NOEMAIL_MSG @"Alert.CantSendMailMessage"
#define ALERT_WRONGCREDENTIALS @"Alert.WrongCredentialsTitle"
#define ALERT_WRONGCREDENTIALS_MSG @"Alert.WrongCredentialsMessage"
#define ALERT_MISSINGFIELDS @"Alert.MissingFieldsTitle"
#define ALERT_MISSINGFIELDS_MSG @"Alert.MissingFieldsMessage"
#define ALERT_MISSINGUSER @"Alert.MissingUserTitle"
#define ALERT_MISSINGUSER_MSG @"Alert.MissingUserMessage"

#define ALERT_TYPE_SUCCESS @"Alert.Success"
#define ALERT_TYPE_SUCCESS_MSG @"Alert.Success.Msg"

#define ALERT_TYPE_NO_TITLE @"Alert.NoTitle"
#define ALERT_TYPE_NO_TITLE_MSG @"Alert.NoTitle.Msg"


static char DISMISS_IDENTIFER;
static char CANCEL_IDENTIFER;


@implementation UIAlertView (Extras)

+ (void)alertWithCause:(EAlertViewCause)cause {
	[UIAlertView alertWithCause:cause success:0 failure:0];
}

+ (void)alertWithCause:(EAlertViewCause)cause success:(void (^)(int))success failure:(void (^)(void))failure {
	NSString *__title = 0;
	NSString *__message = 0;
	EAlertViewType __type = kAlertInformation;
  
  switch (cause) {
    case kAlertViewCameraDisabled:
      __title = @"";
      __message = Localized(@"T663");
    case kAlertViewNoGusets:
      __title = @"";
      __message = Localized(@"T662");
      break;
    case kAlertException:
      __title = Localized(ALERT_EXCEPTION);
      __message = Localized(ALERT_EXCEPTION_MSG);
      break;
    case kAlertDisabledUser:
      __title = Localized(@"T013");
      __message = Localized(@"T011");
      break;
      
    case kAlertPhoneNumberNotValid:
      __title = @"";
      __message = Localized(@"T006.1");
      break;
    case kAlertPhoneNumberShorterThan8Digit:
      __title = @"";
      __message = Localized(@"T006");
      break;
    case kAlertNotRegisteredUser:
      __title = @"";
      __message = Localized(@"T008");
      break;
    case kAlertCanceledUser:
      __title = @"";
      __message = Localized(@"T010");
      break;
    case kAlertNoNameMessage:
      __title = @"";
      __message = Localized(@"T001.1");
      break;
    case kAlertNoMailMessage:
      __title = @"";
      __message = Localized(@"T001.2");
      break;
    case kAlertCanceledNativeUser:
      __title = @"";
      __message = [NSString stringWithFormat:Localized(@"T010B"),[USER_MANAGER takeStringFromDate]];
      break;
    case kAlertNoInternetConnection:
      if([APP_DELEGATE NoInternetConnectionPopUpShow])
        return;
      APP_DELEGATE.NoInternetConnectionPopUpShow = YES;
      __title = @"";
      __message = Localized(@"T303");
      break;
    case kAlertNoInternetConnectionForFramePresentation:
      __title = @"";
      __message = Localized(@"T130");
      break;
    case kAlertCodeNotValid:
      __title = @"";
      __message = Localized(@"T021");
      break;
      
    case kAlertCodeNotMatchMSISDN:
      __title = @"";
      __message = Localized(@"T022");
      break;
    
    case kAlertOutOfStorage:
      __title = @"";
      __message = Localized(@"T300");
      break;

  case kAlertNoConnection:
      if([APP_DELEGATE NoInternetConnectionPopUpShow])
        return;
      APP_DELEGATE.NoInternetConnectionPopUpShow = YES;
      __title = @"";
      __message = Localized(@"T302");
      break;
      
  case kAlertServerUnreachable:
      if([APP_DELEGATE ServerUnreachablePopUpShow])
        return;
      APP_DELEGATE.ServerUnreachablePopUpShow = YES;
      __title = @"";
      __message = Localized(@"T304");
      break;
    case kAlertCamFindAPITechnicalError:
      __title = @"";
      __message = Localized(@"T125");
      break;
    case kAlertNoResultFromCamFindMessage:
      __title = @"";
      __message = Localized(@"T124");
      break;
    case kAlertLockedLesson:
      __title = @"";
      __message = Localized(@"T460");
      break;
    case kAlertLockedLessonAsGuest:
          __title = @"";
          __message = Localized(@"T462");
          break;
    case kAlertNoMoreCreditsForActiveUser:
      __title = @"";
      __message = Localized(@"T464");
      break;
      case KAlertDiplomaSendOK:
          __title = @"";
          __message = Localized(@"T598");
          break;
      case KAlertInstanceNotDownloadedYet:
          __title = @"";
          __message = Localized(@"T461");
          break;
    default:
      __title = Localized(ALERT_ERROR);
      __message = Localized(ALERT_ERROR_MSG);
      break;
	}
  
	[UIAlertView createAlertViewTitle:__title message:__message type:__type success:success failure:failure];
}

+ (void)createAlertViewTitle:(NSString *)title
                     message:(NSString *)message
                        type:(EAlertViewType)type
                     success:(void (^)(int))success
                     failure:(void (^)(void))failure {
	UIAlertView *alert = 0;
	if (type == kAlertApprove) {
		alert = [UIAlertView alertViewWithTitle:title
		                                message:message
		                      cancelButtonTitle:Localized(@"Alert.Cancel")
		                      otherButtonTitles:@[Localized(@"Alert.OK")]
		                              onDismiss: ^(int buttonIndex) {
                                    if (buttonIndex == -1 && success)
                                      success(buttonIndex);
                                    else if (buttonIndex == 0 && failure)
                                      failure();
                                  }
             
		                               onCancel: ^{ failure(); }];
	}
	else {
    alert = [UIAlertView alertViewWithTitle:title message:message cancelButtonTitle:Localized(@"T053") otherButtonTitles:nil
                                  onDismiss: ^(int buttonIndex) {
                                    //DLog(@"@@");
                                  }
             
                                   onCancel: ^{
                                     APP_DELEGATE.NoInternetConnectionPopUpShow = NO;
                                     APP_DELEGATE.ServerUnreachablePopUpShow = NO;
                                   }];
	}
  
	[alert show];
}

#pragma mark - MKBlockAdditions

@dynamic cancelBlock;
@dynamic dismissBlock;

- (void)setDismissBlock:(DismissBlock)dismissBlock {
	objc_setAssociatedObject(self, &DISMISS_IDENTIFER, dismissBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (DismissBlock)dismissBlock {
	return objc_getAssociatedObject(self, &DISMISS_IDENTIFER);
}

- (void)setCancelBlock:(CancelBlock)cancelBlock {
	objc_setAssociatedObject(self, &CANCEL_IDENTIFER, cancelBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CancelBlock)cancelBlock {
	return objc_getAssociatedObject(self, &CANCEL_IDENTIFER);
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title
                            message:(NSString *)message
                  cancelButtonTitle:(NSString *)cancelButtonTitle
                  otherButtonTitles:(NSArray *)otherButtons
                          onDismiss:(DismissBlock)dismissed
                           onCancel:(CancelBlock)cancelled {
	UIAlertView *alert =
  [[UIAlertView alloc] initWithTitle:title message:message delegate:[self class] cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
  
	[alert setDismissBlock:dismissed];
	[alert setCancelBlock:cancelled];
  
	for (NSString *buttonTitle in otherButtons) {
		[alert addButtonWithTitle:buttonTitle];
	}
	return alert;
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message {
	return [UIAlertView alertViewWithTitle:title message:message cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")];
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
	return alert;
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == [alertView cancelButtonIndex]) {
		if (alertView.cancelBlock) {
			alertView.cancelBlock();
		}
	}
	else {
		if (alertView.dismissBlock) {
			alertView.dismissBlock(buttonIndex - 1); // cancel button is button 0
		}
	}
}

@end
