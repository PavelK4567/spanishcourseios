//
//  BaseEntity.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface BaseEntity : NSObject<NSCoding>

@end
