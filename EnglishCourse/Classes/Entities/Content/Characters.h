//
//  Characters.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/19/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Character.h"

@interface Characters : BaseEntity<NSCoding>

@property (nonatomic, strong) NSArray *charactersArray;

+ (Characters *)buildCharacters:(NSDictionary *)dataDict;

- (void)replaceCharactersIfDifferent:(NSDictionary *)dataDict;

@end
