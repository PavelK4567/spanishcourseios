//
//  CourseProperties.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Utilities.h"

@interface CourseProperties : BaseEntity

@property (nonatomic, copy) NSString  *instructionSound;
@property (nonatomic, copy) NSString  *name;
@property (nonatomic, assign) NSInteger mID;
@property (nonatomic, copy) NSString  *correctFeedbackSound;
@property (nonatomic, copy) NSString  *incorrectFeedbackSound;

- (void)updateResources;
- (void)removeResources;

+ (CourseProperties *)buildCourseProperties:(NSDictionary *)dataDict;

- (void)replaceIfDifferent:(NSDictionary *)dataDict;

@end
