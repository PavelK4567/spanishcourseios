//
//  ExposureSection.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "ExposureSection.h"

@implementation ExposureSection

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.instructionText = [aDecoder decodeObjectForKey:@"instructionText"];
    self.instructionSound = [aDecoder decodeObjectForKey:@"instructionSound"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.instructionText forKey:@"instructionText"];
    [aCoder encodeObject:self.instructionSound forKey:@"instructionSound"];
    
}

+ (ExposureSection *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    ExposureSection *instance = nil;
    
    if (instanceDataDict) {
        instance = [ExposureSection new];
        
        instance.instructionText = instanceDataDict[kInstructionText];
        instance.instructionSound = instanceDataDict[kInstructionSound];
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources:(ExposureSection *)tempSection
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempSection.instructionSound isEqualToString:self.instructionSound]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
}

@end
