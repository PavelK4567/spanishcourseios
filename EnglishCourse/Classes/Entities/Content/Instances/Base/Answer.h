//
//  Answer.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface Answer : BaseEntity

@property (nonatomic, assign) NSInteger orderNumber;
@property (nonatomic, assign) BOOL      isCorrect;
@property (nonatomic, copy) NSString  *text;

+ (Answer *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
