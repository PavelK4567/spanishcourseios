//
//  MultipleChoiceImagePracticeSection.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "MultipleChoiceImagePractice.h"

@interface MultipleChoiceImagePracticeSection : BaseEntity

@property (nonatomic, copy) NSString *instructionText;
@property (nonatomic, copy) NSString *instructionSound;
@property (nonatomic, strong) NSArray  *practiceSections;

+ (MultipleChoiceImagePracticeSection *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(MultipleChoiceImagePracticeSection *)tempSection;

@end
