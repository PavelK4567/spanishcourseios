//
//  MultipleChoiceImagePracticeSection.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MultipleChoiceImagePracticeSection.h"

@implementation MultipleChoiceImagePracticeSection

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.instructionText = [aDecoder decodeObjectForKey:@"instructionText"];
    self.instructionSound = [aDecoder decodeObjectForKey:@"instructionSound"];
    self.practiceSections = [aDecoder decodeObjectForKey:@"practiceSections"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.instructionText forKey:@"instructionText"];
    [aCoder encodeObject:self.instructionSound forKey:@"instructionSound"];
    [aCoder encodeObject:self.practiceSections forKey:@"practiceSections"];
    
}

+ (MultipleChoiceImagePracticeSection *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    MultipleChoiceImagePracticeSection *instance = [MultipleChoiceImagePracticeSection new];
    
    instance.instructionSound = instanceDataDict[kInstructionSound];
    instance.instructionText = instanceDataDict[kInstructionText];
    
    NSArray *practicesArray = instanceDataDict[kPractices];
    
    NSMutableArray *practicesDataObjectsArray = nil;
    
    if (practicesArray && [practicesArray count] > 0) {
        practicesDataObjectsArray = [NSMutableArray arrayWithCapacity:1];
        
        for (NSDictionary *practiceDict in practicesArray) {
            MultipleChoiceImagePractice *practice = [MultipleChoiceImagePractice createInstanceWithData:practiceDict];
            
            [practicesDataObjectsArray addObject:practice];
        }
        
        instance.practiceSections = [practicesDataObjectsArray copy];
        
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources:(MultipleChoiceImagePracticeSection *)tempSection
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempSection.instructionSound isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    NSArray *practiceArray = self.practiceSections;
    
    NSArray *tempPracticesArray = tempSection.practiceSections;
    
    for (int index = 0; index < [practiceArray count]; index++) {
        
        MultipleChoiceImagePractice *practice = practiceArray[index];
        MultipleChoiceImagePractice *tempPractice = tempPracticesArray[index];
        
        [practice removeDifferentInstanceResources:tempPractice];
        
    }

}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    NSArray *practiceArray = self.practiceSections;
    
    NSArray *practicesArrayJSON = instanceDict[kPractices];
    
    for (int index = 0; index < [practiceArray count]; index++) {
        
        MultipleChoiceImagePractice *practice = practiceArray[index];
        NSDictionary *segmentDict = practicesArrayJSON[index];
        
        [practice removeUnusedResources:segmentDict];
        
    }
    
}

@end
