//
//  MultipleChoiceImage.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "ExposureSection.h"
#import "MultipleChoiceImagePracticeSection.h"
#import "WordMultipleChoiceImage.h"

@interface MultipleChoiceImage : BaseInstance



@property (nonatomic, strong) NSArray         *wordMultipleChoiceImagesArray;
@property (nonatomic, strong) ExposureSection *exposureSection;
@property (nonatomic, strong) MultipleChoiceImagePracticeSection *practiceSection;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
