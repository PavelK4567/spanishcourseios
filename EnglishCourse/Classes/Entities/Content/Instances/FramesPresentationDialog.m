//
//  FramesPresentationDialog.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "FramesPresentationDialog.h"

@implementation FramesPresentationDialog

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    self.sectionTitle = [aDecoder decodeObjectForKey:@"sectionTitle"];
    self.sectionText = [aDecoder decodeObjectForKey:@"sectionText"];
    self.segmentsArray = [aDecoder decodeObjectForKey:@"segments"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    
    self.longVersion = [aDecoder decodeBoolForKey:@"longVersion"];
    
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.sectionTitle forKey:@"sectionTitle"];
    [aCoder encodeObject:self.sectionText forKey:@"sectionText"];
    [aCoder encodeObject:self.segmentsArray forKey:@"segments"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    
    [aCoder encodeBool:self.longVersion forKey:@"longVersion"];
    
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (FramesPresentationDialog *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    FramesPresentationDialog *instance = nil;
    
    if (instanceDataDict) {
        instance = [FramesPresentationDialog new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        instance.instructionText = instanceDataDict[kInstructionText];
        instance.instructionSound = instanceDataDict[kInstructionSound];
        instance.sectionText = instanceDataDict[kSectionText];
        instance.sectionTitle = instanceDataDict[kSectionTitle];
        
        NSArray *segmentsArray = instanceDataDict[kSegments];
        
        NSMutableArray *segmentsDataArray = nil;
        
        if (segmentsArray && [segmentsArray count] > 0) {
            segmentsDataArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *segmentDict in segmentsArray) {
                SegmentFramesPresentation *segmentFramePresentation = [SegmentFramesPresentation createInstanceWithData:segmentDict];
                
                [segmentsDataArray addObject:segmentFramePresentation];
            }
            
            instance.segmentsArray = [segmentsDataArray copy];
        }
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.longVersion = [instanceDataDict[kLongVersion] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((FramesPresentationDialog *)self.tempInstance).instructionSound isEqualToString:self.instructionSound]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    NSArray *tempSegmentsArray = ((FramesPresentationDialog *)self.tempInstance).segmentsArray;
    
    NSArray *segmentsArray = self.segmentsArray;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
        
        SegmentFramesPresentation *segmentFramePresentation = segmentsArray[index];
        SegmentFramesPresentation *tempSegment = tempSegmentsArray[index];
        
        [segmentFramePresentation removeDifferentInstanceResources:tempSegment];
        
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    NSArray *segmentsArrayJSON = instanceDict[kSegments];
    
    NSArray *segmentsArray = self.segmentsArray;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
     
        SegmentFramesPresentation *segmentFramePresentation = segmentsArray[index];
        NSDictionary *segmentDict = segmentsArrayJSON[index];
        
        [segmentFramePresentation removeUnusedResources:segmentDict];
        
    }
    
}

- (void)replaceWithInstance:(FramesPresentationDialog *)instance {
    [super replaceWithInstance:instance];

    self.title = instance.title;
    self.sectionTitle = instance.sectionTitle;
    self.sectionText = instance.sectionText;
    self.segmentsArray = [instance.segmentsArray copy];
    self.isInitial = instance.isInitial;
    self.longVersion = instance.longVersion;
}

@end
