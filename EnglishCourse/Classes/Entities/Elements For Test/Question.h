//
//  Question.h
//  K1000
//
//  Created by Action-Item on 5/5/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Element.h"
#import "TestAnswer.h"

@interface Question : Element <NSCopying>

- (int)correctAnswerIndex;

@property(nonatomic, unsafe_unretained) int userSelectedAnswer;
@property(nonatomic, strong) NSString *question;
@property(nonatomic, strong) NSString *picture;
@property(nonatomic, strong) NSURL *imageURL;
@property(nonatomic, strong) NSMutableArray *answers;

@end
