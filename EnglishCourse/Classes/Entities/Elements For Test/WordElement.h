//
//  WordElement.h
//  K1000
//
//  Created by Action Item on 4/24/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Element.h"

@interface WordElement : Element

@property(nonatomic, unsafe_unretained) int order;
@property(nonatomic, strong) NSString *englishWord;
@property(nonatomic, strong) NSString *portugeseWord;
@property(nonatomic, strong) NSString *exampleText;

@end
