//
//  NetworkService.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/7/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Utilities.h"
#import "BaseEntity.h"
#import "BaseInstance.h"

#import "InstanceFactory.h"
#import "CourseStructure.h"
#import "CourseProperties.h"

#import "Characters.h"
#import "Character.h"

#import "Level.h"
#import "InstanceSignature.h"

#import "LevelData.h"

@protocol NetworkServiceDelegate <NSObject>

- (void)returnCourseInfoWithError:(BOOL)hasError message:(NSString *)message courseInfo:(BaseEntity *)courseInfo;
- (void)returnCourseDataWithError:(BOOL)hasError message:(NSString *)message courseData:(BaseEntity *)courseData;
- (void)returnCharactersInfoWithError:(BOOL)hasError message:(NSString *)message charactersInfo:(BaseEntity *)charactersInfo;
- (void)returnLevelsInfoWithError:(BOOL)hasError message:(NSString *)message levelsInfo:(BaseEntity *)levelsInfo;
- (void)returnLevelsDataWithError:(BOOL)hasError message:(NSString *)message levelsData:(BaseEntity *)levelsData;
- (void)returnInstnaceDataWithError:(BOOL)hasError message:(NSString *)message instnaceData:(BaseEntity *)instnaceData;



@end

@interface NetworkService : NSObject

+ (NetworkService *)sharedInstance;

- (void)executeCallForCourseInfoWithContentVersion:(NSString *)contentVersion delegate:(id<NetworkServiceDelegate>)delegate;
- (void)executeCallForCourseDataDownloadWithContentVersion:(NSString *)contentVersion delegate:(id<NetworkServiceDelegate>)delegate;
- (void)executeCallForCharactersInfoDownload:(NSString *)contentVersion delegate:(id<NetworkServiceDelegate>)delegate;
- (void)executeCallForLevelInfo:(NSString *)contentVersion orderNumber:(NSInteger)orderNumber delegate:(id<NetworkServiceDelegate>)delegate;
- (void)executeCallForLevelDataDownloadWithContentVersion:(NSString *)contentVersion orderNumber:(NSInteger)orderNumber delegate:(id<NetworkServiceDelegate>)delegate;
- (void)executeCallForInstanceDataDownloadWithOrderNumber:(NSInteger)orderNumber levelVersion:(NSInteger)levelVersion instanceOrderID:(NSInteger)instanceOrderID delegate:(id<NetworkServiceDelegate>)delegate;

@end
