//
//  UIApplication+UIID.m
//  UIID
//
//  Created by akisute on 11/08/22.
//
/*
 Copyright (c) 2011 Masashi Ono.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#import "UIApplication-UIID.h"
#import "SDCloudUserDefaults.h"


#if UIID_PERSISTENT
// Use keychain as a storage
#import <Security/Security.h>
#endif


static NSString *const UIApplication_UIID_Key = @"uniqueInstallationIdentifier";
static NSString *const UIApplication_UserUIID_Key = @"uniqueUserIdentifier";


@implementation UIApplication (UIApplication_UIID)

- (NSString *)uniqueInstallationIdentifier
{
  NSString *cloud_uuid = [SDCloudUserDefaults stringForKey:@"com.lamark.EnglishCourseSpanish.monthly"];
  
  // Search for already created UIID
  // If found, return it
  // If not found, create a new UUID as a new UIID of this installation and save it
  NSString *uuidString = nil;
  
#if UIID_PERSISTENT
  // UIID must be persistent even if the application is removed from devices
  // Use keychain as a storage
  NSDictionary *query =
  [NSDictionary dictionaryWithObjectsAndKeys:(id)kSecClassGenericPassword, (id)kSecClass, UIApplication_UIID_Key, (id)kSecAttrGeneric, UIApplication_UIID_Key,
   (id)kSecAttrAccount, [[NSBundle mainBundle] bundleIdentifier], (id)kSecAttrService, (id)kSecMatchLimitOne,
   (id)kSecMatchLimit, (id)kCFBooleanTrue, (id)kSecReturnAttributes, nil];
  NSDictionary *attributes = nil;
  OSStatus result = SecItemCopyMatching((CFDictionaryRef)query, (CFTypeRef *)&attributes);
  if (result == noErr) {
    
    NSMutableDictionary *valueQuery = [NSMutableDictionary dictionaryWithDictionary:attributes];
    [attributes release];
    
    [valueQuery setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    [valueQuery setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    
    NSData *passwordData = nil;
    OSStatus result = SecItemCopyMatching((CFDictionaryRef)valueQuery, (CFTypeRef *)&passwordData);
    if (result == noErr) {
      
      // Assume the stored data is a UTF-8 string.
      uuidString = [[[NSString alloc] initWithBytes:[passwordData bytes] length:[passwordData length] encoding:NSUTF8StringEncoding] autorelease];
      [passwordData release];
    }
  }
#else
  // UIID may not be persistent
  // Use NSUserDefalt as a storage
  // WARNING: this could be much more vulnerable since the NSUserDefaults stores values as a plist file. Any jailbroken user can extract values from it.
  uuidString = [[NSUserDefaults standardUserDefaults] stringForKey:UIApplication_UIID_Key];
#endif
  
  if (uuidString == nil) {
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    uuidString = (NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
    [uuidString autorelease];
    CFRelease(uuidRef);
    
#if UIID_PERSISTENT
    // UIID must be persistent even if the application is removed from devices
    // Use keychain as a storage
    NSMutableDictionary *query =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:(id)kSecClassGenericPassword, (id)kSecClass, UIApplication_UIID_Key, (id)kSecAttrGeneric,
     UIApplication_UIID_Key, (id)kSecAttrAccount, [[NSBundle mainBundle] bundleIdentifier],
     (id)kSecAttrService, @"", (id)kSecAttrLabel, @"", (id)kSecAttrDescription, nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 4) {
      // kSecAttrAccessible is iOS 4 or later only
      // Current device is running on iOS 3.X, do nothing here
    } else {
      // Set kSecAttrAccessibleAfterFirstUnlock so that background applications are able to access this key.
      // Keys defined as kSecAttrAccessibleAfterFirstUnlock will be migrated to the new devices/installations via encrypted backups.
      // If you want different UIID per device, use kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly instead.
      // Keep in mind that keys defined as kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly will be removed after restoring from a backup.
      [query setObject:(id)kSecAttrAccessibleAfterFirstUnlock forKey:(id)kSecAttrAccessible];
    }
    
    if (cloud_uuid) {
      uuidString = cloud_uuid;
    }
    
    [query setObject:[uuidString dataUsingEncoding:NSUTF8StringEncoding] forKey:(id)kSecValueData];
    
    OSStatus result = SecItemAdd((CFDictionaryRef)query, NULL);
    if (result != noErr) {
      DLog(@"[ERROR] Couldn't add the Keychain Item. result = %ld query = %@", (long)result, query);
      return nil;
    }
    
    if (!cloud_uuid || [cloud_uuid isEqualToString:@""]) {
      [SDCloudUserDefaults setString:uuidString forKey:@"com.lamark.EnglishCourseSpanish.monthly"];
      [SDCloudUserDefaults synchronize];
    }
#else
    // UIID may not be persistent
    // Use NSUserDefalt as a storage
    // WARNING: this could be much more vulnerable since the NSUserDefaults stores values as a plist file. Any jailbroken user can extract values from it.
    [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:UIApplication_UIID_Key];
#endif
  }
  if([cloud_uuid isMemberOfClass:[NSString class]])
  if (cloud_uuid && ![cloud_uuid isEqualToString:@""]) {
    if (![cloud_uuid isEqualToString:uuidString]) {
      
      [self resetUniqueInstallationIdentifier];
      
#if UIID_PERSISTENT
      // UIID must be persistent even if the application is removed from devices
      // Use keychain as a storage
      NSMutableDictionary *query =
      [NSMutableDictionary dictionaryWithObjectsAndKeys:(id)kSecClassGenericPassword, (id)kSecClass, UIApplication_UIID_Key, (id)kSecAttrGeneric,
       UIApplication_UIID_Key, (id)kSecAttrAccount, [[NSBundle mainBundle] bundleIdentifier],
       (id)kSecAttrService, @"", (id)kSecAttrLabel, @"", (id)kSecAttrDescription, nil];
      
      if ([[[UIDevice currentDevice] systemVersion] floatValue] < 4) {
        // kSecAttrAccessible is iOS 4 or later only
        // Current device is running on iOS 3.X, do nothing here
      } else {
        // Set kSecAttrAccessibleAfterFirstUnlock so that background applications are able to access this key.
        // Keys defined as kSecAttrAccessibleAfterFirstUnlock will be migrated to the new devices/installations via encrypted backups.
        // If you want different UIID per device, use kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly instead.
        // Keep in mind that keys defined as kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly will be removed after restoring from a backup.
        [query setObject:(id)kSecAttrAccessibleAfterFirstUnlock forKey:(id)kSecAttrAccessible];
      }
      
      if (cloud_uuid) {
        uuidString = cloud_uuid;
      }
      
      [query setObject:[uuidString dataUsingEncoding:NSUTF8StringEncoding] forKey:(id)kSecValueData];
      
      OSStatus result = SecItemAdd((CFDictionaryRef)query, NULL);
      if (result != noErr) {
        DLog(@"[ERROR] Couldn't add the Keychain Item. result = %ld query = %@", (long)result, query);
        return nil;
      }
      
      if (!cloud_uuid || [cloud_uuid isEqualToString:@""]) {
        [SDCloudUserDefaults setString:uuidString forKey:@"com.lamark.EnglishCourseSpanish.monthly"];
        [SDCloudUserDefaults synchronize];
      }
#else
      // UIID may not be persistent
      // Use NSUserDefalt as a storage
      // WARNING: this could be much more vulnerable since the NSUserDefaults stores values as a plist file. Any jailbroken user can extract values from it.
      [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:UIApplication_UIID_Key];
#endif
    }
  }
  
  return uuidString;
}

- (void)resetUniqueInstallationIdentifier
{
#if UIID_PERSISTENT
  // UIID must be persistent even if the application is removed from devices
  // Use keychain as a storage
  NSDictionary *query =
  [NSDictionary dictionaryWithObjectsAndKeys:(id)kSecClassGenericPassword, (id)kSecClass, UIApplication_UIID_Key, (id)kSecAttrGeneric, UIApplication_UIID_Key,
   (id)kSecAttrAccount, [[NSBundle mainBundle] bundleIdentifier], (id)kSecAttrService, nil];
  OSStatus result = SecItemDelete((CFDictionaryRef)query);
  if (result == noErr) {
    DLog(@"[INFO}  Unique Installation Identifier is successfully reset.");
  } else if (result == errSecItemNotFound) {
    DLog(@"[INFO}  Unique Installation Identifier is successfully reset.");
  } else {
    DLog(@"[ERROR] Coudn't delete the Keychain Item. result = %ld query = %@", (long)result, query);
  }
#else
  // UIID may not be persistent
  // Use NSUserDefalt as a storage
  // WARNING: this could be much more vulnerable since the NSUserDefaults stores values as a plist file. Any jailbroken user can extract values from it.
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:UIApplication_UIID_Key];
  DLog(@"[INFO}  Unique Installation Identifier is successfully reset.");
#endif
}

- (NSString *)uniqueUserIdentifier
{
  NSString *cloudString = [SDCloudUserDefaults stringForKey:UIApplication_UserUIID_Key];
  NSString *uuidString = nil;
  
  NSMutableDictionary *query = [NSMutableDictionary dictionary];
  [query setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
  [query setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrGeneric];
  [query setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrAccount];
  [query setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:(__bridge id)kSecAttrService];
  [query setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
  [query setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnAttributes];
  
  NSDictionary *attributes = nil;
  OSStatus result = SecItemCopyMatching((CFDictionaryRef)query, (CFTypeRef *)&attributes);
  if (result == noErr) {
    NSMutableDictionary *valueQuery = [NSMutableDictionary dictionaryWithDictionary:attributes];
    [attributes release];
    [valueQuery setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    [valueQuery setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    NSData *passwordData = nil;
    OSStatus result = SecItemCopyMatching((CFDictionaryRef)valueQuery, (CFTypeRef *)&passwordData);
    if (result == noErr) {
      uuidString = [[[NSString alloc] initWithBytes:[passwordData bytes] length:[passwordData length] encoding:NSUTF8StringEncoding] autorelease];
      [passwordData release];
    }
  }
  
  if (cloudString && ![cloudString isEqualToString:@""]) {
    if (![uuidString isEqualToString:cloudString]) {
      [self setUniqueUserIdentifier:cloudString];
      uuidString = cloudString;
    }
  } else {
    [SDCloudUserDefaults setString:uuidString forKey:UIApplication_UserUIID_Key];
    [SDCloudUserDefaults synchronize];
  }
  
  return uuidString;
}

- (void)setUniqueUserIdentifier:(NSString *)uuidString
{
  NSString *cloudString = [SDCloudUserDefaults stringForKey:UIApplication_UserUIID_Key];
  
  OSStatus errorCode = 0;
  NSMutableDictionary *queryDelete = [NSMutableDictionary dictionary];
  [queryDelete setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
  [queryDelete setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrGeneric];
  [queryDelete setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrAccount];
  [queryDelete setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:(__bridge id)kSecAttrService];
  
  errorCode = SecItemDelete((CFDictionaryRef)queryDelete);
  
  NSMutableDictionary *query = [NSMutableDictionary dictionary];
  [query setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
  [query setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrGeneric];
  [query setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrAccount];
  [query setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:(__bridge id)kSecAttrService];
  [query setObject:@"" forKey:(__bridge id)kSecAttrLabel];
  [query setObject:@"" forKey:(__bridge id)kSecAttrDescription];
  [query setObject:(__bridge id)kSecAttrAccessibleAfterFirstUnlock forKey:(__bridge id)kSecAttrAccessible];
  [query setObject:[uuidString dataUsingEncoding:NSUTF8StringEncoding] forKey:(__bridge id)kSecValueData];
  
  errorCode = SecItemAdd((CFDictionaryRef)query, NULL);
  
  if (errorCode != noErr) {
    DLog(@"[ERROR] Couldn't add the Keychain Item. result = %ld query = %@", (long)errorCode, query);
    return;
  }
  
  if (![cloudString isEqualToString:uuidString]) {
    [SDCloudUserDefaults setString:uuidString forKey:UIApplication_UserUIID_Key];
    [SDCloudUserDefaults synchronize];
  }
}

@end
