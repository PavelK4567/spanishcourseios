//
//  LevelsData.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/26/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelStatusData : NSObject

@property (nonatomic, assign) NSInteger      levelId;
@property (nonatomic, assign) NSInteger      maxUnitId;
@property (nonatomic, assign) NSInteger      score;
@property (nonatomic, assign) NSString       *contentVersion;
@property (nonatomic, strong) NSMutableArray *unitDataArray;
@property (nonatomic        ) BOOL           diploma;

- (id)initWithDict:(NSDictionary *)dict;
@end
