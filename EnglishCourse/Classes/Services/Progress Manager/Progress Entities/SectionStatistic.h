//
//  SectionStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/25/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseStatistic.h"

@interface SectionStatistic : BaseStatistic

@property (nonatomic, assign) NSInteger      sectionID;
@property (nonatomic, assign) NSInteger      sectionOrder;
@property (nonatomic, strong) NSMutableArray *unitsArray;

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;
@end
