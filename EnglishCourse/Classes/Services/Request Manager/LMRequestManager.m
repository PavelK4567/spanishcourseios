//  LMRequestManager.m
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMRequestManager.h"
#import <ASIHTTP/ASIDownloadCache.h>


@implementation LMRequestManager


#ifdef REQUEST_SINGLETON
SINGLETON_GCD(LMRequestManager)
#endif


- (ASIHTTPRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers {
	NSURL *url = [self urlForType:type headers:headers];
	ASIHTTPRequest *request;
	if (url) {
		request = [ASIHTTPRequest requestWithURL:url usingCache:[ASIDownloadCache sharedCache] andCachePolicy:ASIAskServerIfModifiedCachePolicy];
		NSArray *keys = [headers allKeys];
		for (NSString *key in keys) {
			[request addRequestHeader:key value:headers[key]];
			return request;
		}
	}
	return 0;
}

- (NSURL *)urlForType:(ELMRequestType)type headers:(NSDictionary *)headers {
	NSString *path = 0;
	switch (type) {
     
    case kRequestLoginTokenAPICall:
      self.groupID = @"8";
      self.contentID = @"6";
      path = [NSString stringWithFormat:@"%@/sendLoginToken?msisdn=%@&groupID=%@&contentID=%@", kApiBaseUrl, headers[kUserMSISDN],self.groupID,self.contentID];
      break;
      
    case kRequestShowTokenAPICall:
      path = [NSString stringWithFormat:@"http://la-mark-il.com:8081/LMWeb/pictionary/getToken?msisdn=%@&groupID=%@", headers[kUserMSISDN],self.groupID];
      if(STAGING){
      path = [NSString stringWithFormat:@"http://kantoo.com/LMWeb/pictionary/getToken?msisdn=%@&groupID=%@", headers[kUserMSISDN],self.groupID];
      }
      break;
      
    case kRequestConfirmLoginTokenAPICall:
      path = [NSString stringWithFormat:@"%@/confirmLoginToken?msisdn=%@&groupID=8&contentID=6&phoneModel=%@&osType=IOS&osVersion=%@&token=%@", kApiBaseUrl, [headers objectForKey:kUserMSISDN],
              [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
              [[UIDevice currentDevice] systemVersion],
              [headers objectForKey:kUserCode]];
      break;

    case kRequestGetUserStatusAPICall:
        self.groupID = @"8";
        self.contentID = @"6";
      path =
      [NSString stringWithFormat:@"%@/status?msisdn=%@&groupID=%@&contentID=%@", kApiECBaseUrl, [headers objectForKey:kUserMSISDN],self.groupID,self.contentID];
      break;
      
    case kRequestGetUserNativeStatusAPICall:
            self.groupID = @"8";
            self.contentID = @"6";
      path =
       [NSString stringWithFormat:@"%@/status?msisdn=%@&groupID=%@&contentID=%@", kApiECBaseUrl, [headers objectForKey:kUserMSISDN],self.groupID,self.contentID];
      break;
    case kRequestSubscriptionForNativeUserAPICall:
            self.groupID = @"8";
            self.contentID = @"6";
      //http://la-mark-il.com:8081/LMWeb/GroupServices/confirmLoginTokenSTO?msisdn=131ECD8D-32EA-43C9-B8B4-8D8FBF964418&siteID=51&productID=14&groupID=6&contentID=1&source=Course&phoneModel=Samsung/S3&osType=version&osVersion=4.1.2
      path =
      [NSString stringWithFormat:@"%@/confirmLoginTokenSTO?msisdn=%@&siteID=78&productID=16&groupID=%@&contentID=%@&source=CourseAPP&phoneModel=%@&osType=ios&osVersion=%@",
       kApiBaseUrl,
       [headers objectForKey:kUserMSISDN],
       self.groupID,
       self.contentID,
       [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
       [[UIDevice currentDevice] systemVersion]
       ];

      break;
    case kRequestBillingForNativeUserAPICall:
            self.groupID = @"8";
            self.contentID = @"6";
      /*path =
      [NSString stringWithFormat:@"%@/billing?userUniqueID=%@&priceCode=%@",kApiBaseBillingUrl, [headers objectForKey:kUserMSISDN],kApiPriceCode];*/
      path =
      [NSString stringWithFormat:@"%@/confirmLoginTokenSTO?msisdn=%@&siteID=78&productID=16&groupID=%@&contentID=%@&source=CourseAPP&phoneModel=%@&osType=ios&osVersion=%@",
       kApiBaseUrl,
       [headers objectForKey:kUserMSISDN],
       self.groupID,
       self.contentID,
       [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
       [[UIDevice currentDevice] systemVersion]
       ];

      break;
    case kRequestLogoutUserAPICall:
      path = 0;
      break;
    case kRequestNuanceAPICall:
      
        self.groupID = @"8";
        self.contentID = @"6";
            //http://speech-13.la-mark-il.com:8080/SpeechProxy/EC/SpeechZip?userid=guest&uid=47A7046E-AE2D-4F8A-844C-DC0AAC637F84&words=Hello%20how%20%20are%20you%20my%20friend
      //path = [NSString stringWithFormat:@"%@?userid=%@&uid=%@&words=%@",kApiNuanceUrl,[headers objectForKey:kUserMSISDN],[headers objectForKey:kUserDeviceId],[self encodeToPercentEscapeString: [headers objectForKey:kSearchWords]]];
      path = [NSString stringWithFormat:@"%@?uid=%@&msisdn=%@&userid=%@&groupID=%@&contentID=%@&deviceID=%@&words=%@",kApiNuanceUrl,[headers objectForKey:kUserDeviceId],[headers objectForKey:kUserMSISDN],[headers objectForKey:kUserMSISDN],[REQUEST_MANAGER groupID],[REQUEST_MANAGER contentID],[headers objectForKey:kUserDeviceId],[self encodeToPercentEscapeString: [headers objectForKey:kSearchWords]]];
      
      //path = [NSString stringWithFormat:@"%@?userid=%@&uid=%@&words=%@",kApiNuanceUrl,[headers objectForKey:kUserMSISDN],[headers objectForKey:kUserDeviceId],[self encodeToPercentEscapeString: [headers objectForKey:kSearchWords]]];
      //path = @"http://speech-13.la-mark-il.com:8080/SpeechProxy/EC/SpeechZip?userid=guest&uid=47A7046E-AE2D-4F8A-844C-DC0AAC637F84&words=Hello%20how%20%20are%20you%20my%20friend";
      break;
    default:
      break;
	}
	if (path) {
		return [NSURL URLWithString:path];
	}
	return 0;
}

- (NSString *)encodeToPercentEscapeString:(NSString *)string {
  return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL,
                                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}

@end
