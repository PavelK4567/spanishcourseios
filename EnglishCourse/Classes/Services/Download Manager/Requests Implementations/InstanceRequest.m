//
//  InstanceRequest.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 1/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "InstanceRequest.h"

@interface InstanceRequest (Private)

- (BOOL)isBaseInstanceAlreadyDownloaded:(NSMutableArray *)arrayInstances baseInstance:(BaseInstance *)instance;

@end

@implementation InstanceRequest

- (void)executeRequest
{
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
  dispatch_async(queue, ^{
    [self getNewRequest]; // call my function - this get added first in my serial queue
  });
  
}

- (void)getNewRequest
{
    DLog(@"Instance Request is executed");
    [[Utilities sharedInstance] appendLogFileWithString:@"- Instance Request is executed\n"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
  
    __block NSString *fileNameWithPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld.zip", (long)self.instanceOrder]];
    
    NSString *instanceDataURL = [self generateURL];
    
    __weak typeof(self) weakSelf = self;
    DLog(@"instanceDataURL: %@",instanceDataURL);
         
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:instanceDataURL]];
    [request setDownloadDestinationPath:fileNameWithPath];
    [request setTimeOutSeconds:10];
    
    [request setCompletionBlock:^{
        //DLog(@"INFO: %@", request.error.description);
        
        if (request.responseStatusCode == 200 && !request.error) {
            NSError *error = 0;
            if ([[Utilities sharedInstance] fileExistAtPath:fileNameWithPath]) {
                [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"- INFO: %@", request.error.description]];
                
                //unzip
                NSDictionary *instanceDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:fileNameWithPath];
                [[Utilities sharedInstance] removeModelDataRootFolderWithContent];
                //[[Utilities sharedInstance] createModelDataRootFolder];
                
                BaseInstance *instanceData = [InstanceFactory createInstanceForType:[instanceDataParsed[kType] integerValue] instanceDataDict:instanceDataParsed];
                instanceData.pages = self.pages;
                instanceData.levelOrder = self.levelOrder;

                NSMutableArray *instancesFromSectionUnit = [self.unit instancesArray];
        
                if (!instancesFromSectionUnit) {
                    instancesFromSectionUnit = [NSMutableArray arrayWithCapacity:1];
                    self.unit.instancesArray = instancesFromSectionUnit;
                }
                
                if (self.isSynced) {
        
                    if (instancesFromSectionUnit && [instancesFromSectionUnit count] > 0 && ((instanceData.order - 1) < [instancesFromSectionUnit count])) {
        
                        BaseInstance *existingInstance = instancesFromSectionUnit[(instanceData.order - 1)];
        
                        if (existingInstance.mID == instanceData.mID) {

                            existingInstance.tempInstance = instanceData;
                            [self changeMd5:instanceData.mID andLevelOrder:instanceData.levelOrder];
                        }
        
                    }
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
                                              dispatch_async(queue, ^{
                                                    [[DATA_SERVICE dataModel] saveModel];
                                                });
        
                } else {
        
                  if(instanceData) {
                      
                    @try {
        
                        if (![self isBaseInstanceAlreadyDownloaded:instancesFromSectionUnit baseInstance:instanceData]) {
                            [instancesFromSectionUnit addObject:instanceData];
                            
                            //NSLog(@">>UNIT MEMORY ADDRESS:%p UNIT ID:%lu DOWNLOADED INSTANCES:%lu IN ARRAY:%lu", self.unit, (unsigned long)self.unit.unitID, (unsigned long)[self.unit.instancesArray count], (unsigned long)[instancesFromSectionUnit count]);
//                            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
//                            dispatch_async(queue, ^{
//                                [[DATA_SERVICE dataModel] saveModel];
//                            });
                            
//                            NSArray *sortedArray = [self.unit.instancesArray sortedArrayUsingComparator:^NSComparisonResult(BaseInstance *obj1, BaseInstance *obj2) {
//                                NSInteger firstObjID = obj1.order;
//                                NSInteger secondObjID = obj2.order;
//                                if (firstObjID < secondObjID) {
//                                    return NSOrderedAscending;
//                                } else if (firstObjID > secondObjID) {
//                                    return NSOrderedDescending;
//                                } else {
//                                    return NSOrderedSame;
//                                }
//                            }];
//                            
//                            self.unit.instancesArray = nil;
//                            self.unit.instancesArray = [NSMutableArray arrayWithArray:sortedArray];
                            
//                            NSLog(@"SORTED INSTANCES BY ORDER");
//                            
//                            for (int i = 0; i < [ self.unit.instancesArray  count]; i++) {
//                                BaseInstance *instance =  self.unit.instancesArray [i];
//                                NSLog(@"INSTNACE ID:%d AT INDEX:%d WITH ORDER:%d",instance.mID, i, instance.order);
//                            }
                            
                        }
                        else {
        
                            //NSLog(@"Already downloaded and added in data source");
                        }
                    }
                    @catch (NSException *exception) {
                      DLog(@"#error self.instancesFromSectionUnit addObject:instanceData");
                    }
                    @finally {
                      //DLog(@"");
                    }
                  }
                  
                }
                
                self.isRequestFinished = YES;
                self.isRequestRunning = NO;
                
                [self.delegate requestDidFinishWithExecution:weakSelf withError:NO errorMessage:nil];
                DLog(@"finish with download: %@", fileNameWithPath);
                
            } else {
                [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"- ERROR: %@", request.error.description]];
                [self.delegate requestDidFinishWithExecution:weakSelf withError:YES errorMessage:@"FAILED"];
            }
        } else {
            [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"- request.responseStatusCode: %d", request.responseStatusCode]];
            [self.delegate requestDidFinishWithExecution:weakSelf withError:YES errorMessage:@"FAILED"];
        }
        
    }];
    [request setFailedBlock:^{
        DLog(@"ERROR: %@", request.error.description);
        [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"- ERROR: %@", request.error.description]];
        [self.delegate requestDidFinishWithExecution:weakSelf withError:YES errorMessage:@"FAILED"];
    }];
    [request startAsynchronous];
    
    self.isRequestRunning = YES;
    
}
-(void)changeMd5:(NSInteger)instanceID andLevelOrder:(NSInteger)levelOrder{
 
    NSMutableArray *instanceSignaturesArray = [[SyncService sharedInstance] lastInstanceSignaturesArray];;
    
    CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
    
    LevelData *currentLevelData = courseStructure.levelsArray[levelOrder-1];
    Level *levelDataLevelInfo = currentLevelData.levelInfo;
    NSMutableArray *levelDataInstanceSignaturesArray = levelDataLevelInfo.instanceSignaturesArray;
    
   
    
    for (int i=0;i<instanceSignaturesArray.count;i++) {
        
        for (int j=0;j<levelDataInstanceSignaturesArray.count;j++) {
            
            
            InstanceSignature *newSig = instanceSignaturesArray[i];
            InstanceSignature *oldSig = levelDataInstanceSignaturesArray[j];
            
            if(newSig.instanceID == oldSig.instanceID && oldSig.instanceID == instanceID) {
                
                levelDataInstanceSignaturesArray[j] = newSig;
            }
            
        }
        
    }
    
    levelDataLevelInfo.instanceSignaturesArray = levelDataInstanceSignaturesArray;
    [DATA_SERVICE.dataModel saveModel];
}
#pragma mark -
#pragma mark - Helper method check if base instance exists

- (BOOL)isBaseInstanceAlreadyDownloaded:(NSMutableArray *)arrayInstances baseInstance:(BaseInstance *)instance
{
    
    for (BaseInstance *currentInstance in arrayInstances) {
        
        if ([currentInstance isKindOfClass:[BaseInstance class]]) {
            
            if (currentInstance.mID == instance.mID) {
                
                return YES;
            }
            
        }
        
    }
    
    return NO;
}

- (NSString *)generateURL
{
    return [NSString stringWithFormat:kCallDownloadInstnaceData,(long)self.levelOrder, self.levelVersion, (long)self.instanceOrder];
}

- (void)printDataAsString
{
    //NSLog(@"INSTANCE ID:%ld LEVEL INDEX:%ld SECTION INDEX:%ld UNIT INDEX:%ld PAGES:%ld NEXT INDEX:%ld", self.instanceID, self.levelIndex, self.sectionIndex, self.unitIndex, self.pages, self.nextIndex);
}

@end
