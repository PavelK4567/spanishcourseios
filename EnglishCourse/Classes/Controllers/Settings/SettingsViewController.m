//
//  SettingsViewController.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)configureAppearance
{
  [super configureAppearance];
  versionInfoLabel.text = [NSString stringWithFormat:@"Current version:%@",[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]];
  nativeUserInfoLabel.text = @"";
  userDetailsLabel.text = Localized(@"T551");
  statusLabel.text = Localized(@"T552");
  // userStatusInfoLabel.text = Localized(@"T550");
  vivoNumberLabel.text = Localized(@"T553");
  // vivoNumberDetailsLabel.text = Localized(@"T550");
  enableNotificationLabel.text = Localized(@"T557");
  soundEfectsLabel.text = Localized(@"T562");
  [APPSTYLE applyTitle:Localized(@"T564") toButton:logoutButton];
  
  vivoNumberDetailsLabel.text = [NSString stringWithFormat:@"  %@", [USER_MANAGER userPhone]];
  if ([USER_MANAGER isGuest]) {
    //self.numberLabel.enabled = NO;
    [vivoNumberDetailsLabel setHidden:YES];
    [vivoNumberLabel setHidden:YES];
    [APPSTYLE applyTitle:Localized(@"T563") toButton:logoutButton];
    logoutButton.y = logoutButton.y - 50;
    userStatusInfoLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T570")];
  }
  else if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
    userStatusInfoLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T567")];
  }
  else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
    userStatusInfoLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T568")];
  }
  else {
    userStatusInfoLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T569")];
  }
  if ([USER_MANAGER isNative] ) {
    //self.numberLabel.enabled = NO;
    [logoutButton  setHidden:YES];
    [vivoNumberDetailsLabel setHidden:YES];
    [vivoNumberLabel setHidden:YES];
    nativeUserInfoLabel.text = [NSString stringWithFormat:Localized(@"T566"), [USER_MANAGER takeNativeDateStringFromDate]];
  }
}

- (void)configureUI
{
  [super configureUI];
//  [APPSTYLE applyStyle:@"Profile_Title_Label" toLabel:userDetailsLabel];
    userDetailsLabel.textColor = [APPSTYLE colorForType:@"Purple_Main_Color"];
    
  [APPSTYLE applyStyle:@"Label_Title" toLabel:statusLabel];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:userStatusInfoLabel];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:vivoNumberLabel];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:vivoNumberDetailsLabel];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:enableNotificationLabel];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:soundEfectsLabel];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:nativeUserInfoLabel];
  
  logoutButton.layer.cornerRadius = 4.0;
  logoutButton.layer.borderWidth = 1;
  
   soundEfectsSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"SoundEffectsEnabled"];
   enableNotificationSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"NotificationEnabled"];
  

  
}


- (void)configureObservers
{
  [super configureObservers];
}

- (void)configureNavigation
{
  [super configureNavigation];
  self.navigationItem.title = Localized(@"T550");
  // self.navigationController.navigationBarHidden = YES;
}

- (void)configureData
{
  [super configureData];

}

- (void)loadData
{
  [super loadData];
}
#pragma mark - Interface Builder Actions

- (IBAction)notificationSwitchPress:(id)sender {
  [[NSUserDefaults standardUserDefaults] setBool:enableNotificationSwitch.on forKey:@"NotificationEnabled"];
  //[AUDIO_MANAGER setEnabled:enableNotificationSwitch.on];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)soundEffectsSwichPress:(id)sender {
  [[NSUserDefaults standardUserDefaults] setBool:soundEfectsSwitch.on forKey:@"SoundEffectsEnabled"];
  [AUDIO_MANAGER setEnabled:soundEfectsSwitch.on];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)pressLogOut:(id)sender {
  if ([USER_MANAGER isGuest]) {
      
    [PROGRESS_MANAGER saveGestUserStatistic:PROGRESS_MANAGER.userStatistic];
    [APP_DELEGATE buildLoginStack];
    if(DOWNLOAD_MANAGER.isManagerExecutingRequests) {
      [DOWNLOAD_MANAGER cancelRequestsExecution];
    }
  }else{
    [self showLogOutAlert];
  }
    
  [PROGRESS_MANAGER deleteStatistic];
}

#pragma mark - log out for active user
-(void)showLogOutAlert{
  UIAlertView *tempAlert = [UIAlertView alertViewWithTitle:@""
                                                   message:Localized(@"T370")
                                         cancelButtonTitle:Localized(@"T371")
                                         otherButtonTitles:@[Localized(@"T372")]
                                                 onDismiss: ^(int buttonIndex) {
                                                   if (buttonIndex == -1){
                                                     
                                                   }else if (buttonIndex == 0){
                                                     [USER_MANAGER logOutUser];
                                                     [APP_DELEGATE buildLoginStack];
                                                     if(DOWNLOAD_MANAGER.isManagerExecutingRequests) {
                                                       [DOWNLOAD_MANAGER cancelRequestsExecution];
                                                       [PROGRESS_MANAGER deleteStatistic];
                                                     }
                                                   }
                                                 }
                                                  onCancel: ^{
                                                    
                                                  }];
  [tempAlert show];
}

@end
