//
//  ECLevelViewController.h
//  KMGame
//
//  Created by Dimitar Shopovski on 11/14/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//  Updated by Darko

#import <UIKit/UIKit.h>

@interface ECLevelViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tbLevels;
@property (nonatomic, strong) NSMutableArray *arrayDataSource;

@end
