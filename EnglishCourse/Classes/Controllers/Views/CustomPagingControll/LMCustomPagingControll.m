//
//  LMCustomPagingControll.m
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/10/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "LMCustomPagingControll.h"

@implementation LMCustomPagingControll
@synthesize delegate, viewDotsContent, pageCurrent, dotsNumber;

- (void)drawTheControll:(CGRect)rect {
    
    PageDotView *viewDot;
    UIView *viewMaskCurrentDot;
    
    
    ///create the dots, alloc
    float widthOfDots = 6;
    float xPos = 0;
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, widthOfDots+5+44);
    
    //change the positions of the buttons
    self.btnNext.frame = CGRectMake(self.frame.size.width - 170, self.frame.size.height - 44, 170, 40);
    self.btnPrevious.frame = CGRectMake(0, self.frame.size.height - 44, 70, 40);
    
    for (int i=0; i<dotsNumber; i++) {
        
        viewDot = [[PageDotView alloc] initWithFrame:CGRectMake(xPos, 0, widthOfDots, widthOfDots)];
        [viewDot.layer setCornerRadius:2];
        [viewDot.layer setMasksToBounds:YES];
        
        //Check in the progress manager if the instance is finished
        
        UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
        BaseInstance *instance = [self.arrayLessonInstances objectAtIndex:i];
        InstanceStatistic *instanceStatistic;;
        PageStatistic *pageStatistic;
        
        if (instance.pageNumber == 0) {
            
//                DLog(@"Instance number - %ld", instance.instanceNumber);

            instanceStatistic = [PROGRESS_MANAGER getInstanceStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber userStatistic:userStatistic];
            
            if (instanceStatistic.status)
                [viewDot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
            else
                [viewDot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];

        }
        else {
            
//                DLog(@"Instance number - %ld", instance.instanceNumber);

            pageStatistic = [PROGRESS_MANAGER getPageStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber page:instance.pageNumber-1 userStatistic:userStatistic];
        
            if ([pageStatistic pageStatus])
                [viewDot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
            else
                [viewDot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];

        }
        
        
        
        viewDot.tag = i;
        
        [viewDotsContent addSubview:viewDot];
        
        
        /////mask views
        
        viewMaskCurrentDot = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0, widthOfDots, widthOfDots)];
        [viewMaskCurrentDot.layer setCornerRadius:2];
        [viewMaskCurrentDot.layer setMasksToBounds:YES];

        viewMaskCurrentDot.tag = 200+i;
//        [viewMaskCurrentDot setBackgroundColor:[UIColor redColor]];
//        viewMaskCurrentDot.alpha = 0.2;
        
        if (i == pageCurrent) {
            
            [viewMaskCurrentDot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Current_Color"]];
        }

        
        [viewDotsContent addSubview:viewMaskCurrentDot];
        
        xPos+=widthOfDots+2;

        
    }
    
    self.viewDotsContent.frame = CGRectMake(0, self.viewDotsContent.frame.origin.y, xPos, self.viewDotsContent.frame.size.height);
    self.viewDotsContent.center = CGPointMake(self.center.x, self.viewDotsContent.center.y);

    
    
    //buttons
    if (pageCurrent == 0) {
        
        self.btnPrevious.hidden = YES;
        [self.btnPrevious setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
    }
    else {
        
        self.btnPrevious.hidden = NO;
        [self.btnPrevious setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
        
    }    
}


#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame userProgress:(NSInteger)userProgress instances:(NSArray *)arrayInstances andDots:(NSInteger)dots {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.dotsNumber = dots;
        self.pageCurrent = userProgress;
        self.arrayLessonInstances = arrayInstances;
        
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self baseInit];
        
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self baseInit];
    }
    
    return self;
}

- (void)baseInit {	
    
    [self setBackgroundColor:[APPSTYLE colorForType:@"New_Vehicle_Background"]];
    
    viewDotsContent = [[PageDotView alloc] initWithFrame:CGRectMake(0, 2, self.frame.size.width, self.frame.size.height-44)];
    self.viewDotsContent.tag = 5555;
    [viewDotsContent setBackgroundColor:[UIColor clearColor]];
    [self addSubview:viewDotsContent];
    
    //line
    
    UIView *viewLine = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.frame.size.width, 1)];
    viewLine.tag = 4444;
    [viewLine setBackgroundColor:[APPSTYLE colorForType:@"Dot_Current_Color"]];
    [self addSubview:viewLine];
    
    self.btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnNext.frame = CGRectMake(self.frame.size.width - 160, self.frame.size.height - 44, 150, 40);
    self.btnNext.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.btnNext setTitle:@"SKIP" forState:UIControlStateNormal];
    [self.btnNext setImage:[UIImage imageNamed:@"ic_next"] forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
    [self.btnNext addTarget:self action:@selector(nextButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext setImageEdgeInsets:UIEdgeInsetsMake(0, 140, 0, 0)];
    [self.btnNext setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 30)];

    [self addSubview:self.btnNext];
    
    self.btnPrevious = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnPrevious.frame = CGRectMake(0, self.frame.size.height - 44, 70, 40);
    [self.btnPrevious setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];
    [self.btnPrevious setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
    [self.btnPrevious addTarget:self action:@selector(previousButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPrevious setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 35)];

    [self addSubview:self.btnPrevious];
    
    isFirstInstall = YES;

//    DLog(@"Number of objects - %ld", self.arrayLessonInstancesStats.count);
    
    [self drawTheControll:self.frame];
}



#pragma mark - Next and Previous button events

- (IBAction)nextButtonSelected:(id)sender {
    
    [delegate nextButtonAction:self withSpeed:0.25];

}


- (IBAction)previousButtonSelected:(id)sender {
    
    [delegate previousButtonSelected:self withSpeed:0.25];
    
}


#pragma mark - Set current page

- (void)refreshControlWithCurrentPage:(NSInteger)currentPage {
    
    if (self.pageCurrent == currentPage)
        return;
    if (currentPage >= self.dotsNumber) {
        return;
    }
    
    self.pageCurrent = currentPage;
    
    ///set color of dots
    PageDotView *viewDotPrevious = (PageDotView *)[self.viewDotsContent viewWithTag:currentPage-1];
    if (viewDotPrevious)
        [self updatePageDot:viewDotPrevious];
    PageDotView *viewDotNext = (PageDotView *)[self.viewDotsContent viewWithTag:currentPage+1];
    if (viewDotNext)
        [self updatePageDot:viewDotNext];
    
    
    
    UIView *viewDot = (UIView *)[self.viewDotsContent viewWithTag:200+currentPage];
    
    if (viewDot != nil) {

        [viewDot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Current_Color"]];
    }
    else {
        
        NSLog(@"---nil dot---");
    }
    
    UIView *viewOtherDots = nil;
    for (int i=0; i<dotsNumber; i++) {
        
        if (i != currentPage) {
            
            viewOtherDots = [self.viewDotsContent viewWithTag:200+i];
            [viewOtherDots setBackgroundColor:[UIColor clearColor]];
        }
        
    }
    
    
    if (currentPage == 0) {
        
        self.btnPrevious.hidden = YES;
        [self.btnPrevious setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
    }
    else {
        
        self.btnPrevious.hidden = NO;
        [self.btnPrevious setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
        
    }
    
}

#pragma mark -
#pragma mark - Updated specific dot

- (void)updatePageDot:(PageDotView *)dot {
    
    if (dot.isFinished) {
        
        [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
        return;
    }
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    BaseInstance *instance = [self.arrayLessonInstances objectAtIndex:dot.tag];
    InstanceStatistic *instanceStatistic;
    PageStatistic *pageStatistic;
    
    if (instance.type == 11) {
        
        if (dot && [dot isKindOfClass:[PageDotView class]]) {
        
            instanceStatistic = [PROGRESS_MANAGER getInstanceStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber userStatistic:userStatistic];
            
            int answered = 0;
            for (PageStatistic *pageStats in [instanceStatistic pagesArray]) {
                
                answered+=pageStats.numberOfCorrectAnswers;
                answered+=pageStats.numberOfIncorrectAnswers;
            }
            
            if (answered == [[instanceStatistic pagesArray] count]) {
                
                dot.isFinished = YES;
                [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
                
            }
            else {
                
                dot.isFinished = NO;
                [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];
            }
        }
        
    }
    else {
        
        if (instance.pageNumber == 0) {
            
            instanceStatistic = [PROGRESS_MANAGER getInstanceStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber userStatistic:userStatistic];
            
            if (dot && [dot isKindOfClass:[PageDotView class]]) {
                
                if (instanceStatistic.status) {
                    
                    dot.isFinished = YES;
                    [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
                }
                else {
                    
                    dot.isFinished = NO;
                    [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];
                    
                }
            }
        }
        else {
            
//            DLog(@"Level - %ld, Section - %ld, Unit - %ld, instance - %ld, page number - %ld", [PROGRESS_MANAGER currentLevelIndex], [PROGRESS_MANAGER currentSectionIndex], [PROGRESS_MANAGER currentUnitIndex], instance.instanceNumber, instance.pageNumber-1);
            pageStatistic = [PROGRESS_MANAGER getPageStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber page:instance.pageNumber-1 userStatistic:userStatistic];
            
            if (dot && [dot isKindOfClass:[PageDotView class]]) {
                
                if ([pageStatistic pageStatus]) {
                    
                    dot.isFinished = YES;
                    [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
                    
                }
                else {
                    
                    dot.isFinished = NO;
                    [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];
                }
            }
        }

        
    }
    
}

- (void)updateDotAtIndex:(int)dotIndex {
   
    PageDotView *dot = (PageDotView *)[self.viewDotsContent viewWithTag:dotIndex];
    
    if (dot.isFinished) {
        
        [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
        return;
    }
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    BaseInstance *instance = [self.arrayLessonInstances objectAtIndex:dot.tag];
    InstanceStatistic *instanceStatistic;
    PageStatistic *pageStatistic;
    
    if (instance.pageNumber == 0) {
        
        instanceStatistic = [PROGRESS_MANAGER getInstanceStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber userStatistic:userStatistic];
        
        if (dot && [dot isKindOfClass:[PageDotView class]]) {
            
            if (instanceStatistic.status) {
                
                dot.isFinished = YES;
                [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
            }
            else {
                
                dot.isFinished = NO;
                [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];
                
            }
        }
    }
    else {
        
        //        DLog(@"Level - %ld, Section - %ld, Unit - %ld, instance - %ld, page number - %ld", [PROGRESS_MANAGER currentLevelIndex], [PROGRESS_MANAGER currentSectionIndex], [PROGRESS_MANAGER currentUnitIndex], instance.instanceNumber, instance.pageNumber-1);
        pageStatistic = [PROGRESS_MANAGER getPageStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:instance.instanceNumber page:instance.pageNumber-1 userStatistic:userStatistic];
        
        if (dot && [dot isKindOfClass:[PageDotView class]]) {
            
            if ([pageStatistic pageStatus]) {
                
                dot.isFinished = YES;
                [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Active_Color"]];
                
            }
            else {
                
                dot.isFinished = NO;
                [dot setBackgroundColor:[APPSTYLE colorForType:@"Dot_Closed_Color"]];
            }
        }
    }
    
}


#pragma mark - Create again with new parameters

- (void)recreateControllWithCurrentPage:(NSInteger)currentPage instances:(NSArray *)arrayInstance andDots:(NSInteger)dots {
    
    self.dotsNumber = dots;
    self.pageCurrent = currentPage;
    self.arrayLessonInstances = arrayInstance;
    
    for (PageDotView *vv in viewDotsContent.subviews) {
        
        [vv removeFromSuperview];
    }
    
    
    [self drawTheControll:self.frame];
    
}


@end
