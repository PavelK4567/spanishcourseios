//
//  MultiChoiceImages.m
//  ECControls
//
//  Created by Dimitar Shopovski on 11/18/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "MultiChoiceImages.h"

@implementation MultiChoiceImages
@synthesize width, arrayInteractiveImages;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.arrayInteractiveImages = [[NSMutableArray alloc] init];
    }
    
    return self;
    
}

- (void)layoutSubviews {
    
    //// number of images - [words count];
    
    //order
    
    UIView *interactiveImage;
    
    width = (self.frame.size.width-2*20-10)/2;
    
    for (int i=0; i<4; i++) {
        
        interactiveImage = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width-(width/2), -width, width, width)];
        [interactiveImage setBackgroundColor:[UIColor redColor]];
//        interactiveImage.hidden = YES;
        interactiveImage.alpha = 0.0;
        interactiveImage.tag = i+1;
        [self addSubview:interactiveImage];
        
        [self.arrayInteractiveImages addObject:interactiveImage];
        
    }
    
    
    
}

- (void)animateLayout {
    
    for (UIView *interactiveImage in self.arrayInteractiveImages) {
                
        if (interactiveImage.tag != 0) {
            
            [UIView animateWithDuration:2.0 animations:^{
                
                interactiveImage.alpha = 1.0;
                interactiveImage.center = self.center;
                
            } completion:^(BOOL finished) {
                
                //go to the proper position
                
                if (interactiveImage.tag == 1) {
                    
                    interactiveImage.center = CGPointMake(20+width/2, 20+width/2);
                
                }
                else if (interactiveImage.tag == 2) {
                    
                    interactiveImage.center = CGPointMake(20+width+10+width/2, 20+width/2);
                }
                else if (interactiveImage.tag == 3) {
                    
                    interactiveImage.center = CGPointMake(20+width/2, 20+width+10+width/2);
                }
                else if (interactiveImage.tag == 4) {
                    
                    interactiveImage.center = CGPointMake(20+width+10+width/2, 20+width+10+width/2);
                }
                
            }];
            
            
            
        }
    }
    
}

@end
