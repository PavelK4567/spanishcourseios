//
//  CustomVehicleButton.m
//  ScrollPagingTest
//
//  Created by Dimitar Shopovski on 11/11/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "CustomVehicleButton.h"

@implementation CustomVehicleButton
@synthesize dictAnswerInfo, delegate;


- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.numberOfLines = 0;
    
//        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        [self setTitleEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];

        
        [self addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.layer setCornerRadius:3];
        [self.layer setMasksToBounds:YES];
        
        [self setBackgroundColor:[APPSTYLE colorForType:@"Bg_Multichoice_Normal"]];
        
        [self.titleLabel setFont:[APPSTYLE fontForType:@"Button_Multichoice"]];
        
        self.layer.masksToBounds = NO;

        [self.layer setShadowColor:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0].CGColor];
        [self.layer setShadowOpacity:1.0];
        [self.layer setShadowOffset:CGSizeMake(0.0, 1.0)];
    }
    
    return self;
}


- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    
    [super setTitle:title forState:state];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    
    [self.titleLabel setFont:[APPSTYLE fontForType:@"Button_Multichoice"]];



}

- (void)setType:(BOOL)isPractice; {
    
    self.isPracticeQuestion = isPractice;

    if (self.isPracticeQuestion)
        [self setBackgroundColor:[APPSTYLE colorForType:@"New_Vehicle_Background"]];
    else
        [self setBackgroundColor:[APPSTYLE colorForType:@"Bg_Multichoice_Normal"]];
    
}

- (IBAction)click:(id)sender {
    
    [self.delegate customButtonSelected:self];
    
}

- (void)setStateOfTheAnswer:(BOOL)state {
        
    if (state) {
        [self setBackgroundColor:[APPSTYLE colorForType:@"Correct_Answer"]];
    }
    else {
        
        [self setBackgroundColor:[APPSTYLE colorForType:@"Incorrect_Answer"]];
    }
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

}

- (void)resetToNormalState {
    
    if (self.isPracticeQuestion)
        [self setBackgroundColor:[APPSTYLE colorForType:@"New_Vehicle_Background"]];
    else
        [self setBackgroundColor:[APPSTYLE colorForType:@"Bg_Multichoice_Normal"]];

    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}

@end
