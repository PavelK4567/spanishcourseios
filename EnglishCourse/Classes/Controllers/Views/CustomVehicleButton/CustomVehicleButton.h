//
//  CustomVehicleButton.h
//  ScrollPagingTest
//
//  Created by Dimitar Shopovski on 11/11/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomVehicleButtonDelegate <NSObject>

@required

- (void)customButtonSelected:(id)sender;

@end

@interface CustomVehicleButton : UIButton

@property (nonatomic, strong) Answer *dictAnswerInfo;
@property (nonatomic, weak) id <CustomVehicleButtonDelegate> delegate;
@property (nonatomic, assign) BOOL isPracticeQuestion;

- (void)setStateOfTheAnswer:(BOOL)state;
- (void)resetToNormalState;
- (void)setType:(BOOL)isPractice;

@end
