//
//  CustomTitleView.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 2/12/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "CustomTitleView.h"

@implementation CustomTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    
  self = [super initWithFrame:frame];
  
  if (self) {
    
        [self setBackgroundColor:[APPSTYLE colorForType:@"Vehicle_Title_Background"]];
        
        self.lblTitle = [[UITextView alloc] initWithFrame:CGRectMake(36, 7, (LM_WIDTH_INSTACE-2*36), 76)];
        [self.lblTitle setBackgroundColor:[UIColor clearColor]];
//        [self.lblTitle setFont:[UIFont systemFontOfSize:14.0]];
        self.lblTitle.textAlignment = NSTextAlignmentCenter;
        [self.lblTitle setTextColor:[UIColor blackColor]];
        self.lblTitle.editable = NO;
        self.lblTitle.textContainerInset = UIEdgeInsetsZero;
        [APPSTYLE applyStyle:@"Label_Question_Instance" toTextView:self.lblTitle];
        [self addSubview:self.lblTitle];

    }
    
    return self;
}
-(void)setTitleForFramePresentation:(NSString *)title {
  if (IS_IPHONE4)
  {
    [self.lblTitle setFrame:CGRectMake(10, 5, (LM_WIDTH_INSTACE-2*10), 76)];
    //[self.lblTitle setBackgroundColor:[UIColor redColor]];
  }
    [self.lblTitle setFrame:CGRectMake(10, 5, (LM_WIDTH_INSTACE-2*10), 76)];

    [self setTitleOftheInstance:title];
}
- (void)setTitleOftheInstance:(NSString *)title {
        
    if (!title) {
        return;
    }
    self.lblTitle.text = title;
    
    [self.lblTitle sizeToFit];
    [self.lblTitle layoutIfNeeded];
    [self.lblTitle setScrollEnabled:NO];
    self.lblTitle.x = (LM_WIDTH_INSTACE-self.lblTitle.width)/2;
    
    
//    UIFont *font = self.lblTitle.font;
//    NSString *strTextTitle = title;
//    
//    
//    NSAttributedString *attributedText = [[NSAttributedString alloc]
//                                          initWithString:strTextTitle
//                                          attributes:@{ NSFontAttributeName: font}];
//    
//    const CGRect paragraphRect = [attributedText boundingRectWithSize:CGSizeMake(self.lblTitle.width, 300)
//                                                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
//                                                              context:nil];
//    CGSize contentSize = paragraphRect.size;
//    NSInteger numberOfLines = contentSize.height / font.lineHeight;
//    
//    
//    float fontLineHeight = [self.lblTitle font].lineHeight;
//    
////    DLog(@"Title - %@ ----- %ld", title, numberOfLines);
//    
//    if ([self.lblTitle.text length] == 0) {
//        
//        self.hidden = YES;
//    }else{
//      self.hidden = NO;
//    }
//    
//    if (numberOfLines == 1) {
//        
//        self.lblTitle.height = fontLineHeight;
//        self.lblTitle.center = CGPointMake(self.lblTitle.center.x, 8+(fontLineHeight)/2);
//        self.lblTitle.scrollEnabled = NO;
//    }
//    else if (numberOfLines == 2) {
//        self.lblTitle.frame = CGRectMake(self.lblTitle.x, self.lblTitle.y, self.lblTitle.width, (fontLineHeight+3)*2);
//        self.lblTitle.center = CGPointMake(self.lblTitle.center.x, 8+((fontLineHeight+3)*2)/2);
//        self.lblTitle.scrollEnabled = NO;
//    }
//    else if (numberOfLines == 3) {
//        
//        self.lblTitle.height = fontLineHeight*3+3;
//        self.lblTitle.center = CGPointMake(self.lblTitle.center.x, 8+((fontLineHeight)*3)/2);
//        self.lblTitle.scrollEnabled = NO;
//    }
//    else if (numberOfLines > 3) {
//        
//        self.lblTitle.height = fontLineHeight*3+3;
//        self.lblTitle.center = CGPointMake(self.lblTitle.center.x, 8+(fontLineHeight*3)/2);
//    }
    
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.lblTitle.frame.size.height + 16)];
    
}

@end
