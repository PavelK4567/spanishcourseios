//
//  FeedbackView.m
//  Proba
//
//  Created by Dimitar Shopovski on 11/4/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "FeedbackView.h"

@implementation FeedbackView
@synthesize lblBodyMessage, strFeedbackMessage;
@synthesize delegateFeedback;


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
                
        self.viewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 100)];
        self.viewBg.alpha = 0.0;
        
        self.lblHeaderMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, frame.size.width, 25)];
        [self.lblHeaderMessage setBackgroundColor:[UIColor clearColor]];
        [self.lblHeaderMessage setTextAlignment:NSTextAlignmentCenter];
        [self.lblHeaderMessage setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:23.0]];
        [self.lblHeaderMessage setTextColor:[UIColor whiteColor]];
        [self.viewBg addSubview:self.lblHeaderMessage];

        self.lblBodyMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, frame.size.width, 40)];
        [self.lblBodyMessage setBackgroundColor:[UIColor clearColor]];
        [self.lblBodyMessage setTextAlignment:NSTextAlignmentCenter];
        [self.lblBodyMessage setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.0]];
        [self.lblBodyMessage setTextColor:[UIColor whiteColor]];
        [self.viewBg addSubview:self.lblBodyMessage];

        self.lblFooterMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, frame.size.width, 20)];
        [self.lblFooterMessage setBackgroundColor:[UIColor clearColor]];
        [self.lblFooterMessage setTextColor:[UIColor whiteColor]];
        [self.lblFooterMessage setTextAlignment:NSTextAlignmentCenter];
        [self.viewBg addSubview:self.lblFooterMessage];

        // drop shadow
        [self.viewBg.layer setShadowColor:[UIColor colorWithRed:47.0/255.0 green:47.0/255.0 blue:47.0/255.0 alpha:0.64].CGColor];
        [self.viewBg.layer setShadowOpacity:1.0];
        [self.viewBg.layer setShadowOffset:CGSizeMake(0.0, 6.0)];
        
        [self addSubview:self.viewBg];
        
        self.leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeAction:)];
        [self.leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
        self.rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeAction:)];
        [self.rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];

        [self addGestureRecognizer:self.leftSwipe];
        [self addGestureRecognizer:self.rightSwipe];
        [self addGestureRecognizer:self.tapGesture];
        
        UISwipeGestureRecognizer *leftSwipeMiddle = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeAction:)];
        [leftSwipeMiddle setDirection:UISwipeGestureRecognizerDirectionLeft];
        UISwipeGestureRecognizer *rightSwipeMiddle = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeAction:)];
        [rightSwipeMiddle setDirection:UISwipeGestureRecognizerDirectionRight];
        UITapGestureRecognizer *tapGestureMiddle = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];

        [self.viewBg addGestureRecognizer:leftSwipeMiddle];
        [self.viewBg addGestureRecognizer:rightSwipeMiddle];
        [self.viewBg addGestureRecognizer:tapGestureMiddle];
        
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
        [panRecognizer requireGestureRecognizerToFail:leftSwipeMiddle];
        [panRecognizer requireGestureRecognizerToFail:rightSwipeMiddle];
        [panRecognizer requireGestureRecognizerToFail:tapGestureMiddle];

        [self.viewBg addGestureRecognizer:panRecognizer];
        
        [self setBackgroundColor:[UIColor clearColor]];
        
    }
    
    return self;
    
}



- (void)layoutSubviews {
    
    if (self.isCorrectAnswer) {
        
        [self.viewBg setBackgroundColor:[APPSTYLE colorForType:@"Correct_Answer"]];
    }
    else {
        
        [self.viewBg setBackgroundColor:[APPSTYLE colorForType:@"Incorrect_Answer"]];
    }
    
}

- (void)setInformation:(BOOL)isCorrect {
    
    self.isCorrectAnswer = isCorrect;
    
    self.lblBodyMessage.numberOfLines = 0;
    if (isCorrect) {
        
        if (self.correctFeedback) {
            self.lblBodyMessage.text = self.correctFeedback.body;
            self.lblHeaderMessage.text = (self.correctFeedback.header && ![self.correctFeedback.header isEqualToString:@""]) ? self.correctFeedback.header : Localized(@"T104");
            self.lblFooterMessage.text = (self.correctFeedback.footer && ![self.correctFeedback.footer isEqualToString:@""]) ? self.correctFeedback.footer : Localized(@"T105");
        }
        else {
            
            self.lblHeaderMessage.text = Localized(@"T104");
            self.lblFooterMessage.text = Localized(@"T105");
        }
        [self.viewBg setBackgroundColor:[APPSTYLE colorForType:@"Correct_Answer"]];
    }
    else {
        
        if (self.incorrectFeedback) {
        
            self.lblHeaderMessage.text = (self.incorrectFeedback.header && ![self.correctFeedback.header isEqualToString:@""]) ? self.incorrectFeedback.header : Localized(@"T106");
            self.lblBodyMessage.text = self.incorrectFeedback.body;
            self.lblFooterMessage.text = (self.correctFeedback.footer && ![self.correctFeedback.footer isEqualToString:@""]) ? self.correctFeedback.footer : Localized(@"T107");
        
        }
        else {
            
            self.lblHeaderMessage.text = Localized(@"T106");
            self.lblFooterMessage.text = Localized(@"T107");
        }
        [self.viewBg setBackgroundColor:[APPSTYLE colorForType:@"Incorrect_Answer"]];
    }
    
    self.lblBodyMessage.frame = CGRectMake(0, 20, self.frame.size.width, 40);
    
    [self.lblBodyMessage sizeToFit];
    
    self.lblBodyMessage.frame = CGRectMake((self.viewBg.width-self.lblBodyMessage.width)/2, self.lblHeaderMessage.y+self.lblHeaderMessage.height+2, self.lblBodyMessage.width, self.lblBodyMessage.height);

    [self.lblFooterMessage sizeToFit];
    self.lblFooterMessage.frame = CGRectMake((self.viewBg.width-self.lblFooterMessage.width)/2, self.lblBodyMessage.y+self.lblBodyMessage.height+2, self.lblFooterMessage.width, self.lblFooterMessage.height);

    
    self.viewBg.height = self.lblFooterMessage.y+self.lblFooterMessage.height+6;
    
    if (self.isCorrectAnswer) {
        [AUDIO_MANAGER playRightSound];
        //NSString *strCorrectFeedbackSound = [DATA_SERVICE dataModel].courseData.properties.correctFeedbackSound;
        //[AUDIO_MANAGER playAudioFromDocumentPath:strCorrectFeedbackSound];
        
    }
    else {
        [AUDIO_MANAGER playWrongSound];
        //NSString *strIncorrectFeedbackSound = [DATA_SERVICE dataModel].courseData.properties.incorrectFeedbackSound;
        //[AUDIO_MANAGER playAudioFromDocumentPath:strIncorrectFeedbackSound];

    }
    
}

#pragma mark - Gestures

- (void)tapGestureAction:(id)sender {
    
    DLog(@"Tap gesture action");
    
    [self fadeOutWithAnimation];
    [self.delegateFeedback feedSwipe];
}

- (void)leftSwipeAction:(id)sender {
    
    DLog(@"Swipe Left");
    
    [self closeToLeft];
    [self.delegateFeedback feedSwipe];

}

- (void)rightSwipeAction:(id)sender {
    
    DLog(@"Swipe Right");
    
    [self closeToRight];
    [self.delegateFeedback feedSwipe];
}

- (void)closeToLeft {
    
    
    [UIView animateWithDuration:0.2 animations:^() {
        
        self.viewBg.x = 0.0 - self.viewBg.width;
        [self.delegateFeedback feedbackClose:[NSNumber numberWithInt:1]];

    } completion:^(BOOL finished) {
        
        self.hidden = YES;
        [self settTheInfoViewOnTheOriginalPosition];
    }];
    
}

- (void)closeToRight {
    
    [UIView animateWithDuration:0.2 animations:^() {
        
        self.viewBg.x = 2*self.viewBg.width;
        [self.delegateFeedback feedbackClose:[NSNumber numberWithInt:0]];

    } completion:^(BOOL finished) {
        
        self.hidden = YES;
        [self settTheInfoViewOnTheOriginalPosition];
        
    }];
    
}


- (void)showWithAnimation {
    
    [(UIScrollView *)[[self superview] superview] setScrollEnabled:NO];
    [self.delegateFeedback feedbackViewWasShown:self];
    
    self.hidden = NO;
    self.viewBg.hidden = NO;
    
    [UIView animateWithDuration:0.5 animations:^() {
        
        self.userInteractionEnabled = YES;
        self.viewBg.alpha = 1.0;

        
    } completion:^(BOOL finished) {
        
//        [self.delegateFeedback feedbackClose:self];
        
    }];
    
}

- (void)fadeOutWithAnimation {

    [UIView animateWithDuration:0.5 animations:^() {

        self.viewBg.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        [self.delegateFeedback feedbackClose:[NSNumber numberWithInt:2]];
        self.hidden = YES;
        self.userInteractionEnabled = NO;


        
    }];
}

#pragma mark - Pan gesture, moving

-(void)panDetected:(UIPanGestureRecognizer *)gesture
{
    
    static CGRect originalFrame;
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        originalFrame = gesture.view.frame;
        [gesture view].alpha = 0.5;
    }
    else if (gesture.state == UIGestureRecognizerStateChanged)
    {
        CGPoint translate = [gesture translationInView:gesture.view.superview];
        CGRect newFrame = CGRectMake(fmin(gesture.view.superview.frame.size.width - originalFrame.size.width, fmax(originalFrame.origin.x + translate.x, 0.0)),
                                     fmin(gesture.view.superview.frame.size.height - originalFrame.size.height, fmax(originalFrame.origin.y + translate.y, 0.0)),
                                     originalFrame.size.width,
                                     originalFrame.size.height);
        
        gesture.view.frame = newFrame;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded) {
        
        [gesture view].alpha = 1.0;
    }
    
}

#pragma mark - Set the view on center

- (void)settTheInfoViewOnTheOriginalPosition {
    
    self.viewBg.center = self.center;
    
}

@end
