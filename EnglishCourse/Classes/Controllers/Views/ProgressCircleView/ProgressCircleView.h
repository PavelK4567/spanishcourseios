//
//  ProgressCircleView.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/16/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface ProgressCircleView : UIView

@property (nonatomic, assign) float progress;
@property (nonatomic) NSDictionary *dictProgressColor;

@property (nonatomic, weak) IBOutlet UIImageView *imgBackground;
@property (nonatomic, weak) IBOutlet KAProgressLabel *progressLabel;
@property (nonatomic) BOOL isProfileProgressCircle;

- (void)changeProgress:(float)newProgress;
- (void)changeProfileProgress:(float)newProgress;
- (void)setColorForProfileProgressCircle:(NSDictionary *)dictColorScheme;
- (void)changeDictProgressColors:(NSDictionary *)dict;

- (void)drawGUI;

@end
