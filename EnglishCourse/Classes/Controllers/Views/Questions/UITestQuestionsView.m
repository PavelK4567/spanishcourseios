//
//  UITestQuestionsScrollView.m
//  K1000
//
//  Created by Action-Item on 5/5/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "UITestQuestionsView.h"

#import "UIView+K1000Shadow.h"

@implementation UITestQuestionsView

@synthesize testCategory = _testCategory;
@synthesize question = _question;
@synthesize defaultColor = _defaultColor;
@synthesize selectedColor = _selectedColor;

- (void)awakeFromNib
{
    [super awakeFromNib];

    _questionImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _questionImageView.layer.borderWidth = 1.0f;
    _buttonsView.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
    if (!IS_IPHONE4)
    {
        _questionImageView.width += 50;
        _questionImageView.centerX = self.width * 0.5f;
        _questionTitleWithImageLabel.top += 20;
    }
    // add shadow
    for (UIView *view in _buttonsView.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton *)view;
            [button addK1000Shadow];
        }
    }
    [self reloadData];
    [self layoutData];
}

- (void)setTestCategory:(TestCategory *)testCategory
           withQuestion:(Question *)question
        andDefaultColor:(UIColor *)defaultColor
       andSelectedColor:(UIColor *)selectedColor;
{
    _testCategory = testCategory;
    _question = question;
    _defaultColor = defaultColor;
    _selectedColor = selectedColor;

    [self layoutData];
}

- (void)reloadData
{

}

- (void)layoutData
{
  if (!_question)
  {
    
    [APPSTYLE applyTitle:nil toButton:_answerButtonOne];
    [APPSTYLE applyTitle:nil toButton:_answerButtonTwo];
    [APPSTYLE applyTitle:nil toButton:_answerButtonThree];
    [APPSTYLE applyTitle:nil toButton:_answerButtonFour];
    
    _questionLabel.text = nil;
    _questionTitleNoImageLabel.text = nil;
    _questionTitleWithImageLabel.text = nil;
    _questionImageView.image = nil;
    return;
  }
  
  [APPSTYLE applyTitle:[[_question.answers objectAtIndex:_answerButtonOne.tag] text] toButton:_answerButtonOne];
  [APPSTYLE applyTitle:[[_question.answers objectAtIndex:_answerButtonTwo.tag] text] toButton:_answerButtonTwo];
  [APPSTYLE applyTitle:[[_question.answers objectAtIndex:_answerButtonThree.tag] text] toButton:_answerButtonThree];
  [APPSTYLE applyTitle:[[_question.answers objectAtIndex:_answerButtonFour.tag] text] toButton:_answerButtonFour];
  
    
    [_answerButtonOne.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_answerButtonTwo.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_answerButtonThree.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_answerButtonFour.titleLabel setAdjustsFontSizeToFitWidth:YES];
  
  [self resetButtonBackgroundColor:_answerButtonOne];
  [self resetButtonBackgroundColor:_answerButtonTwo];
  [self resetButtonBackgroundColor:_answerButtonThree];
  [self resetButtonBackgroundColor:_answerButtonFour];
  
  if (_testCategory.categoryType == categoryTypePics)
  {
    _questionNoImageView.hidden = YES;
    _questionWithImageView.hidden = NO;
    _questionTitleWithImageLabel.text = _testCategory.questionGenericTitle;
    [_questionImageView sd_setImageWithURL:_question.imageURL];
  }
  else
  {
    _questionWithImageView.hidden = YES;
    _questionNoImageView.hidden = NO;
    _questionLabel.text = _question.question;
    _questionTitleNoImageLabel.text = _testCategory.questionGenericTitle;
      if([_question.question length]>35){
          [_questionLabel setAdjustsFontSizeToFitWidth:YES];
          [_questionLabel setTextAlignment:NSTextAlignmentCenter];
          
      }
  }
}

- (void)resetButtonBackgroundColor:(UIButton *)button
{
    button.backgroundColor = _defaultColor;
}

- (void)highLightAnswerAndUnHighLightNonSelected:(UIButton *)chosenAnswerButton
{
    NSMutableArray *buttonsArray = [[NSMutableArray alloc] init];
    for (UIView *view in _buttonsView.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton *)view;
            [buttonsArray addObject:button];
            button.selected = NO;

            [self resetButtonBackgroundColor:button];
        }
    }

    chosenAnswerButton.selected = YES;
    chosenAnswerButton.backgroundColor = _selectedColor;
    _question.userSelectedAnswer = chosenAnswerButton.tag;
}

/* UIButton Delegates */

- (IBAction)answerButtonClicked:(UIButton *)button
{
    [self highLightAnswerAndUnHighLightNonSelected:button];

    if ([self.delegate respondsToSelector:@selector(testQuestionsView:answerSelected:)])
        [self.delegate testQuestionsView:self answerSelected:button.tag];
}

/* End */

@end
