//
//  MultipleChoiceImagePracticeScreen.m
//  ECMultiImage
//
//  Created by Dimitar Shopovski on 11/19/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "MultipleChoiceImagePracticeScreen.h"

@implementation MultipleChoiceImagePracticeScreen

@synthesize width;


- (void)initializeElements {
    
    _arrayPossibleAnswers = [[(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo wordMultipleChoiceImagesArray] mutableCopy];

    InteractiveImageAnswer *interactiveImage;
    
    width = 133;
    
    for (int i=0; i<_arrayPossibleAnswers.count; i++) {
        
        interactiveImage = [[InteractiveImageAnswer alloc] initWithFrame:CGRectMake(0, 0, width, width)];
        interactiveImage.center = self.center;
        interactiveImage.tag = i;
        interactiveImage.delegate = self;
        interactiveImage.answerInfo = [_arrayPossibleAnswers objectAtIndex:i];
        [interactiveImage drawGUIElements];
        [interactiveImage bindData];

        [self addSubview:interactiveImage];
        
        [self.arrayAnswers addObject:interactiveImage];
        
    }
    
    [self showTheQuestion];
    [self drawGUI];
}

- (void)drawGUI {
    
    NSMutableArray *arrayAnswers = [NSMutableArray arrayWithArray:self.arrayAnswers];
    
    InteractiveImageAnswer *interactiveImage;
    float yCenterPosition = 0.0;
    
    if (arrayAnswers.count == 3) {
        
        float yPos = self.yPosAnswers;
        
        for (int i = 0; i<arrayAnswers.count; i++) {
            
            interactiveImage = [arrayAnswers objectAtIndex:i];
            
            if (i==0) {
                
                interactiveImage.frame = CGRectMake(LM_WIDTH_INSTACE/2-width/2, yPos, width, width);
                yPos+=(width+17);
                
            }
            else {
                
                interactiveImage.frame = CGRectMake(17+i%2*(width+17), yPos, width, width);
                yCenterPosition = yPos+width;
            }
            if (yCenterPosition + width/2 > LM_SCROLL_HEIGHT) {
                
                [self setContentSize:CGSizeMake(LM_WIDTH_INSTACE, yCenterPosition + 20)];
            }
            
        }
        
    }
    else {
        
        for (int i = 0; i<arrayAnswers.count; i++) {
            
            interactiveImage = [arrayAnswers objectAtIndex:i];
            
            interactiveImage.frame = CGRectMake(17+i%2*(width+17), self.yPosAnswers+i/2*(width+17), width, width);
            yCenterPosition = self.yPosAnswers+i/2*(width+17)+width;
            
            if (yCenterPosition + width/2 > LM_SCROLL_HEIGHT) {
                
                [self setContentSize:CGSizeMake(LM_WIDTH_INSTACE, yCenterPosition + 20)];
            }
            
        }
    }
    
    //    self.lblQuestion.text = [self.dictPracticeInfo objectForKey:@"question"];
    
    self.feedbackView.correctFeedback = [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo correctFeedback];
    self.feedbackView.incorrectFeedback = [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo incorrectFeedback];

}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.nPosibleAttempts = 2;
        self.arrayAnswers = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}





#pragma mark - Show the question after next button

- (void)showTheQuestion {
    
    NSString *strQuestion = [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo question];
    
    [self.viewTitleLabel setTitleOftheInstance:strQuestion];
    
    self.yPosAnswers = self.viewTitleLabel.frame.origin.y + self.viewTitleLabel.frame.size.height + 10;
    
//    [self layoutTheAnswers:self.arrayAnswers];
    
    if ([(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo audio]) {
        
        if (self.btnSound == nil) {
            
            self.btnSound = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.btnSound setFrame:CGRectMake(3, 3, 30, 34)];
            [self.btnSound setImage:[UIImage imageNamed:@"ic_small_sound.png"] forState:UIControlStateNormal];
            [self.btnSound addTarget:self action:@selector(playSoundQuestion:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:self.btnSound];
        }
    }
    
}


- (void)translationWasSelected:(id)sender {
    
    InteractiveImageAnswer *interactiveAnswer = (InteractiveImageAnswer *)sender;
    self.currentActiveAnswer = interactiveAnswer;
    
    [interactiveAnswer showTranslationToNativeLanguage];
    
    [self hideActiveTranslation];
    
}

- (void)answerWasSelected:(id)sender {
    
    if (self.nPosibleAttempts == 0 || self.isCompleted) {
        return;
    }
    
    
    InteractiveImageAnswer *interactiveAnswer = (InteractiveImageAnswer *)sender;
    
    self.currentActiveAnswer = interactiveAnswer;
    
    [self hideAllActiveTranslations];
    
    if ([(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo answer] == [(WordMultipleChoiceImage *)[interactiveAnswer answerInfo] orderNumber]) {
        
        self.isCompleted = YES;
        [self bringSubviewToFront:self.feedbackView];
        [interactiveAnswer setStateOfTheAnswer:YES];
       
        self.feedbackView.correctFeedback = [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo correctFeedback];
        [self.feedbackView setInformation:YES];

        [self.feedbackView showWithAnimation];
        
        if (self.nPosibleAttempts == 2)
            [self.vehicleDelegate addPoints:kParam010 isLastAnswer:YES];
        else if (self.nPosibleAttempts == 1)
            [self.vehicleDelegate addPoints:kParam011 isLastAnswer:YES];
        
    }
    else {
    
        [interactiveAnswer setStateOfTheAnswer:NO];
        [AUDIO_MANAGER setDelegate:nil];
        [AUDIO_MANAGER playWrongSound];
        
        if (self.nPosibleAttempts == 1) {
            
            [self bringSubviewToFront:self.feedbackView];
            
            [self performSelector:@selector(markTheRightAnswer) withObject:nil afterDelay:0.5];
            [self.feedbackView setInformation:NO];
            self.feedbackView.correctFeedback = [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo incorrectFeedback];

            [self.feedbackView showWithAnimation];
            self.isCompleted = YES;
            
            [self.vehicleDelegate addPoints:kParam012 isLastAnswer:NO];
            [self.vehicleDelegate finishInstanceWithIncorrectAnswer];

        }
        else {
            
            self.isCompleted = YES;
            [self performSelector:@selector(resetFirstAnswer) withObject:nil afterDelay:2.0];
        }
        
        self.nPosibleAttempts--;

    }
    
}

#pragma mark - Touches

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideAllActiveTranslations];
    
}

#pragma mark - Custom function hideActiveTranslation

- (void)hideActiveTranslation {
    
    for (InteractiveImageAnswer *interactiveImage in self.subviews) {
        
        if ([interactiveImage isKindOfClass:[InteractiveImageAnswer class]] && self.currentActiveAnswer != interactiveImage) {
            
            [interactiveImage hideTranslationBallon];
        }
    }
}

- (void)hideAllActiveTranslations {
    
    for (InteractiveImageAnswer *interactiveImage in self.subviews) {
        
        if ([interactiveImage isKindOfClass:[InteractiveImageAnswer class]]) {
            
            [interactiveImage hideTranslationBallon];
        }
    }
}


#pragma mark - Mark Correct answer

- (void)markTheRightAnswer {
    
    for (InteractiveImageAnswer *answer in self.arrayAnswers) {
        
        if ([(WordMultipleChoiceImage *)answer.answerInfo orderNumber] == [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo answer]) {
            
            [answer setStateOfTheAnswer:YES];
        }
    }
    
    
}

#pragma mark - Reset the instance

- (void)resetTheInstance {
    
    self.isCompleted = NO;
    self.nPosibleAttempts = 2;
    self.currentActiveAnswer = nil;
    
    for (InteractiveImageAnswer *answers in self.arrayAnswers) {
        
        [answers backTheAnswerInNormalState];
        
    }
    
}

- (void)showLayoutForCompletedInstance {
    
    for (InteractiveImageAnswer *answer in self.arrayAnswers) {
        
        if ([(WordMultipleChoiceImage *)answer.answerInfo orderNumber] == [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo answer]) {
            
            [answer setStateOfTheAnswer:YES];
        }
        else {
            [answer backTheAnswerInNormalState];
        }
    }

}

#pragma mark - Play sound

- (IBAction)playSoundQuestion:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [btn setHighlighted:NO];
    [btn setSelected:NO];

    DLog(@"playSoundQuestion");
    if (!self.isCompleted && [(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo audio])
        [AUDIO_MANAGER playAudioFromDocumentPath:[(MultipleChoiceImagePracticeObject *)self.dictVehicleInfo audio] componentSender:self.btnSound];
    
}

#pragma mark - Implement feedback view delegate

- (void)feedbackClose:(id)sender {
    
    [self showLayoutForCompletedInstance];
    [self.vehicleDelegate closeFeedbackandSwipePage:[sender intValue]];

}

#pragma mark - Reset first wrong answer

- (void)resetFirstAnswer {
    
    self.isCompleted = NO;
    [self.currentActiveAnswer backTheAnswerInNormalState];
    
}

@end
