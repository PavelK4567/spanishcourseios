//
//  MultipleChoiceImageExposureScreen.m
//  ECMultiImage
//
//  Created by Dimitar Shopovski on 11/18/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "MultipleChoiceImageExposureScreen.h"

@implementation MultipleChoiceImageExposureScreen
@synthesize width, delegate, widthBig, yPosAnswers;


- (void)initializeElements {
    
    CGRect rect = self.frame;
    
    float yCenterPosition = 0, maxPosition = 0;
    
    [self.viewTitleLabel setTitleOftheInstance:[(BaseInstance *)self.dictVehicleInfo title]];
    
    self.arrayInteractiveImages = [[(MultipleChoiceImage *)self.dictVehicleInfo wordMultipleChoiceImagesArray] mutableCopy];
    
    //add answers
    InteractiveImageAnswer *interactiveImage;
    
    width = 133;
    widthBig = 272;
    
    float xCenterPos = (rect.size.width)/2-(widthBig/2);
    float yCenterPos = rect.size.height/2-widthBig/2-5;
    
    self.rectAnswerCenter = CGRectMake(xCenterPos, yCenterPos, widthBig, widthBig);
    self.rectAnswerCenterSmall = CGRectMake((rect.size.width)/2-(width/2), rect.size.height/2-width/2-5, width, width);
    
    for (int i=0; i<self.arrayInteractiveImages.count; i++) {
        
        interactiveImage = [[InteractiveImageAnswer alloc] initWithFrame:self.rectAnswerCenterSmall];
        interactiveImage.tag = i;
        interactiveImage.alpha = 0.0;
        interactiveImage.delegate = self;
        interactiveImage.isExposureAnswer = YES;
        interactiveImage.rectCentralPosition = self.rectAnswerCenter;
        interactiveImage.userInteractionEnabled = YES;
        interactiveImage.answerInfo = [self.arrayInteractiveImages objectAtIndex:i];
        [interactiveImage drawGUIElements];
        [interactiveImage bindData];
        
        if (self.arrayInteractiveImages.count == 3) {
            
            self.yPosAnswers = self.viewTitleLabel.height+10;
            
            float yPos = self.yPosAnswers;
            
            if (i==0) {
                
                interactiveImage.rectPosition = CGRectMake(LM_WIDTH_INSTACE/2-width/2, yPos, width, width);
                yPos+=(width+17);
                
            }
            else {
                
                yPos = self.yPosAnswers+width+17;
                interactiveImage.rectPosition = CGRectMake(17+(i-1)%2*(width+17), yPos, width, width);
            }
            
        }
        else {
            
            
            if (self.arrayInteractiveImages.count == 4) {

                self.yPosAnswers = self.viewTitleLabel.height + 18.0;
                
            }
            else {

                self.yPosAnswers = self.viewTitleLabel.height + 75.0;
                
            }
            
            interactiveImage.rectPosition = CGRectMake(17+i%2*(width+17), self.yPosAnswers+i/2*(width+17), width, width);
            
        }
        
        [self addSubview:interactiveImage];
        
        [self.arrayAnswers addObject:interactiveImage];
        
        yCenterPosition = self.yPosAnswers+i/2*(width+17) + width;
        
        if (yCenterPosition > maxPosition) {
            
            maxPosition = yCenterPosition;
        }
        
        
        if (maxPosition + 20 > LM_SCROLL_HEIGHT) {
            
            [self setContentSize:CGSizeMake(LM_WIDTH_INSTACE, maxPosition + 20)];
        }
        
    }
    
    [self setAnswersOnTheStartPositions];


}

- (void)setAnswersOnTheStartPositions {
    
    for (InteractiveImageAnswer *interactiveImage in self.arrayAnswers) {
        
        interactiveImage.frame = self.rectAnswerCenterSmall;
        [interactiveImage backTheAnswerInNormalState];
        
    }
    
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.arrayAnswers = [[NSMutableArray alloc] init];        
        
        yPosAnswers = 60;
        
    }
    
    return self;
}


- (void)startAnimationWithAnswers:(BOOL)isFirstTime {
    
    if (self.isCompleted) {
        return;
    }
    
    self.animationInProgress = YES;
    
    static int currentElement = 0;
    
    if (isFirstTime) {
        
        [self setAnswersOnTheStartPositions];
        //[self.exposureDelegate disableScrolling:self];
        currentElement = 0;
    }

    __block InteractiveImageAnswer *interactiveImage;
    __weak MultipleChoiceImageExposureScreen *weakSelf = self;
    
    
    [UIView animateWithDuration:1.5 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
        interactiveImage = (InteractiveImageAnswer *)[self.arrayAnswers objectAtIndex:currentElement];
        interactiveImage.alpha = 1.0;
        interactiveImage.transform = CGAffineTransformMakeScale(1.8, 1.8);
        //[self bringSubviewToFront:interactiveImage];
        [interactiveImage playTheSound];
        
    } completion:^(BOOL finished) {
        
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
            interactiveImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            interactiveImage.frame = interactiveImage.rectPosition;
            
        } completion:^(BOOL finished) {
            
            currentElement++;
            
            if (currentElement < [self.arrayAnswers count]) {
                
                [weakSelf startAnimationWithAnswers:NO];
            }
            else {
                
                self.animationInProgress = NO;
                self.isCompleted = YES;
                [self.vehicleDelegate enableScrolling:self];
            }
            
        }];
        
        
    }];

}

#pragma mark - Animate one element

- (void)animateImage:(InteractiveImageAnswer *)interactiveImage {
    
    double duration = [AUDIO_MANAGER getSoundDuration:[interactiveImage.answerInfo sound]];
    
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
        interactiveImage.alpha = 1.0;
        interactiveImage.answerAnimationInProgress = YES;
        interactiveImage.transform = CGAffineTransformMakeScale(1.8, 1.8);
        [interactiveImage playTheSound];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
            interactiveImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            interactiveImage.frame = interactiveImage.rectPosition;
            interactiveImage.answerAnimationInProgress = NO;
        
        } completion:^(BOOL finished) {
            
            if (interactiveImage.tag+1 < [self.arrayAnswers count] && !self.isCompleted) {
                InteractiveImageAnswer *nextImage = [self.arrayAnswers objectAtIndex:interactiveImage.tag+1];
                [self animateImage:nextImage];
            }
            else {
                
                self.animationInProgress = NO;
                self.isCompleted = YES;
//                [self.vehicleDelegate enableScrolling:self];
            }
            
        }];
    }];
}



#pragma mark - Animate all elements

- (void)startAnimatingTheElements {
    
    self.animationInProgress = YES;
    
    InteractiveImageAnswer *nextImage = [self.arrayAnswers objectAtIndex:0];
    [self animateImage:nextImage];

}



- (void)translationWasSelected:(id)sender {
    
    InteractiveImageAnswer *interactiveAnswer = (InteractiveImageAnswer *)sender;
    self.currentActiveAnswer = interactiveAnswer;
    
    [interactiveAnswer showTranslationToNativeLanguage];
    
    [self hideActiveTranslation];
    
}

- (void)answerWasSelected:(id)sender {
    
    if (self.animationInProgress) {
        return;
    }
    
    [self hideAllActiveTranslations];
    
    InteractiveImageAnswer *interactiveAnswer = (InteractiveImageAnswer *)sender;
    self.currentActiveAnswer = interactiveAnswer;
    
    [self bringSubviewToFront:interactiveAnswer];
    //[interactiveAnswer emphasizeAnswerTo:self.rectAnswerCenter from:interactiveAnswer.frame];
    [interactiveAnswer playTheSound];
    [interactiveAnswer showTranslationToNativeLanguage];

    
}

#pragma mark - Touches

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self hideAllActiveTranslations];
    
}

#pragma mark - Custom function hideActiveTranslation

- (void)hideActiveTranslation {
    
    for (InteractiveImageAnswer *interactiveImage in self.subviews) {
        
        if ([interactiveImage isKindOfClass:[InteractiveImageAnswer class]] && self.currentActiveAnswer != interactiveImage) {
            
            [interactiveImage hideTranslationBallon];
        }
    }
}

- (void)hideAllActiveTranslations {
    
    for (InteractiveImageAnswer *interactiveImage in self.subviews) {
        
        if ([interactiveImage isKindOfClass:[InteractiveImageAnswer class]]) {
            
            [interactiveImage hideTranslationBallon];
        }
    }
}


#pragma mark - Show the question after next button

- (void)showTheQuestion {

//    NSString *strQuestion = [[(NSArray *)[self.dictExposureInfo objectForKey:@"practices"] objectAtIndex:0] objectForKey:@"question"];
    
//    self.lblQuestion.text = strQuestion;
//    self.lblInstanceTitle.alpha = 1.0;
//    self.lblInstanceTitle.numberOfLines = 0;
//    self.lblInstanceTitle.text = strQuestion;
//    [self.lblInstanceTitle sizeToFit];
//        
//    self.lblInstanceTitle.center = CGPointMake(LM_WIDTH_INSTACE/2, self.lblInstanceTitle.center.y+10);
    
//    self.yPosAnswers = self.lblInstanceTitle.frame.origin.y + self.lblInstanceTitle.frame.size.height + 10;
    
}

- (void)disableVehicleInteractions {
    
    [self.layer removeAllAnimations];
    [self showLayoutForCompletedInstance];

}

#pragma mark - Show completed instance

- (void)showLayoutForCompletedInstance {
    
    self.isCompleted = YES;
    self.currentActiveAnswer = nil;
    [self hideActiveTranslation];
//    [self disableVehicleInteractions];
    
    for (InteractiveImageAnswer *interactiveImage in self.arrayAnswers) {
        
        if (interactiveImage.answerAnimationInProgress) {
            
            interactiveImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            interactiveImage.answerAnimationInProgress = NO;

        }
        [UIView animateWithDuration:0.0 animations:^{
        
            [interactiveImage setFrame:interactiveImage.rectPosition];

        }];
        interactiveImage.alpha = 1.0;

        
    }
}

- (void)smallDelay {
    

}



@end
