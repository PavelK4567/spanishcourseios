//
//  PracticePage.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 3/18/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PracticePage : UIScrollView

@property (nonatomic, assign) BOOL isPageCompleted;

@end
