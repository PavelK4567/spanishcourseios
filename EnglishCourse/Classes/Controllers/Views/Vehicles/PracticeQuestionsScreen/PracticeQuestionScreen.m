//
//  PracticeQuestionScreen.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "PracticeQuestionScreen.h"

@implementation PracticeQuestionScreen

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.viewTitleLabel.hidden = YES;
    }
    
    return self;
    
}

- (void)drawRect:(CGRect)rect {
    
    currentPage = 0;
    
    NSArray *arrayPages = [(Practice *)self.dictVehicleInfo practiceSections];
    numberOfPages = arrayPages.count;
    
    self.pagingEnabled = YES;
    [self setContentSize:CGSizeMake(arrayPages.count*LM_WIDTH_INSTACE, LM_SCROLL_HEIGHT)];
    self.scrollEnabled = NO;
    
    PracticePage *viewPage;
    for (int i=0; i<arrayPages.count; i++) {
        
        PractiseSection *practiceSection = (PractiseSection *)[arrayPages objectAtIndex:i];
        
        viewPage = [self createPage:practiceSection atPosition:i+200];
        [self showLayoutWithAnswers:practiceSection.isLongLayout :viewPage :practiceSection];
        
        [self addSubview:viewPage];
        
    }
    
}

- (PracticePage *)createPage:(PractiseSection *)practiceSection atPosition:(NSInteger)pageIndex {
    
    PracticePage *viewPage = [[PracticePage alloc] initWithFrame:CGRectMake((pageIndex-200)*LM_WIDTH_INSTACE, 0, LM_WIDTH_INSTACE, LM_SCROLL_HEIGHT)];
    viewPage.tag = pageIndex;
    [viewPage setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *imageViewForQuestion = [[UIImageView alloc] initWithFrame:CGRectMake((LM_WIDTH_INSTACE-widthImageMultiChoiceText)/2, 0, widthImageMultiChoiceText, heightImageMultiChoiceText)];
    [imageViewForQuestion setContentMode:UIViewContentModeScaleAspectFit];
    [imageViewForQuestion setImageFromDocumentsResourceFile:[practiceSection image]];
    imageViewForQuestion.tag = 100;
    [viewPage addSubview:imageViewForQuestion];

    CustomTitleView *titleView = [[CustomTitleView alloc] initWithFrame:CGRectMake(0, 0, LM_WIDTH_INSTACE, 82)];
    [titleView setTitleOftheInstance:[practiceSection question]];
    titleView.tag = 101;
    [viewPage addSubview:titleView];
    
    viewPage.isPageCompleted = NO;
    
    return viewPage;
    
}


- (void)showLayoutWithAnswers:(BOOL)isLongLayout :(PracticePage *)viewPage :(PractiseSection *)practiceSection {
    
    CustomTitleView *titleView = (CustomTitleView *)[viewPage viewWithTag:101];
    [titleView setTitleOftheInstance:[practiceSection question]];
    
    float yPosAnswersArea = titleView.frame.origin.y + titleView.frame.size.height + 10;

    NSInteger numberOfAnswers = [[practiceSection answersArray] count];
    
    CustomVehicleButton *customButton = nil;
    
    float widthOfAnswers;
    float heightOfAnswers = 36.0;
    float xPos, yPos = yPosAnswersArea;
    float spaceBetweenAnswers = 11;
    
    Answer *answer;
    
    if (isLongLayout) {
        
        widthOfAnswers = 296.0;
        xPos = (LM_WIDTH_INSTACE-widthOfAnswers)/2;
        
        
        if (numberOfAnswers == 3) {
            //yPos = 39;
            spaceBetweenAnswers = 13;
        }
        else if (numberOfAnswers == 4) {
            
            //yPos = 49;
            spaceBetweenAnswers = 11;
        }
        
        for (int i=0; i<numberOfAnswers; i++) {
            
            answer = [[practiceSection answersArray] objectAtIndex:i];
            
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            customButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [customButton setTitleEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
            customButton.dictAnswerInfo = answer;
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
//            [self.arrayPossibleAnswers addObject:customButton];
            
            yPos+=heightOfAnswers+spaceBetweenAnswers;
        }
        
    }
    else {
        
        widthOfAnswers = (self.bounds.size.width-30)/2;
        
        if (numberOfAnswers == 3) {
            
            widthOfAnswers = 120.0;
            xPos = (LM_WIDTH_INSTACE-widthOfAnswers)/2;
            
            answer = [[practiceSection answersArray] objectAtIndex:0];
            
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            customButton.dictAnswerInfo = answer;
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
            yPos+=heightOfAnswers+15;
            xPos = (LM_WIDTH_INSTACE-2*widthOfAnswers-11)/2;
            
            answer = [[practiceSection answersArray] objectAtIndex:1];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
            xPos+=widthOfAnswers+11;
            
            answer = [[practiceSection answersArray] objectAtIndex:2];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
            
        }
        else if (numberOfAnswers == 4) {
            
            widthOfAnswers = 120.0;
            xPos = (LM_WIDTH_INSTACE-2*widthOfAnswers-15)/2;
            
            answer = [[practiceSection answersArray] objectAtIndex:0];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
            xPos+=widthOfAnswers+15;
            
            answer = [[practiceSection answersArray] objectAtIndex:1];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
            yPos+=heightOfAnswers+15;
            xPos = (LM_WIDTH_INSTACE-2*widthOfAnswers-15)/2;
            
            answer = [[practiceSection answersArray] objectAtIndex:2];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
            xPos+=widthOfAnswers+15;
            
            answer = [[practiceSection answersArray] objectAtIndex:3];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [customButton setType:YES];
            [viewPage addSubview:customButton];
            
        }
        
        yPos+=heightOfAnswers+spaceBetweenAnswers;
    }
    
    UIImageView *imgQuestion = (UIImageView *)[viewPage viewWithTag:100];
    
    imgQuestion.frame = CGRectMake((LM_WIDTH_INSTACE-widthImageMultiChoiceText)/2, yPos, widthImageMultiChoiceText, heightImageMultiChoiceText);
    
    if (yPos + heightImageMultiChoiceText > LM_SCROLL_HEIGHT) {
        
        [viewPage setContentSize:CGSizeMake(LM_WIDTH_INSTACE, yPos + heightImageMultiChoiceText + 20)];
    }
}

#pragma mark - Implementation delegate custom button

- (void)customButtonSelected:(id)sender {

    if (self.isCompleted) {
        return;
    }
    
    [self.vehicleDelegate setIsPracticeVehicleActive:YES];
    
    CustomVehicleButton *cb = (CustomVehicleButton *)sender;
    
    PracticePage *practicePage = (PracticePage *)[cb superview];
    if (practicePage.isPageCompleted) {
        return;
    }
    
    if (practicePage == nil) {
        return;
    }
    
    if (practicePage.isPageCompleted) {
        return;
    }
    
    
    if ([cb.dictAnswerInfo isCorrect]) {
        
//        self.isCompleted = YES;
        [cb setStateOfTheAnswer:YES];
        [AUDIO_MANAGER setDelegate:nil];
        [AUDIO_MANAGER playRightSound];

        [self.vehicleDelegate addPoints:kParam010 forPage:(int)currentPage status:1];
        
    }
    else {
        
        [cb setStateOfTheAnswer:NO];
        
        [AUDIO_MANAGER setDelegate:nil];
        [AUDIO_MANAGER playWrongSound];
        
//        self.isCompleted = YES;
        [self performSelector:@selector(markTheRightAnswer) withObject:nil afterDelay:0.5];
        
        [self.vehicleDelegate addPoints:kParam012 forPage:(int)currentPage status:2];
        [self.vehicleDelegate finishInstanceWithIncorrectAnswer];
    
    }
    
    practicePage.isPageCompleted = YES;
    [self performSelector:@selector(gotoNextPracticeQuestion) withObject:nil afterDelay:1.0];

}

- (void)gotoNextPracticeQuestion {

    
    if (currentPage+1 < [[(Practice *)self.dictVehicleInfo practiceSections] count]) {
    
        [self setContentOffset:CGPointMake((currentPage+1)*LM_WIDTH_INSTACE, 0) animated:YES];
        currentPage++;
        
        
    }
    else if (currentPage+1 == numberOfPages) {
        
        [self.vehicleDelegate changeNavigationButtons:nil];
    }
    
    [self.vehicleDelegate setIsPracticeVehicleActive:NO];
//    [self.vehicleDelegate gotoNextPage];
}


#pragma mark - States

- (void)changeTheState:(id)sender {
    
    CustomVehicleButton *cb = (CustomVehicleButton *)sender;
    [cb setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}


- (void)markTheRightAnswer {
    
//    NSArray *arrayPages = [(Practice *)self.dictVehicleInfo practiceSections];
//    PractiseSection *practice = [arrayPages objectAtIndex:currentPage];
    
    UIView *viewPage = (UIView *)[self viewWithTag:currentPage+200];
    
    for (CustomVehicleButton *customButton in viewPage.subviews) {
        
        if ([customButton isKindOfClass:[CustomVehicleButton class]])
        if ([customButton.dictAnswerInfo isCorrect]) {
            
            [customButton setStateOfTheAnswer:YES];
        }
        
    }
}

#pragma mark - Reset the instance

- (void)resetTheInstance {
    
    self.isCompleted = NO;
    
//    NSArray *arrayPages = [(Practice *)self.dictVehicleInfo practiceSections];
//    PractiseSection *practice = [arrayPages objectAtIndex:currentPage];
    
    for (PracticePage *viewPage in self.subviews) {
        
        if (![viewPage isKindOfClass:[PracticePage class]]) {
            continue;
        }
        viewPage.isPageCompleted = NO;
        for (CustomVehicleButton *customButton in viewPage.subviews) {
            
            if ([customButton isKindOfClass:[CustomVehicleButton class]])
                [customButton resetToNormalState];
            
        }

    }
    
    currentPage = 0;
    [self scrollRectToVisible:CGRectMake(0, 0, LM_WIDTH_INSTACE, LM_SCROLL_HEIGHT) animated:NO];
    
}

- (void)showLayoutForCompletedInstance {
    
//    NSArray *arrayPages = [(Practice *)self.dictVehicleInfo practiceSections];
//    PractiseSection *practice = [arrayPages objectAtIndex:currentPage];
    
    UIView *viewPage = (UIView *)[self viewWithTag:currentPage+200];
    
    for (CustomVehicleButton *customButton in viewPage.subviews) {
        
        if ([customButton isKindOfClass:[CustomVehicleButton class]])
            [customButton resetToNormalState];
        
    }
    [self markTheRightAnswer];
}


@end
