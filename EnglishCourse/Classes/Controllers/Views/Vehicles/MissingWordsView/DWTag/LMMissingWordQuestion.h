//
//  LMMissingWordQuestion.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "DWTagList.h"
#import "LMTagView.h"
#import "LMHelper.h"

@interface LMMissingWordQuestion : DWTagList<DWTagViewDelegate>

@property(nonatomic,strong) NSArray *allWords;
@property(nonatomic,strong) NSArray *correctWordsIndexes;

- (void)layoutSelection:(NSInteger)index;
- (void)layoutCorrectTags;
- (void)setCorrectWordsIndexes:(NSArray *)correctWordsIndexes;
- (void)resetSelection;

@end
