//
//  WordListFlowLayout.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/18/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMFlowTagList.h"
#import "LMFlowTagView.h"
#import "QuestionListInterface.h"
#import "WordListInterface.h"

@interface WordListFlowLayout : UIView <DWTagListDelegate,WordListDelegate>

@property (nonatomic, strong) LMFlowTagList *tagList;
@property (nonatomic, strong) NSArray *translations;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSString *placeholderWord;
@property (nonatomic, strong) NSMutableArray *wordList;
@property (nonatomic) CGPoint placeholderPoint;
@property (nonatomic) NSInteger wordIndex;
@property (nonatomic) NSInteger attemptsCount;
@property (nonatomic) BOOL isFinish;
@property (nonatomic, weak) id<WordListDelegate>delegate;

- (void)loadGUI;
@end
