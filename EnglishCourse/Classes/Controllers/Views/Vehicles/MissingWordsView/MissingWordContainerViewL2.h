//
//  MissingWordContainerViewL2.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/16/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionListInterface.h"
#import "ECVehicleDisplay.h"
#import "WordListFlowLayout.h"
#import "MissingWordView.h"
#import "WordListInterface.h"
#import "FillTheMissingWords.h"

@interface MissingWordContainerViewL2 : ECVehicleDisplay<QuestionListDelegate,DWTagListDelegate,WordListDelegate>
{
    WordListFlowLayout *wordListFlowView;
    UIScrollView *questionsScrollView;
    UIView *titleView;
    FillTheMissingWords *fillTheMissingWords;
    NSMutableArray *words;
    NSMutableArray *translations;
    NSString *answer;
    NSString *question;
    NSString *title;
    UILabel *titleLabel;
}

- (void)loadData;

@end
