//
//  MissingWordContainerViewL1.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/13/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MissingWordContainerViewL1.h"
#import "Character.h"
#import "SegmentMissingWords.h"
#import "WordMissingWords.h"

@implementation MissingWordContainerViewL1


#pragma mark - loadDataForIndex

- (void)loadDataForIndex:(NSInteger)index
{
    translations = [NSMutableArray new];
    words = [NSMutableArray new];
    
    fillTheMissingWords =  ((FillTheMissingWords *)self.dictVehicleInfo);
    SegmentMissingWords *segmentWords = [fillTheMissingWords.segmentsArray objectAtIndex:index];
    answer = [segmentWords.translation stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    question = [segmentWords.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    title = fillTheMissingWords.title;
    
    for(WordMissingWords *word in segmentWords.wordsArray) {
        [words addObject:word.word];
        [translations addObject:word.translation];
    }
    
    randomWords = [LMHelper shuffleArray:words];
    randomTranslations = [NSMutableArray new];
    
    for(NSString *word in randomWords) {
        if([words containsObject:word]) {
            NSInteger index = [words indexOfObject: word];
            [randomTranslations addObject:translations[index]];
        }
    }
    
    if(index == 0) {
        
        correctFeedback = YES;
        [self loadGUI];
    }
    else {
        
        [wordListView setTranslationsList:randomTranslations];
        [wordListView setWordList:randomWords];
    }

}


#pragma mark - loadGUI

- (void)loadGUI
{
    
    wordListView = [WordListView new];
    questionsScrollView = [UIScrollView new];
   
    [questionsScrollView setDelegate:self];
    
    if(!titleView) {
        titleView = [MissingWordTitleView getFromNib];
        [self addSubview:titleView];
    }
    
    [self addSubview:questionsScrollView];
    [self addSubview:wordListView];
    
    [self bindGUI];
    [self layoutGUI];
    
    if(!self.renderCompletedViews)
        [self insertNewQuestion];
    else
        [self renderFinishedGame];
}


#pragma mark - bindGUI

- (void)bindGUI
{

    [wordListView setWordList:randomWords];
    [wordListView setTranslationsList:randomTranslations];
    
    Character *firstPerson  = [DATA_SERVICE findCharacterByID:fillTheMissingWords.leftCharacterID];
    Character *secondPerson = [DATA_SERVICE findCharacterByID:fillTheMissingWords.rightCharacterID];

    [titleView setFirstPersonItem:firstPerson];
    [titleView setSecondPersonItem:secondPerson];
    
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    [self layoutTitleView];
    [self layoutQuestionsView];
    [self layoutWordsView];
    [self loadStyles];
}

- (void)layoutTitleView
{
    Character *person  = [DATA_SERVICE findCharacterByID:fillTheMissingWords.leftCharacterID];
    
    [self.viewTitleLabel setTitleOftheInstance:title];
    [titleView setY:self.viewTitleLabel.height];
    [titleView setWidth:self.width andHeight:person.image ? 100 : 0];
    [titleView bindGUI];
}

- (void)layoutWordsView
{
    [wordListView setWidth:kScrollViewWidth];
    [wordListView setHeight:kScrollViewHeight];
    [wordListView setX:kZeroPointValue];
    [wordListView setY:(self.height - wordListView.height - kWordsSpacing)];
    [wordListView setDelegate:self];
    [wordListView setAttemptsCount:0];   
}

- (void)layoutQuestionsView
{
    [questionsScrollView setWidth:kScrollViewWidth];
    [questionsScrollView setHeight:(self.height - (self.viewTitleLabel.height + kScrollViewHeight + titleView.height + kWordsSpacing * 2))];
    [questionsScrollView setX:kZeroPointValue];
    [questionsScrollView setY:titleView.height + self.viewTitleLabel.height];
}


#pragma mark - layoutGUI

- (void)loadStyles
{
    [questionsScrollView setBackgroundColor:[UIColor whiteColor]];
}


#pragma mark - insert new question view

- (void)insertNewQuestion
{
    MissingWordView *questionView = [[MissingWordView alloc] initWithFrame:CGRectMake(5, kZeroPointValue, self.width - 10,80)];
    
    [questionView setQuestionString:question];
    [questionView setAnswer:answer];
    [questionView setDelegate:self];
    [questionView setCorrectWord:correctWord];
    
    if(questionIndex > 0) {
        [wordListView removeFromSuperview];
        wordListView = [WordListView new];
        
        [self addSubview:wordListView];
        [self bindGUI];
        [self layoutGUI];
    }
    
    if([self getLastQuestionView]) {
        
        MissingWordView *lastQuestionView = [self getLastQuestionView];
        [questionView setY:lastQuestionView.y];
        [questionView placeCenterAndBelowView:lastQuestionView withPadding:5];
        [questionView setDelegate:self];
    }

    [questionsScrollView addSubview:questionView];
    [questionsScrollView setContentSize:CGSizeMake(kScrollViewWidth,[self scrollViewContentHeight] + 10 + kWordsSpacing)];
    
    for (int i = 0; i < questionView.tagList.subviews.count; i++) {
        if([questionView.tagList.subviews[i] isKindOfClass:[LMTagView class]]) {

            if([questionView.placeholderArray[i] isEqualToString:kUnderlineWord]) {
                [questionView selectedTag:questionView.placeholderArray[i] tagIndex:i];
            }
        }
    }
    
    
    MissingWordView *lastQuestionView = [self getLastQuestionView];
    [questionsScrollView scrollRectToVisible:lastQuestionView.frame animated:YES];
}


#pragma mark - getQuestionHeight

- (CGFloat)getQuestionHeight
{
    NSArray *subviews = [[questionsScrollView.subviews lastObject] subviews];
    return [[subviews lastObject] frame].size.height;
}


#pragma mark - getLastQuestionView

- (MissingWordView *)getLastQuestionView
{
    MissingWordView *lastView;
    for(id view in questionsScrollView.subviews) {
            
        if([view isKindOfClass:[MissingWordView class]]) {
            lastView = view;
        }
    }
    
    return lastView;
}


#pragma mark - Missing word delegate

- (void)selectedPlaceholderAtPoint:(CGPoint)point
                              word:(NSString *)word
                         wordIndex:(NSInteger)index
{
    [wordListView setPlaceholderPoint:point];
    [wordListView setPlaceholderWord:word];
    [wordListView setWordIndex:index];
    [wordListView setAttemptsCount:0];
}

- (void)feedback:(BOOL)status
{
    correctFeedback = status;
}


#pragma mark - gameOver

- (void)gameOver:(BOOL)status
{
    if(status && (fillTheMissingWords.segmentsArray.count - 1 <= questionIndex)) {

        if(!correctFeedback) {
            for (UIView *view in questionsScrollView.subviews) {
            
                if([view isKindOfClass:[MissingWordView class]]) {
            
                    [(MissingWordView *)view resizeAllViews];
                }
            }
        
            [self reorderViews];
        }
        if(!self.renderCompletedViews && correctFeedback) {
            [self finishAnimated:YES];
            [self bringSubviewToFront:self.feedbackView];
            self.feedbackView.correctFeedback = [(FillTheMissingWords *)self.dictVehicleInfo correctFeedback];
            [self.feedbackView setInformation:YES];
            [self.feedbackView showWithAnimation];
        }
        else {
            [self finishAnimated:NO];
        }
            [self.vehicleDelegate changeNavigationButtons:nil];
    }
    
    else {
        
        questionIndex ++;
        [self loadDataForIndex:questionIndex];

        if(!self.renderCompletedViews)
            [self insertNewQuestion];
    }
    
  
}


#pragma mark - correctWordIndex

- (void)correctWordIndex:(NSInteger)index
{
    MissingWordView *questionView = [self getLastQuestionView];
    [questionView addCorrectWordIndex:index];
    [questionView.tagList resetSelection];
    [questionsScrollView setContentSize:CGSizeMake(kScrollViewWidth,[self scrollViewContentHeight] + 60 +kWordsSpacing)];
    

}


#pragma mark - answerPoints

- (void)answerPoints:(int)points
{
    if (fillTheMissingWords.segmentsArray.count - 1 <= questionIndex) {
        self.isCompleted = YES;
        if(correctFeedback)
            [self.vehicleDelegate addPoints:points isLastAnswer:YES];
        else
            [self.vehicleDelegate addPoints:points isLastAnswer:NO];
    }
    else {
        [self.vehicleDelegate addPoints:points isLastAnswer:NO];
    }
}


#pragma mark - finish game

- (void)finishAnimated:(BOOL)animated
{
    CGFloat newHeight = questionsScrollView.height + wordListView.height;
    
    if(animated) {
        [UIView animateWithDuration:1.0 animations:^{
            questionsScrollView.height = newHeight;
            wordListView.alpha = 0;
            [wordListView setHidden:YES];
        }];
    }
    else
    {
        questionsScrollView.height = newHeight;
        wordListView.alpha = 0;
        [wordListView setHidden:YES];
    }
    
    CGFloat contentHeight = [self scrollViewContentHeight] + kWordsSpacing + 100;

    [questionsScrollView setContentSize:CGSizeMake(questionsScrollView.width, contentHeight)];

}


#pragma mark - scrollViewContentHeight

- (CGFloat)scrollViewContentHeight
{
    CGFloat height = 0.0;
    
    for (UIView *view in questionsScrollView.subviews) {
       
        for(id subview in view.subviews) {
            height += [subview frame].size.height;
        }
    }
    
    return height;

}


#pragma mark - reorderViews

- (void)reorderViews
{
    NSArray *allViews = [questionsScrollView subviews];
    
    [[questionsScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [questionsScrollView setContentSize:CGSizeZero];
    
    for(int i = 0; i < allViews.count; i++) {
        
        MissingWordView *view = allViews[i];

        if(i != 0) {
            
            MissingWordView *lastView = [self getLastQuestionView];
            [lastView resizeAllViews];
            view.y = lastView.y + lastView.height;
            view.x = 5;
            view.width = self.width - 10;
            [questionsScrollView addSubview:view];
            [view placeBelowView:lastView withPadding:10];
        }
        
        view.x = 5;
        view.width = self.width - 10;
        
        if([view isKindOfClass:[MissingWordView class]]) {
            [view resizeAllViews];
        }
        
        [questionsScrollView addSubview:view];
    
    }
        [questionsScrollView setContentSize:CGSizeMake(kScrollViewWidth,[self scrollViewContentHeight] + kWordsSpacing + 50)];
    
}

- (void)renderFinishedGame
{

    FillTheMissingWords *missingWords =  ((FillTheMissingWords *)self.dictVehicleInfo);
    
    for(int i = 0; i < missingWords.segmentsArray.count; i++)
    {
        
        SegmentMissingWords *segmentWords = [missingWords.segmentsArray objectAtIndex:i];
        answer = [segmentWords.translation stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        question = [segmentWords.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
     
        [self insertNewQuestion];
        
    }
    
    int subviewIndex = 0;
    for (UIView *view in questionsScrollView.subviews) {
        if([view isKindOfClass:[MissingWordView class]]) {
            [(MissingWordView *)view setCorrectWord:[self getCorrectWordForQuestionAt:subviewIndex]];
            [(MissingWordView *)view correctIndexOfWord];
            subviewIndex++;
        }
    }

    [self reorderViews];
    [questionsScrollView setContentSize:CGSizeMake(kScrollViewWidth,[self scrollViewContentHeight] + 200 + kWordsSpacing)];
    [self hideWordsView];
}


- (NSString *)getCorrectWordForQuestionAt:(NSInteger)index
{

    FillTheMissingWords *missingWords =  ((FillTheMissingWords *)self.dictVehicleInfo);
    SegmentMissingWords *segmentWords = [missingWords.segmentsArray objectAtIndex:index];
    
    for(WordMissingWords *word in segmentWords.wordsArray) {
        
        return word.word;
    }
    
    return @"";
}



#pragma mark - hideWordsView

- (void)hideWordsView
{

    for (id view in self.subviews) {
        if([view isKindOfClass:[WordListView class]])
           [view setHidden:YES];
    }
}


- (void)feedSwipe
{
    for (UIView *view in questionsScrollView.subviews) {
        
        if([view isKindOfClass:[MissingWordView class]]) {
            
            [(MissingWordView *)view resizeAllViews];
        }
    }
    
    [self reorderViews];
}

-(void)showLayoutForCompletedInstance
{
    
    [self setRenderCompletedViews:YES];
    [self loadDataForIndex:0];
}

- (void)resetTheInstance
{
    questionIndex = 0;
    [[questionsScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [questionsScrollView setContentSize:CGSizeZero];
    [wordListView setWordList:nil];
    [wordListView setTranslationsList:nil];
    [self layoutGUI];
    [self setRenderCompletedViews:NO];
    [self loadDataForIndex:0];
    [self layoutGUI];
}

#pragma -
@end
