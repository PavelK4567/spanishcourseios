//
//  ECVehicleDisplay.h
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/13/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedbackView.h"
#import "CustomTitleView.h"

#define LM_WIDTH_INSTACE ([UIScreen mainScreen].bounds.size.width)
#define LM_HEIGHT_INSTACE ([UIScreen mainScreen].bounds.size.height)

@protocol ECVehicleDisplayDelegate <NSObject>

@required

- (void)updatePoints:(id)sender;

- (void)addPoints:(int)points isLastAnswer:(BOOL)isLastAnswer;
- (void)addPoints:(int)points forPage:(int)pageNumber status:(int)status;
- (void)addPointsWithIncorrectAnswer:(int)points;

- (void)changeNavigationButtons:(id)sender;
- (void)finishInstanceWithIncorrectAnswer;

- (void)closeFeedbackandSwipePage:(int)side;

@optional
- (void)addActivityIndicator;
- (void)removeActivityIndicator;
- (void)addActivityIndicatorForFramePresent;

@optional
- (void)gotoNextPage;
- (void)gotoPreviousPage;

@optional
- (void)enableScrolling:(id)sender;
- (void)disableScrolling:(id)sender;

- (void)enablePageControlButtons;
- (void)disablePageControlButtons;

@optional

- (void)setIsPracticeVehicleActive:(BOOL)isActive;

@end

@interface ECVehicleDisplay : UIScrollView <FeedbackViewDelegate>

@property (nonatomic, strong) BaseInstance *dictVehicleInfo;
@property (nonatomic, weak) id<ECVehicleDisplayDelegate> vehicleDelegate;

//positions of question, answers
@property float yPosQuestionArea;
@property float yPosAnswersArea;

@property (nonatomic, strong) CustomTitleView *viewTitleLabel;

@property (nonatomic, strong) FeedbackView *feedbackView;
@property (nonatomic, strong) Feedback *incorrectFeedback;
@property (nonatomic, strong) Feedback *correctFeedback;

@property (nonatomic, readwrite) BOOL isCompleted;
@property (nonatomic, readwrite) BOOL animationInProgress;

@property (nonatomic, assign) NSInteger instanceOrder;

- (void)resetTheInstance;
- (void)showLayoutForCompletedInstance;

// Explanantion example: start animations and stop animations in specific instance
// stop/start sounds

- (void)disableVehicleInteractions;
- (void)enableVehicleInteractions;

/////////////////////////////////////////////
// instance number and page number

@property (nonatomic, assign) NSInteger instanceNumber;
@property (nonatomic, assign) NSInteger pageNumber;

@end
