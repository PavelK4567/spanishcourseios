//
//  WritingVehicleScreen.h
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/27/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"

@interface WritingVehicleScreen : ECVehicleDisplay <UITextViewDelegate> {
    
    BOOL isWritingActive;
    int lastNumberOfLines;
}

//GUI elements

@property (nonatomic, strong) UIImageView *imgBoard;
@property (nonatomic, strong) UITextView *txtTranslationText;
//@property (nonatomic, strong) UITextView *txtTextToBeTranslate;
@property (nonatomic, strong) UIButton *btnProfileSound;
@property (nonatomic, strong) UIButton *btnTranslate;

- (IBAction)playIntroSound:(id)sender;
- (void)showKeyboard;

- (void)initializeElements;
- (void)drawGUI;

@end
