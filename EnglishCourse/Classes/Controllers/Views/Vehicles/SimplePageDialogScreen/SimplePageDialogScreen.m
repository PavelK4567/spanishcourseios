//
//  SimplePageDialogScreen.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SimplePageDialogScreen.h"

#define BUBBLE_VIEW 100

@implementation SimplePageDialogScreen

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    
  self = [super initWithFrame:frame];
  
  if (self) {
        self.nibView = [[[NSBundle mainBundle] loadNibNamed:@"SimplePageDialogScreenView" owner:self options:nil] objectAtIndex:0];
        [self addSubview:self.nibView];
        
        [self bringSubviewToFront:self.viewTitleLabel];
        
        self.bubbledScrollView = (UIScrollView *)[self.nibView viewWithTag:BUBBLE_VIEW];
        [self.bubbledScrollView setBackgroundColor:[UIColor clearColor]];

    }
    
    return self;
}

- (void)initData {
    
    self.charactersArray = [[DataService sharedInstance] findAllCharacters];
    self.segmentsArray = ((SimplePageBubbleDialog *)self.dictVehicleInfo).segmentsArray;
    
    if (!isViewGenerated) {
        
        self.bubblesDataArray = [NSMutableArray arrayWithCapacity:1];
        
        NSInteger prevCharacterID = -1;
        
        NSInteger cellIndexCounter = 0;
        
        IconPosition iconPosition = LEFT;
        
        NSBubbleData *chatBubbleData = nil;
        
        Character *character = nil;
        
        for (SegmentBubbleDialog *segmentData in self.segmentsArray) {
            
            if (prevCharacterID == -1) {
                
                prevCharacterID = segmentData.characterID;
                
            } else if (prevCharacterID != segmentData.characterID) {
                
                iconPosition = (iconPosition == LEFT)?RIGHT:LEFT;
                
                prevCharacterID = segmentData.characterID;
                
            }
            
            character = self.charactersArray[(segmentData.characterID - 1)];
            
            chatBubbleData = [NSBubbleData dataWithText:segmentData.text date:nil type:((iconPosition == LEFT)?BubbleTypeSomeoneElse:BubbleTypeMine) index:cellIndexCounter chatType:SIMPLE_CHAT answersArray:nil audioSample:nil drawAnswered:NO];
            chatBubbleData.avatar = [UIImage imageFromDocumentsResourceFile:character.image];
            chatBubbleData.showAvatar = YES;
            chatBubbleData.colorInHEX = character.color;
            chatBubbleData.indexPosition = cellIndexCounter;
            chatBubbleData.iconPosition = iconPosition;
            chatBubbleData.characterID = character.mID;
            
            [self.bubblesDataArray addObject:chatBubbleData];
            
            cellIndexCounter++;
            
        }
        
        isViewGenerated = YES;
        
        [self drawInstance];
        
    }
    
}

- (void)drawInstance {
    
    NSArray *bubblesArray = self.bubblesDataArray;
    
    NSInteger yOffset = 10;
    
    NSInteger startY = self.bubbledScrollView.contentSize.height + yOffset;
    
    NSInteger tagCounter = 0;
    
    for (NSBubbleData *bubbleData in bubblesArray) {
        
        if (tagCounter > 0) {
            NSBubbleData *prevBubbleData = bubblesArray[(tagCounter - 1)];
            
            if (bubbleData.iconPosition == prevBubbleData.iconPosition && bubbleData.characterID == prevBubbleData.characterID) {
                startY += (yOffset / 2);
            }
            
        }

        CGRect frame = bubbleData.view.frame;
        frame.origin.y = startY;
        
        LMBubbleView *bubbleView = [[LMBubbleView alloc] initWithFrame:CGRectMake(0, startY, frame.size.width, frame.size.height)];
        bubbleView.showAvatar = YES;
        bubbleView.delegate = self;
        bubbleView.tag = tagCounter;
        bubbleView.bubbleType = BUBBLE_CHAT;
        
        [bubbleView setDataInternal:bubbleData];
        
        [self.bubbledScrollView addSubview:bubbleView];
        
        bubbleData.isCellViewVisible = YES;
        
        startY += bubbleData.view.height + yOffset + 10;
        
        CGSize contentSize = self.size;
        
        CGSize containerSize = CGSizeMake(contentSize.width, startY + ((tagCounter - 1) * 5));
        //CGSize containerFrameSize = self.bubbledScrollView.frame.size;
        
        [self.bubbledScrollView setContentSize:containerSize];
        self.bubbledScrollView.height = self.bubbledScrollView.contentSize.height;
        
        tagCounter++;
        
    }
    
    //self.bubbledScrollView.scrollEnabled = NO;
    
    [self setContentSize:CGSizeMake(self.width, self.bubbledScrollView.height)];
    
    [self sizeToFit];
    [self layoutIfNeeded];
    
}

- (void)drawRect:(CGRect)rect
{
    [self.viewTitleLabel setTitleOftheInstance:((SimplePageBubbleDialog *)self.dictVehicleInfo).title];
    
    //self.bubbledScrollView.height = [[UIScreen mainScreen] bounds].size.height - 113 - self.viewTitleLabel.height;
    //self.bubbledScrollView.y = self.viewTitleLabel.y + self.viewTitleLabel.height + 40;
    
    self.nibView.y = self.viewTitleLabel.y + self.viewTitleLabel.height + 5;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end
