//  LMLoginViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMLoginViewController.h"
#import "LMLoginCodeViewController.h"
#import "PleaseSubscribeViewController.h"
//#import "LMBuyInAppViewController.h"

@interface LMLoginViewController () {
  IBOutlet UIView *subscriptionPopUpView;
  UIView *subscriptionBlackView;
  IBOutlet UITextView *subscriptionExplanationText, *subscriptionIntroductionText;
  IBOutlet UILabel *subscriptionExplanationLabel, *subscriptionIntroductionLabel;
  IBOutlet UIButton *subscriptionPopUpCancelButton, *subscriptionPopUpsubscribeButton;
  IBOutlet UILabel *subscriptionPopUpTitle;
  IBOutlet UILabel *subscriptionExpiredLabel;
  UITapGestureRecognizer *tap;
    
  BOOL isReExtractingInProcess;
    
}

@property(weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *contentScroll;
@property(weak, nonatomic) IBOutlet UIView *contentView;
@property(weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property(weak, nonatomic) IBOutlet UILabel *userLoginTitleLabel;
@property(weak, nonatomic) IBOutlet UIView *userPhoneNumberView;
@property(weak, nonatomic) IBOutlet UITextField *userPhonePrefixField;
@property(weak, nonatomic) IBOutlet UITextField *userPhoneNumberField;
@property(weak, nonatomic) IBOutlet UIButton *userLoginButton;
@property(weak, nonatomic) IBOutlet UILabel *infoLabe;
@property(weak, nonatomic) IBOutlet UIView *separatorView;
@property(weak, nonatomic) IBOutlet UILabel *guestLoginTitleLabel;
@property(weak, nonatomic) IBOutlet UIButton *guestLoginButton;
@property(weak, nonatomic) IBOutlet UIButton *inAppButton;
@property(weak, nonatomic) IBOutlet UIButton *restoreButton;
    @property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
    @property (weak, nonatomic) IBOutlet UITextField *fullNameField;
    @property (weak, nonatomic) IBOutlet UILabel *mailLabel;
    @property (weak, nonatomic) IBOutlet UITextField *mailField;
    

- (IBAction)userLoginTapped:(id)sender;
- (IBAction)guestLoginTapped:(id)sender;

- (void)executeWithDelay;

@end


@implementation LMLoginViewController

- (id)initWithNibName:(NSString *)nib bundle:(NSBundle *)budnle
{
  self = [super initWithNibName:nib bundle:budnle];
  if (self) {
    //self.title = Localized(@"Login");
  }
  return self;
}

- (void)configureAppearance
{
  [super configureAppearance];
    
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(5, 10, 40, 50)];
    [back setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateNormal];
    [back setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateSelected];
    [back addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backMenuBtn = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backMenuBtn;
  
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.userLoginButton];
  [APPSTYLE applyStyle:@"Button_Guest_Login_Page" toButton:self.guestLoginButton];
  //[APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.inAppButton];
  //[APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.restoreButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:subscriptionPopUpCancelButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:subscriptionPopUpsubscribeButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.userLoginButton];
  //[APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.guestLoginButton];
  //[APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.inAppButton];
  //[APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.restoreButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:subscriptionPopUpCancelButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:subscriptionPopUpsubscribeButton];
  
    [APPSTYLE applyStyle:@"Button_Login_skip" toButton:self.inAppButton];
    [self.inAppButton setBackgroundColor:[UIColor clearColor]];
    
//  self.inAppButton.layer.cornerRadius = 4.0;
//  self.inAppButton.layer.borderWidth = 1;
//  self.inAppButton.backgroundColor = [UIColor whiteColor];
//  [self.inAppButton.layer setBorderColor: [APPSTYLE colorForType:@"Purple_Main_Color"].CGColor];
//  [self.inAppButton setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
  
  self.restoreButton.layer.cornerRadius = 4.0;
  self.restoreButton.layer.borderWidth = 1;
  self.restoreButton.backgroundColor = [UIColor whiteColor];
  [self.restoreButton.layer setBorderColor: [APPSTYLE colorForType:@"Purple_Main_Color"].CGColor];
  [self.restoreButton setTitleColor:[APPSTYLE colorForType:@"Purple_Main_Color"] forState:UIControlStateNormal];
  
  self.userLoginButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  self.guestLoginButton.backgroundColor = [UIColor clearColor];
  //self.inAppButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  //self.restoreButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  subscriptionPopUpCancelButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  subscriptionPopUpsubscribeButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
    
    [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.fullNameLabel];
    [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.mailLabel];
  
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.userLoginTitleLabel];
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.guestLoginTitleLabel];
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.infoLabe];
  [APPSTYLE applyStyle:@"Login_Title" toLabel:subscriptionPopUpTitle];
  // subscriptionPopUpTitle.textColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  [APPSTYLE applyStyle:@"Login_Label2_Body" toLabel:subscriptionExplanationLabel];
    
    [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.fullNameField];
    [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.mailField];
  
  
  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhonePrefixField];
  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhoneNumberField];
  self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
}
    
-(void) backPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configureUI
{
  [super configureUI];
  
  [self.contentScroll addSubview:self.contentView];
  [self.contentScroll setContentSize:self.contentView.frame.size];
  //[self.contentScroll contentSizeToFit];
  
  //[self.userLoginButton setCornerRadius:2.0];
  //[self.guestLoginButton setCornerRadius:2.0];
  
  [self.userPhonePrefixField setCornerRadius:4.0];
  [self.userPhonePrefixField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];
  [self.userPhoneNumberField setCornerRadius:4.0];
  [self.userPhoneNumberField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];
    [self.fullNameField setCornerRadius:4.0];
    [self.fullNameField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];
    [self.mailField setCornerRadius:4.0];
    [self.mailField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];
  
  self.userLoginTitleLabel.text = Localized(@"T001");
  self.guestLoginTitleLabel.text = Localized(@"T004");
  [APPSTYLE applyTitle:Localized(@"T002") toButton:self.userLoginButton];
  [APPSTYLE applyTitle:Localized(@"T005") toButton:self.guestLoginButton];
  
  //[APPSTYLE applyTitle:Localized(@"T003.3") toButton:self.inAppButton];
    [APPSTYLE applyTitle:Localized(@"T003.8") toButton:self.inAppButton];
  [APPSTYLE applyTitle:Localized(@"T018") toButton:subscriptionPopUpCancelButton];
  [APPSTYLE applyTitle:Localized(@"T017.1") toButton:subscriptionPopUpsubscribeButton];
  

    self.fullNameLabel.text = Localized(@"T001.1");
    self.mailLabel.text = Localized(@"T001.2");
  
  
  subscriptionExplanationText.text = Localized(@"T016");
  subscriptionExplanationLabel.text = Localized(@"T016");
  subscriptionPopUpTitle.text = Localized(@"T015");
  
  
  NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
  
  
  [self.restoreButton setHidden:YES];
  [self.restoreButton setEnabled:NO];
  [self.inAppButton setHidden:NO];
  [self.inAppButton setEnabled:YES];
  self.guestLoginTitleLabel.text = Localized(@"T003.1");
    self.infoLabe.numberOfLines = 0;
  self.infoLabe.text = Localized(@"T003.2");
    //[self.infoLabe sizeToFit];
  if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
    //[APPSTYLE applyTitle:Localized(@"T003.4") toButton:self.inAppButton];
    self.infoLabe.text = [NSString stringWithFormat:Localized(@"T010B"), [USER_MANAGER takeNativeDateStringFromDate]];
      //[self.infoLabe sizeToFit];
  }
  ///////
  
  if ([self youHaveRestoreData]) {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"youHaveRestoreDataChek"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.restoreButton setHidden:NO];
    [self.restoreButton setEnabled:YES];
//    [self.inAppButton setHidden:YES];
//    [self.inAppButton setEnabled:NO];
    
    self.guestLoginTitleLabel.text = Localized(@"T003.5");
    self.infoLabe.text = Localized(@"T003.6");
     // [self.infoLabe sizeToFit];
    [APPSTYLE applyTitle:Localized(@"T003.7") toButton:self.restoreButton];
  } else {
    BOOL firsTimeCloudChek = [[NSUserDefaults standardUserDefaults] boolForKey:@"firsTimeCloudChek"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firsTimeCloudChek"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (!firsTimeCloudChek) {
      [self addActivityIndicator];
      [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(chekCloudData) userInfo:nil repeats:NO];
    }
  }
  //
  //[AUDIO_MANAGER record:[self recordingPathForIndex:100 order:200]];
  //[AUDIO_MANAGER stopRecording];
}
- (NSString *)recordingPathForIndex:(int)path order:(NSInteger)order {
  return [[self recordingPath] stringByAppendingPathComponent:
          [NSString stringWithFormat:@"record%ld_%d.lpcm", (long)order,path]];
}
- (NSString *)recordingPath {
  NSString *recordingPathUrlString = [[NSString documentsDir] stringByAppendingPathComponent:@"GuestRecordings"];

  if (![[NSFileManager defaultManager] fileExistsAtPath:recordingPathUrlString]) {
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:recordingPathUrlString withIntermediateDirectories:NO attributes:nil error:&error];
  }
  return recordingPathUrlString;
}
- (void)chekCloudData
{
  [self removeActivityIndicator];
  NSString *cloud_uuid = [SDCloudUserDefaults stringForKey:kMonthlyPurchaseId];
  DLog(@"chekCloudData>>> cloud_uuid %@", cloud_uuid);
  DLog(@"chekCloudData>>> objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
  DLog(@"chekCloudData>>>kMonthlyPurchaseId %@ ", [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]]);
  if ([self youHaveRestoreData]) {
    [self.restoreButton setHidden:NO];
    [self.restoreButton setEnabled:YES];
//    [self.inAppButton setHidden:YES];
//    [self.inAppButton setEnabled:NO];
    
    self.guestLoginTitleLabel.text = Localized(@"T003.5");
    self.infoLabe.text = Localized(@"T003.6");
      [self.infoLabe sizeToFit];
    [APPSTYLE applyTitle:Localized(@"T003.7") toButton:self.restoreButton];
  }
  [AUDIO_MANAGER record:[self recordingPathForIndex:100 order:200]];
  [AUDIO_MANAGER stopRecording];
}
- (BOOL)youHaveRestoreData
{
  if ([USER_MANAGER isGuest]) {
    BOOL youHaveRestoreDataBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"youHaveRestoreDataChek"];
    return youHaveRestoreDataBefore;
  }
  if (![USER_MANAGER getUserStatus]) {
    NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
    DLog(@" youHaveRestoreData uid_icloud:%@ ", uid_icloud);
    if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
      return YES;
    }
  } else {
    if (![USER_MANAGER isNative]) {
      BOOL youMakeNativeUserBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"makeNativeUser"];
      if(youMakeNativeUserBefore)
      {
        //this is new in english course
        //return yes after log out with native user
        if ([[SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]] isAfter:[NSDate date]]) {
          return YES;
        }
        return NO;
      }
      NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
      DLog(@"#2 youHaveRestoreData uid_icloud:%@ ", uid_icloud);
      if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
        return YES;
      }
    }
  }
  if ([USER_MANAGER isNative]) {
    //this is new in english course
    //return yes after log out with native user
    if ([[SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]] isAfter:[NSDate date]]) {
      return YES;
    }
  }
  return NO;
}
- (void)configureObservers
{
  [super configureObservers];
}

- (void)configureNavigation
{
  [super configureNavigation];
  self.navigationItem.title = Localized(@"T069");
  // self.navigationController.navigationBarHidden = YES;
}

- (void)configureData
{
  [super configureData];
}

- (void)loadData
{
  [super loadData];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[Utilities sharedInstance] getFreeDiskspace] > ((100 * 1024) * 1024)) {
        
        if ([defaults objectForKey:kDataExtracted] && [defaults boolForKey:kInitialModelGenerated] && !DATA_SERVICE.dataModel) {
            
            isReExtractingInProcess = YES;
            
            [self addActivityIndicator];
            
            [self performSelector:@selector(executeWithDelay) withObject:nil afterDelay:1.0];
            
        } else {
            
            if (![defaults objectForKey:kDataExtracted]) {
                
                if (![[Utilities sharedInstance] checkIfASITmpFolderCreated]) {
                    [[Utilities sharedInstance] createASITmpFolder];
                }
                
                [[Utilities sharedInstance] extractInitialData:self];
            }
            
            if ([defaults objectForKey:kDataExtracted] && ![defaults boolForKey:kInitialModelGenerated]) {
                
                isReExtractingInProcess = YES;
                
                [self addActivityIndicator];
                
                [self performSelector:@selector(executeWithDelay) withObject:nil afterDelay:1.0];
            }
            
        }
        
    } else {
        
        [UIAlertView alertWithCause:kAlertOutOfStorage];
        
    }
    
    
  //[AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"ButtonClickFX.mp3" ofType:@""]]];
}



- (void)executeWithDelay
{
    [[Utilities sharedInstance] extractInitialData:self];
}

- (void)layout
{
  [super layout];
}

- (void)dismissObservers
{
  [super dismissObservers];
}

#pragma mark - validation of phone number
- (BOOL)chekUserTextFields
{
    return ([self chekUserPhonePrefixField] && [self chekUserPhoneNumberField] &&
            [self checkIfNameIsValid] && [self checkIfMailIsValid]);
}
    
-(BOOL) checkIfNameIsValid {
    if (self.fullNameField.text.length > 0) {
        if (self.fullNameField.text.length < 2) {
            [UIAlertView alertWithCause:kAlertNoNameMessage];
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}
    
-(BOOL) checkIfMailIsValid {
    if (self.mailField.text.length > 0) {
        if ([self validateEmailWithString:self.mailField.text]) {
            return YES;
        } else {
            [UIAlertView alertWithCause:kAlertNoMailMessage];
            return NO;
        }
    } else {
        return YES;
    }
}
    
- (BOOL)validateEmailWithString:(NSString*)email
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [emailTest evaluateWithObject:email];
    }

- (BOOL)chekUserPhonePrefixField
{
  if ([self.userPhonePrefixField.text length] < 2) {
    [UIAlertView alertWithCause:kAlertPhoneNumberNotValid];
    return NO;
  }
  return YES;
}

- (BOOL)chekUserPhoneNumberField
{
  if ([self.userPhoneNumberField.text length] < 7) {
    [UIAlertView alertWithCause:kAlertPhoneNumberShorterThan8Digit];
    return NO;
  }
  return YES;
}

#pragma mark - Interface Builder Actions

- (IBAction)userLoginTapped:(id)sender
{
    if (!DATA_SERVICE.dataModel) {
        
        if ([[Utilities sharedInstance] getFreeDiskspace] < ((100 * 1024) * 1024)) {
            [UIAlertView alertWithCause:kAlertOutOfStorage];
            return;
        } else {
            isReExtractingInProcess = YES;
            
            [self addActivityIndicator];
            
            [self performSelector:@selector(executeWithDelay) withObject:nil afterDelay:1.0];
        }

    } else {
        
        [AUDIO_MANAGER stopRecording];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if (![defaults objectForKey:kDataExtracted]) {
            return;
        }
        
        if (![ReachabilityHelper reachable]) {
            [UIAlertView alertWithCause:kAlertNoInternetConnection];
            return;
        }
        
        if ([self chekUserTextFields]) {
            NSString *msisdn = [NSString stringWithFormat:@"%@%@%@", [USER_MANAGER defaultPrefix], self.userPhonePrefixField.text, self.userPhoneNumberField.text];
            [self addActivityIndicator];
            [self.userPhoneNumberField resignFirstResponder];
            [USER_MANAGER makeLoginTokenWithMSISDN:msisdn WithCompletitionBlock:^(NSNumber *userStatus) {
                [self removeActivityIndicator];
                if([userStatus integerValue] == KAUserStatusNotRegistered){
                    [self showSubscribePopUp:0];
                    return ;
                }
                if([userStatus integerValue] == KAUserStatusCanceled){
                    [self showPurchasePopUp:@"cancel"];
                    return ;
                }
                
                if([USER_MANAGER chekUserStatus:[userStatus integerValue]]){
                    //LMLoginCodeViewController *controler = [LMLoginCodeViewController new];
                    //controler.msisdn = msisdn;
                    self.navigationItem.title = @" ";
                    //[self.navigationController pushViewController:controler animated:YES];
                    [USER_MANAGER loginNoCodeWithMSISDN:msisdn];
                    [USER_MANAGER loadUserValuesWithCompletitionBlock:^(NSError *error)
                     {
                         // check code for error
                         if ([USER_MANAGER isAuthenticated])
                         {
                             [DATA_SERVICE loadDataModel];
                             
                             [APP_DELEGATE buildMainStack];
                             
                             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userWasLogged"];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                             
                             [self initProgressModelLogin];
                             
                         } else {
                             [self showPurchasePopUp:@"cancel"];
                         }
                         
                     }];
                }
            }];
        }
        
    }
    
  /*else
  {
    [UIAlertView alertWithCause:kAlertPhoneNumberNotValid];
  }*/
}
    
- (void)initProgressModelLogin
    {
        
        //NSString *userId = [USER_MANAGER userMsisdn];
        
        UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN];
        if(!userStatistic) {
            ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
            [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
        }
        [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN]];
        [PROGRESS_MANAGER updateProgressModel];
    }

- (IBAction)guestLoginTapped:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (!DATA_SERVICE.dataModel) {
        
        if ([[Utilities sharedInstance] getFreeDiskspace] < ((100 * 1024) * 1024)) {
            [UIAlertView alertWithCause:kAlertOutOfStorage];
            return;
        } else {
            isReExtractingInProcess = YES;
            
            [self addActivityIndicator];
            
            [self performSelector:@selector(executeWithDelay) withObject:nil afterDelay:1.0];
        }
        
    } else {
        
        if (![defaults objectForKey:kDataExtracted]) {
            return;
        }
        
        if (![ReachabilityHelper reachable]) {
            [UIAlertView alertWithCause:kAlertNoInternetConnection];
        }
        [self addActivityIndicator];
        [USER_MANAGER initializeWithGuest];
        //[USER_MANAGER loadUserValuesWithCompletitionBlock:^(NSError *error) {
            [self removeActivityIndicator];
            [USER_MANAGER setGuestStatus];
            [APP_DELEGATE buildMainStack];
        //}];
        [AUDIO_MANAGER stopRecording];
        [self initProgressModel];
        
    }
    
}

- (IBAction)inAppButtonTapped:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:kDataExtracted]) {
        return;
    }
   [AUDIO_MANAGER stopRecording];
  [self subscribe:nil];
}
- (IBAction)restoreButtonTapped:(id)sender
{
   [AUDIO_MANAGER stopRecording];
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    return;
  }
  [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"youHaveRestoreDataChek"]; // this is only for guestUser chek restor data
  [[NSUserDefaults standardUserDefaults] synchronize];
  [self addActivityIndicator];
  [USER_MANAGER initializeWithNativeUser];
 
  [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
  [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
    if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
      [self removeActivityIndicator];
      [self initProgressModel];
      [APP_DELEGATE buildMainStack];
    } else {
      [self.restoreButton setHidden:YES];
      [self.restoreButton setEnabled:NO];
      [self.inAppButton setHidden:NO];
      [self.inAppButton setEnabled:YES];
      [self removeActivityIndicator];
      //[APPSTYLE applyTitle:Localized(@"T003.4") toButton:self.inAppButton];
      self.infoLabe.text = [NSString stringWithFormat:Localized(@"T010B"), [USER_MANAGER takeNativeDateStringFromDate]];
        [self.infoLabe sizeToFit];
    }
    
  }];
}
#pragma mark in app
- (IBAction)subscribe:(id)sender
{
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    return;
  }
  [self addActivityIndicator];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:0];
  
  if (![PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
    [PURCHASE_MANAGER requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
      if (success) {
        NSArray *filtered = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier BEGINSWITH[cd] %@", kMonthlyPurchaseId]];
        if (filtered && [filtered count] != 0) {
          SKProduct *product = filtered[0];
          [PURCHASE_MANAGER buyProduct:product];
          NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:@"No userID", @"userID", nil];
          [Flurry logEvent:kOnAppleSubscriptionTerms withParameters:articleParams];
        } else {
          [[NSNotificationCenter defaultCenter] removeObserver:self];
          [self removeActivityIndicator];
        }
      } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self removeActivityIndicator];
      }
    }];
  } else {
    [self removeActivityIndicator];
  }
}
#pragma mark - Purchased
- (void)productPurchased:(NSString *)productId
{
  DLog(@"productPurchased %@", productId);
  [self removeActivityIndicator];
  //[[NSNotificationCenter defaultCenter] removeObserver:self];
  if ([PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
    DLog(@"YES ");
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // dali samo za yes da go trgam?
    //[self addActivityIndicator];
    NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
    if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
      [USER_MANAGER initializeWithNativeUser];
      [USER_MANAGER makeBillingForNativeUserWithCompletitionBlock:^(NSError *error) {
        [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
          [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
          [[NSUserDefaults standardUserDefaults] synchronize];
          [self initProgressModel];
          [self removeActivityIndicator];
          [APP_DELEGATE buildMainStack];
        }];
      }];
      
      return;
    }
    //[self addActivityIndicator];
    [USER_MANAGER initializeWithNativeUser];
    [USER_MANAGER registerNativeUserValuesWithCompletitionBlock:^(NSError *error) {
      if(error){
        [UIAlertView alertWithCause:kAlertServerUnreachable];
      }
      else {
          [USER_MANAGER setIsUserRegister:YES];
      }
      [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
        if(error){
          [UIAlertView alertWithCause:kAlertServerUnreachable];
        }
        

        [self removeActivityIndicator];
        [self initProgressModel];
        [APP_DELEGATE buildMainStack];
        
      }];
    }];
  } else {
    DLog(@"NO ");
  }
  //[_submitButton setEnabled:YES];
  //[self releaseLoadingWindow];
}

#pragma mark - TextField functionsF
//
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  if ((textField == self.userPhonePrefixField) || (textField == self.userPhoneNumberField)) {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
    if (numberOfMatches == 0)
      return NO;
    
    
    if (textField == self.userPhonePrefixField) {
      if (self.userPhonePrefixField.text.length + [string length] >= 2 && [string length] > 0) {
        if (self.userPhonePrefixField.text.length < 2)
          self.userPhonePrefixField.text = [self.userPhonePrefixField.text stringByAppendingString:string];
        [self.userPhoneNumberField becomeFirstResponder];
        return NO;
      }
      return YES;
    }
    if (textField == self.userPhoneNumberField) {
      if (self.userPhoneNumberField.text.length >= 9 && [string length] > 0)
        return NO;
      return YES;
    }
    return YES;
  }
  
  return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if (textField == self.userPhonePrefixField) {
    [self.userPhoneNumberField becomeFirstResponder];
  }
  return YES;
}
#pragma mark subscriptionPopUpView

- (IBAction)showPurchasePopUp:(NSString *) status {
    PleaseSubscribeViewController *plsSub = [[PleaseSubscribeViewController alloc] init];
    if ([status isEqualToString:@"cancel"]) {
        [plsSub setIsCanceled:YES];
    }
    [self.navigationController pushViewController:plsSub animated:YES];
}
    
- (IBAction)showSubscribePopUp:(id)sender
{
    
    PleaseSubscribeViewController *plsSub = [[PleaseSubscribeViewController alloc] init];
    [plsSub setIsLoginSkip:YES];
    [self.navigationController pushViewController:plsSub animated:YES];
  
//  subscriptionBlackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//  subscriptionBlackView.backgroundColor = [UIColor blackColor];
//  subscriptionPopUpView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
//  [subscriptionBlackView setAlpha:0];
//  [self.view addSubview:subscriptionBlackView];
//
//  [self.view addSubview:subscriptionPopUpView];
//
//  /* loginButton.userInteractionEnabled = NO;
//   guestLoginButoon.userInteractionEnabled = NO;
//   subscribeButton.userInteractionEnabled = NO;
//
//   [prefixPhoneLabel resignFirstResponder];
//   [phoneLabel resignFirstResponder];*/
//  int upSpace = 41;
//  if (IS_IPHONE4) {
//    upSpace = 31;
//  }
//  [self.userPhonePrefixField resignFirstResponder];
//  [self.userPhoneNumberField resignFirstResponder];
//  [subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - subscriptionPopUpView.frame.size.width) / 2, self.view.frame.size.height,
//                                             subscriptionPopUpView.frame.size.width, subscriptionPopUpView.frame.size.height)];
//  subscriptionPopUpView.alpha = 0;
//  [UIView animateWithDuration:0.4
//                        delay:0
//                      options:0
//                   animations:^{
//                     [subscriptionBlackView setAlpha:0.9];
//                     [subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - subscriptionPopUpView.frame.size.width) / 2, upSpace,
//                                                                subscriptionPopUpView.frame.size.width, subscriptionPopUpView.frame.size.height)];
//                     subscriptionPopUpView.alpha = 1;
//                     /*tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
//
//                      [self.view addGestureRecognizer:tap];*/
//                   }
//
//                   completion:^(BOOL finished){
//
//                   }];
}

- (IBAction)cancelSubscribePopUp:(id)sender
{
  [self removeSubscriptionPopUpView];
}

- (void)removeSubscriptionPopUpView
{
  [UIView animateWithDuration:0.4
                        delay:0
                      options:0
                   animations:^{
                     [subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - subscriptionPopUpView.frame.size.width) / 2, self.view.frame.size.height,
                                                                subscriptionPopUpView.frame.size.width, subscriptionPopUpView.frame.size.height)];
                     subscriptionPopUpView.alpha = 0;
                     [subscriptionBlackView setAlpha:0];
                     [self.view removeGestureRecognizer:tap];
                   }
   
                   completion:^(BOOL finished) {
                     /* loginButton.userInteractionEnabled = YES;
                      guestLoginButoon.userInteractionEnabled = YES;
                      subscribeButton.userInteractionEnabled = YES;*/
                     [subscriptionBlackView removeFromSuperview];
                   }];
}

#pragma mark -
#pragma mark - SSZipArchive delegate

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    
    if (isReExtractingInProcess) {
        [self removeActivityIndicator];
        
        isReExtractingInProcess = NO;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:kDataExtracted];
    [defaults setBool:NO forKey:kDataCompleteDownload];
    [defaults synchronize];
    
    [[SyncService sharedInstance] buildInitialDataModel:self];
}

#pragma mark -
#pragma mark - SyncService delegate

- (void)returnBuildStatusWithError:(BOOL)hasError errorMessage:(NSString *)errorMessage {
    //[APP_DELEGATE buildMainStackWithLevelIndex:0];
    
    [self initProgressModel];
    
    self.isInitialDataGenerated = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults objectForKey:kInitialModelGenerated]) {
        [defaults setBool:YES forKey:kInitialModelGenerated];
        [defaults synchronize];
    }
    
}


#pragma mark - initProgressModel

- (void)initProgressModel
{

    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN];
    if(!userStatistic) {
        ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
        [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
    }
    [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN]];
    
    
    NSLog(@"Was logged as VIVO user - %@", [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"] ? @"Yes" : @"No");
    
    if ([USER_MANAGER isNative]) {
        
        [PROGRESS_MANAGER updateProgressModel];
        
    }
    else if ([USER_MANAGER isGuest] && [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"]) {
     
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userWasLogged"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [PROGRESS_MANAGER updateProgressModel];
        
    }
}


-(BOOL)shouldAutorotate
{
    
    
    return NO;
}


@end
