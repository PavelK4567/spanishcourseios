//
//  PleaseSubscribeViewController.h
//  EnglishCourse
//
//  Created by Aleksandar Mitrovski on 7/9/18.
//  Copyright © 2018 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PleaseSubscribeViewController : LMBaseViewController

@property (assign, nonatomic) bool isLoginSkip;
@property (assign, nonatomic) bool isCanceled;
    
@end
