//
//  PleaseSubscribeViewController.m
//  EnglishCourse
//
//  Created by Aleksandar Mitrovski on 7/9/18.
//  Copyright © 2018 LaMark. All rights reserved.
//

#import "PleaseSubscribeViewController.h"

@interface PleaseSubscribeViewController (){
    UIView *subscriptionBlackView;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;
    
@property (weak, nonatomic) IBOutlet UILabel *confirmationCode;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UILabel *confirmationLable;
@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
@property (strong, nonatomic) IBOutlet UITextView *textField;
@property (strong, nonatomic) IBOutlet UIView *subscriptionPopUpView;
    
@end

@implementation PleaseSubscribeViewController
    
    @synthesize isLoginSkip = _isLoginSkip;
    @synthesize isCanceled = _isCanceled;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
    
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void) configureView {
    self.navigationItem.title = Localized(@"T069");
    
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(5, 10, 40, 50)];
    [back setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateNormal];
    [back setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateSelected];
    [back addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backMenuBtn = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = backMenuBtn;
    
    _textField.text = Localized(@"T016");
    _confirmationLable.text = Localized(@"T016");
    _confirmationCode.text = Localized(@"T015");
    
    [APPSTYLE applyStyle:@"Button_Login_Page" toButton:_cancelBtn];
    [APPSTYLE applyStyle:@"Button_Login_Page" toButton:_enterBtn];
    
    [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:_cancelBtn];
    [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:_enterBtn];
    
    _cancelBtn.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
    _enterBtn.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
    
    [APPSTYLE applyTitle:Localized(@"T018") toButton:_cancelBtn];
    [APPSTYLE applyTitle:Localized(@"T017.1") toButton:_enterBtn];
    
    [APPSTYLE applyStyle:@"Login_Title" toLabel:self.titleLbl];
    [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.messageLbl];
    
    [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.continueBtn];
    [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.continueBtn];
    self.continueBtn.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
    //[APPSTYLE applyTitle:Localized(@"T003.3") toButton:self.continueBtn];
    [APPSTYLE applyTitle:Localized(@"T668") toButton:self.continueBtn];
    
    [_enterBtn addTarget:self action:@selector(enterTapped) forControlEvents:UIControlEventTouchUpInside];
    
    self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
    
    if (_isLoginSkip) {
        self.titleLbl.text = Localized(@"T666");
        self.messageLbl.text = Localized(@"T667");
    } else if (_isCanceled) {
        self.titleLbl.text = Localized(@"T666");
        self.messageLbl.text = Localized(@"T016");
    }
}
    
- (void) backPressed {
    [self.navigationController popViewControllerAnimated:YES];
}
    
- (IBAction)continuePressed:(UIButton *)sender {
    subscriptionBlackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    subscriptionBlackView.backgroundColor = [UIColor blackColor];
    _subscriptionPopUpView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    [subscriptionBlackView setAlpha:0];
    [self.view addSubview:subscriptionBlackView];
    
    [self.view addSubview:_subscriptionPopUpView];
    
    /* loginButton.userInteractionEnabled = NO;
     guestLoginButoon.userInteractionEnabled = NO;
     subscribeButton.userInteractionEnabled = NO;
     
     [prefixPhoneLabel resignFirstResponder];
     [phoneLabel resignFirstResponder];*/
    int upSpace = 41;
    if (IS_IPHONE4) {
        upSpace = 31;
    }
    
    
    
    [_subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - _subscriptionPopUpView.frame.size.width) / 2,
                                                self.view.frame.size.height,
                                                _subscriptionPopUpView.frame.size.width,
                                                _subscriptionPopUpView.frame.size.height)];
    _subscriptionPopUpView.alpha = 0;
    [UIView animateWithDuration:0.4
                          delay:0
                        options:0
                     animations:^{
                         [subscriptionBlackView setAlpha:0.9];
                         [_subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - _subscriptionPopUpView.frame.size.width) / 2, upSpace,
                                                                     _subscriptionPopUpView.frame.size.width, _subscriptionPopUpView.frame.size.height)];
                         _subscriptionPopUpView.alpha = 1;
                     }
     
                     completion:^(BOOL finished){
                         
                     }];
}
- (IBAction)enterPressed:(UIButton *)sender {
}
    
- (IBAction)cancelPressed:(UIButton *)sender {
    [self removeSubscriptionPopUpView];
}
    
- (void)removeSubscriptionPopUpView
    {
        [UIView animateWithDuration:0.4
                              delay:0
                            options:0
                         animations:^{
                             [_subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - _subscriptionPopUpView.frame.size.width) / 2, self.view.frame.size.height,
                                                                         _subscriptionPopUpView.frame.size.width, _subscriptionPopUpView.frame.size.height)];
                             _subscriptionPopUpView.alpha = 0;
                             [subscriptionBlackView setAlpha:0];
                         }
         
                         completion:^(BOOL finished) {
                             /* loginButton.userInteractionEnabled = YES;
                              guestLoginButoon.userInteractionEnabled = YES;
                              subscribeButton.userInteractionEnabled = YES;*/
                             [subscriptionBlackView removeFromSuperview];
                         }];
    }
    
-(void) enterTapped {
    [self subscribe:nil];
}
    
- (IBAction)subscribe:(id)sender
    {
        if (![ReachabilityHelper reachable]) {
            [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
            return;
        }
        [self addActivityIndicator];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:0];
        
        if (![PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
            [PURCHASE_MANAGER requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                if (success) {
                    NSArray *filtered = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier BEGINSWITH[cd] %@", kMonthlyPurchaseId]];
                    if (filtered && [filtered count] != 0) {
                        SKProduct *product = filtered[0];
                        [PURCHASE_MANAGER buyProduct:product];
                        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:@"No userID", @"userID", nil];
                        [Flurry logEvent:kOnAppleSubscriptionTerms withParameters:articleParams];
                    } else {
                        [[NSNotificationCenter defaultCenter] removeObserver:self];
                        [self removeActivityIndicator];
                    }
                } else {
                    [[NSNotificationCenter defaultCenter] removeObserver:self];
                    [self removeActivityIndicator];
                }
            }];
        } else {
            [self removeActivityIndicator];
        }
    }
#pragma mark - Purchased
- (void)productPurchased:(NSString *)productId
    {
        DLog(@"productPurchased %@", productId);
        [self removeActivityIndicator];
        //[[NSNotificationCenter defaultCenter] removeObserver:self];
        if ([PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
            DLog(@"YES ");
            [[NSNotificationCenter defaultCenter] removeObserver:self]; // dali samo za yes da go trgam?
            //[self addActivityIndicator];
            NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
            if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
                [USER_MANAGER initializeWithNativeUser];
                [USER_MANAGER makeBillingForNativeUserWithCompletitionBlock:^(NSError *error) {
                    [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
                        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //[self initProgressModel];
                        [self removeActivityIndicator];
                        [APP_DELEGATE buildMainStack];
                    }];
                }];
                
                return;
            }
            //[self addActivityIndicator];
            [USER_MANAGER initializeWithNativeUser];
            [USER_MANAGER registerNativeUserValuesWithCompletitionBlock:^(NSError *error) {
                if(error){
                    [UIAlertView alertWithCause:kAlertServerUnreachable];
                }
                else {
                    [USER_MANAGER setIsUserRegister:YES];
                }
                [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
                    if(error){
                        [UIAlertView alertWithCause:kAlertServerUnreachable];
                    }
                    
                    
                    [self removeActivityIndicator];
                    //[self initProgressModel];
                    [APP_DELEGATE buildMainStack];
                    
                }];
            }];
        } else {
            DLog(@"NO ");
        }
        //[_submitButton setEnabled:YES];
        //[self releaseLoadingWindow];
    }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
