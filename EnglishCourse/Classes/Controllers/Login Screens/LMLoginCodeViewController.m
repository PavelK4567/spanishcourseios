//  LMLoginCodeViewController.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMLoginCodeViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface LMLoginCodeViewController () {
  int tockenClick;
  BOOL haveKeyboard;
}

@property(weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *contentScroll;
@property(weak, nonatomic) IBOutlet UIView *contentView;
@property(weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property(weak, nonatomic) IBOutlet UILabel *userLoginTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *userLoginSubtitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *userPhoneNumberLabel;
@property(weak, nonatomic) IBOutlet UIView *userPhoneNumberView;
@property(weak, nonatomic) IBOutlet UITextField *userPhonePrefixField;
@property(weak, nonatomic) IBOutlet UITextField *userPhoneNumberField;
@property(weak, nonatomic) IBOutlet UILabel *userPhoneCodeLabel;
@property(weak, nonatomic) IBOutlet UITextField *userPhoneCodeField;
@property(weak, nonatomic) IBOutlet UILabel *userLoginDisclaimerLabel;
@property(weak, nonatomic) IBOutlet UIButton *cancelButton;
@property(weak, nonatomic) IBOutlet UIButton *confirmButton;
@property(weak, nonatomic) IBOutlet UIButton *resendTokenButton;

- (IBAction)cancelTapped:(UIButton *)sender;
- (IBAction)confirmTapped:(UIButton *)sender;

@end


@implementation LMLoginCodeViewController


- (void)configureAppearance
{
  [super configureAppearance];

  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.confirmButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.cancelButton];
  [APPSTYLE applyStyle:@"Button_Resend_Tocken" toButton:self.resendTokenButton];
  
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.confirmButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.cancelButton];

  self.confirmButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  self.cancelButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  self.resendTokenButton.backgroundColor = [UIColor clearColor];
  
  [APPSTYLE applyStyle:@"Login_Title" toLabel:self.userLoginTitleLabel];
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.userLoginDisclaimerLabel];
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.userLoginSubtitleLabel];
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.userPhoneNumberLabel];
  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.userPhoneCodeLabel];

  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhonePrefixField];
  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhoneNumberField];
  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhoneCodeField];
  //self.navigationController.navigationBar.topItem.title = Localized(@"T069");
  self.navigationItem.title = Localized(@"T069");
  tockenClick = 0;
  [self.resendTokenButton setHidden:NO];
  [self.resendTokenButton setEnabled:YES];
  self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
  self.contentView.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
}

- (void)configureUI
{
  [super configureUI];

  [self.contentScroll addSubview:self.contentView];
  [self.contentScroll setContentSize:self.contentView.frame.size];
  [self.contentScroll contentSizeToFit];


  [self.userPhonePrefixField setCornerRadius:4.0];
  [self.userPhonePrefixField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];
  [self.userPhoneNumberField setCornerRadius:4.0];
  [self.userPhoneNumberField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];
  [self.userPhoneCodeField setCornerRadius:4.0];
  [self.userPhoneCodeField setBorder:1.0 color:[UIColor colorWithWhite:0.737 alpha:1.000]];

  self.userLoginTitleLabel.text = Localized(@"T023");
  self.userPhoneNumberLabel.text = Localized(@"T025");
  self.userPhoneCodeLabel.text = Localized(@"T026");

  self.userLoginSubtitleLabel.text = Localized(@"T024");
  self.userLoginDisclaimerLabel.text = Localized(@"T028");

  [APPSTYLE applyTitle:Localized(@"T029") toButton:self.confirmButton];
  [APPSTYLE applyTitle:Localized(@"T030") toButton:self.cancelButton];
  [APPSTYLE applyTitle:Localized(@"T027") toButton:self.resendTokenButton];
}

- (void)configureObservers
{
  [super configureObservers];
}

- (void)configureNavigation
{
  [super configureNavigation];
  self.navigationItem.title = Localized(@"T069");
}

- (void)configureData
{
  [super configureData];
  NSString *number = [self.msisdn substringFromIndex:2];
  NSString *areaCode = [number substringToIndex:2];
  number = [self.msisdn substringFromIndex:4];
  self.userPhonePrefixField.text = areaCode;
  self.userPhoneNumberField.text = number;
 //self.userPhoneCodeLabel.text = [NSString stringWithFormat:@"%@ %d",Localized(@"T026"), [[USER_MANAGER showMeTokenForMSISDN:self.msisdn] intValue]];
  DLog(@"### CODE #### %d",[[USER_MANAGER showMeTokenForMSISDN:self.msisdn] intValue]);
    NSLog(@"### CODE #### %d",[[USER_MANAGER showMeTokenForMSISDN:self.msisdn] intValue]);
}

- (void)loadData
{
  [super loadData];
}

- (void)layout
{
  [super layout];
}

- (void)dismissObservers
{
  [super dismissObservers];
}

#pragma mark - Interface Builder Actions

- (IBAction)cancelTapped:(UIButton *)sender
{
  [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)resendTocken:(UIButton *)sender
{
  
  if(haveKeyboard){
    return;
  }
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnection];
    return;
  }
  tockenClick++;
  if (tockenClick > 2) {
    [self.resendTokenButton setHidden:YES];
    [self.resendTokenButton setEnabled:NO];
  }
  [USER_MANAGER takeLoginTokenWithMSISDN:self.msisdn];
}
#pragma mark - validation of phone number


- (BOOL)chekUserConfirmCodeField
{
  if ([self.userPhoneCodeField.text length] < 4) {
    [UIAlertView alertWithCause:kAlertCodeNotValid];
    return NO;
  }
  return YES;
}

- (IBAction)confirmTapped:(UIButton *)sender
{
  /*[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserCode];
   [[NSUserDefaults standardUserDefaults] synchronize];
   */
  /*if ([self chekUserConfirmCodeField]){
    [self addActivityIndicator];
  }else{
    return;
  }*/
  if (![self chekUserConfirmCodeField]) {
    return;
  }
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnection];
    return;
  }
  [self addActivityIndicator];
  //if ([USER_MANAGER loginWithMSISDN:self.msisdn code:self.userPhoneCodeField.text]) {
  [USER_MANAGER loginWithMSISDN:self.msisdn code:self.userPhoneCodeField.text WithCompletitionBlock:^(BOOL isUserLogin) {
    if(isUserLogin){
        
        
      [USER_MANAGER loadUserValuesWithCompletitionBlock:^(NSError *error)
       {
         // check code for error
         [self removeActivityIndicator];
         if ([USER_MANAGER isAuthenticated])
         {
           [DATA_SERVICE loadDataModel];
           
           [APP_DELEGATE buildMainStack];
             
             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userWasLogged"];
             [[NSUserDefaults standardUserDefaults] synchronize];

         }
         
         [self initProgressModel];
       }];
    }
    else{
      [self removeActivityIndicator];
    }
  }];
}

#pragma mark - TextField functionsF
//
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  haveKeyboard = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
  haveKeyboard = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

  NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";

  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
  NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
  if (numberOfMatches == 0)
    return NO;

  if (textField == self.userPhoneCodeField) {
    if (self.userPhoneCodeField.text.length >= 4 && [string length] > 0)
      return NO;
    return YES;
  }
  return YES;
}

#pragma mark - initProgressModel

- (void)initProgressModel
{
    
    //NSString *userId = [USER_MANAGER userMsisdn];
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN];
    if(!userStatistic) {
        ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
        [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
    }
    [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN]];
    [PROGRESS_MANAGER updateProgressModel];
}
@end
