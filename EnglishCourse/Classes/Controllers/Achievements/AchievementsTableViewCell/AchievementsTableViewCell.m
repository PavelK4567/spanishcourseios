//
//  AchievementsTableViewCell.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 2/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "AchievementsTableViewCell.h"

@implementation AchievementsTableViewCell{
  
  __weak IBOutlet UIImageView *achievmentImage;
  __weak IBOutlet UIButton *fbButton;
  __weak IBOutlet UIButton *emailButton;
  __weak IBOutlet UILabel *infoLabel;
  AchievementsObject *currentAchievementsObject;
}


- (void)awakeFromNib {
    // Initialization code
  fbButton.layer.cornerRadius = 2;
  emailButton.layer.cornerRadius = 2;
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTextToLabel:(NSString *)text{
  infoLabel.text = text;
}
-(void)setData:(AchievementsObject *)tempAchievementsObject{
  currentAchievementsObject = tempAchievementsObject;
  infoLabel.text = currentAchievementsObject.name;
  [APPSTYLE applyTitle:Localized(@"T620") toButton:fbButton];
  [APPSTYLE applyTitle:Localized(@"T650") toButton:emailButton];
  if([currentAchievementsObject.type isEqualToString:kLevelDiploma]){
    [emailButton setHidden:NO];
    [achievmentImage setImage:[UIImage imageNamed:@"ic_diploma2"]];
    if(currentAchievementsObject.withHonor){
      [achievmentImage setImage:[UIImage imageNamed:@"ic_diploma"]];
    }
  }else{
    [emailButton setHidden:YES];
    [achievmentImage setImage:[UIImage imageNamed:@"ic_badge"]];
  }
}
#pragma mark - Interface Builder Actions

- (IBAction)emailButtonPress:(id)sender {
  [self.cellDelegate pressMailButton:currentAchievementsObject.levelId];
}

- (IBAction)fbButtonPress:(id)sender {
  [self.cellDelegate pressFBButton:currentAchievementsObject.fbLink];
}
@end
