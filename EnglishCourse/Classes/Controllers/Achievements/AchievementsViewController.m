//
//  AchievementsViewController.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 2/6/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "AchievementsViewController.h"
#import "AchievementsObject.h"
#import "LevelCompletitionViewController.h"

@interface AchievementsViewController (){
  
  __weak IBOutlet UILabel *explanation_1_Label;
  __weak IBOutlet UILabel *explanation_2_Label;
  __weak IBOutlet UILabel *explanation_3_Label;
  IBOutlet UIView *defaultView;
  UITableView *achievementsTableView;
  NSMutableArray *arrayDataSource;
  NSIndexPath *selectedCellIndexPath;
  //UIView *moduleView;
  SLComposeViewController *mySLComposerSheet;
  BOOL youHaveSomeAchievement;
}

@end

@implementation AchievementsViewController


- (void)configureUI
{
  [super configureUI];
  self.view.backgroundColor = [UIColor whiteColor];
  [self takeAchievements];
  if([self youHaveMoreThanOneDiploma]){
    achievementsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0,self.view.width,[[UIScreen mainScreen] bounds].size.height-84-23) style:UITableViewStyleGrouped];
    achievementsTableView.dataSource = self;
    achievementsTableView.delegate = self;
    achievementsTableView.separatorColor = [UIColor clearColor];
    achievementsTableView.backgroundColor = [UIColor whiteColor];
    [achievementsTableView registerNib:[UINib nibWithNibName:@"AchievementsTableViewCell" bundle:[NSBundle mainBundle]]
        forCellReuseIdentifier:@"AchievementsCell"];
    [self.view insertSubview:achievementsTableView atIndex:0];
    [achievementsTableView reloadData];
    
    if(achievementsTableView.contentSize.height < [achievementsTableView height]){
      achievementsTableView.contentSize =  CGSizeMake(achievementsTableView.frame.size.width, achievementsTableView.frame.size.height);
    }

    
  }else{
    [self.view addSubview:defaultView];
    [APPSTYLE applyStyle:@"Label_Title" toLabel:explanation_1_Label];
    [APPSTYLE applyStyle:@"Label_Title" toLabel:explanation_2_Label];
    [APPSTYLE applyStyle:@"Label_Title" toLabel:explanation_3_Label];
    [self setLabels];
  }
}

- (void)configureAppearance {
  [super configureAppearance];
  DLog(@"configureAppearance %@", Localized(@"T001"));
  
}

- (void)loadData
{
  [super loadData];
}

- (void)setData{
  [self takeAchievements];
  [defaultView removeFromSuperview];
  if([self youHaveMoreThanOneDiploma])
  {
    
    
    if(!achievementsTableView)
    {
      achievementsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0,self.view.width,[[UIScreen mainScreen] bounds].size.height-84-23) style:UITableViewStyleGrouped];
      achievementsTableView.dataSource = self;
      achievementsTableView.delegate = self;
      achievementsTableView.separatorColor = [UIColor clearColor];
      achievementsTableView.backgroundColor = [UIColor whiteColor];
      [achievementsTableView registerNib:[UINib nibWithNibName:@"AchievementsTableViewCell"
                                                        bundle:[NSBundle mainBundle]]
                                                        forCellReuseIdentifier:@"AchievementsCell"];
      [self.view insertSubview:achievementsTableView atIndex:0];
      [achievementsTableView reloadData];
      
      if(achievementsTableView.contentSize.height < [achievementsTableView height]){
        achievementsTableView.contentSize =  CGSizeMake(achievementsTableView.frame.size.width, achievementsTableView.frame.size.height);
      }
    }
    [achievementsTableView reloadData];
  }else{
    if(achievementsTableView)
    {
      [achievementsTableView removeFromSuperview];
      achievementsTableView = nil;
    }
    [self.view addSubview:defaultView];
    [APPSTYLE applyStyle:@"Label_Title" toLabel:explanation_1_Label];
    [APPSTYLE applyStyle:@"Label_Title" toLabel:explanation_2_Label];
    [APPSTYLE applyStyle:@"Label_Title" toLabel:explanation_3_Label];
    [self setLabels];
  }
  
  
}

-(void)setLabels{
  explanation_1_Label.text =Localized(@"T652");
  explanation_2_Label.text =[NSString stringWithFormat:Localized(@"T645"),kParam043];
  explanation_3_Label.text =[NSString stringWithFormat:Localized(@"T646"),[PROGRESS_MANAGER lessonsInCurrentModule]];
  
}

#pragma marrk takeAchievements
-(void)takeAchievements{
  arrayDataSource = [[NSMutableArray alloc]init];
  NSArray *arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
  NSInteger numberOfLevel = [arrayCourseLevels count];
  youHaveSomeAchievement = NO;
  for (int j = 0; j < numberOfLevel; j++)
  {
   
    NSMutableArray *LevelAchievementsArray = [[NSMutableArray alloc]init];
    [arrayDataSource addObject:LevelAchievementsArray];
    
    NSMutableArray *arrayModulesSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:j];
    NSInteger numberOfModule = [arrayModulesSource count];
    NSInteger numberOfLessonInModule = 1;
    
    /*if([PROGRESS_MANAGER isLevelCompleted:j]){
      DLog(@"Level Diploma");
      LevelData *tempLevelData =  [arrayCourseLevels objectAtIndex:j];

      AchievementsObject *tempAchievementsObject = [[AchievementsObject alloc]init];
      tempAchievementsObject.type = kLevelDiploma;
      tempAchievementsObject.name = [NSString stringWithFormat:Localized(@"T648"), [PROGRESS_MANAGER takeCurentLevelName:j]];
      tempAchievementsObject.fbLink = tempLevelData.fbLink;
      tempAchievementsObject.levelId = tempLevelData.levelID;
      [LevelAchievementsArray addObject:tempAchievementsObject];
      youHaveSomeAchievement = YES;
    }*/
    
    if([PROGRESS_MANAGER areAllLessonsVisitedInLevel:j]) {
      if([PROGRESS_MANAGER haveEnoughPointsInLevel:j]){
      DLog(@"Level Diploma");
      LevelData *tempLevelData =  [arrayCourseLevels objectAtIndex:j];
      
      AchievementsObject *tempAchievementsObject = [[AchievementsObject alloc]init];
      tempAchievementsObject.type = kLevelDiploma;
      tempAchievementsObject.name = [NSString stringWithFormat:Localized(@"T648"), [PROGRESS_MANAGER takeCurentLevelName:j]];
      
      tempAchievementsObject.fbLink = tempLevelData.fbLink;
      tempAchievementsObject.levelId = tempLevelData.levelID;
      tempAchievementsObject.withHonor = NO;
      [LevelAchievementsArray addObject:tempAchievementsObject];
      youHaveSomeAchievement = YES;
      //diploma with honor
      if( [PROGRESS_MANAGER haveEnoughStarsInLevel:j]){
        tempAchievementsObject.withHonor = YES;
        tempAchievementsObject.name = [NSString stringWithFormat:Localized(@"T649"), [PROGRESS_MANAGER takeCurentLevelName:j]];
      }
      }
      
    }
    
    Section *currentSection;
    for (int i = 0; i < numberOfModule; i++)
    {
      currentSection = (Section *)[arrayModulesSource objectAtIndex:i];
      numberOfLessonInModule = [currentSection.unitsArray count];
      if ([PROGRESS_MANAGER checkForModuleCertificate:i inLevel:j]){
        DLog(@"Modul Sertifikat");
        AchievementsObject *tempAchievementsObject = [[AchievementsObject alloc]init];
        tempAchievementsObject.type = kModulSertifikat;
        tempAchievementsObject.name = [NSString stringWithFormat:Localized(@"T651"), i+1];
        tempAchievementsObject.fbLink = currentSection.fbLink;
        [LevelAchievementsArray addObject:tempAchievementsObject];
        youHaveSomeAchievement = YES;
      }
    }
    
    
  }

}
#pragma mark check data
-(BOOL)youHaveMoreThanOneDiploma{
  return youHaveSomeAchievement;
}

#pragma mark - TableView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  //arrayDataSource
  NSMutableArray *LevelAchievementsArray = [arrayDataSource objectAtIndex:section];
  if(section == 0){
    if([LevelAchievementsArray count] == 0){
      return 50;
    }
    return 105;
  }
  
  if([LevelAchievementsArray count] == 0){
    return 0;
  }
  return 55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  if(section == 3){
    return 33;
  }
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(selectedCellIndexPath != nil
     && [selectedCellIndexPath compare:indexPath] == NSOrderedSame)
    return 120;
  
  return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  
  return [arrayDataSource count];//[[[DataService sharedInstance] findAllLevelsForCourse] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  NSMutableArray *LevelAchievementsArray = [arrayDataSource objectAtIndex:section];
  return [LevelAchievementsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
  static NSString *strCell = @"AchievementsCell";
  
  AchievementsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCell];
  //cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.size.width, cell.frame.size.height);
  if (cell == nil) {
    
    cell = [[AchievementsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strCell];
  }
  cell.cellDelegate = self;
  
  AchievementsObject *tempAchievementsObject = arrayDataSource[indexPath.section][indexPath.row] ;
  [cell setData:tempAchievementsObject];
  
  
  //[cell setTextToLabel:tempAchievementsObject.name];
  return cell;
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  NSIndexPath *previousSelectedIndexPath = selectedCellIndexPath;  // <- save previously selected cell
  selectedCellIndexPath = indexPath;
  if (previousSelectedIndexPath && previousSelectedIndexPath.row == selectedCellIndexPath.row && previousSelectedIndexPath.section == selectedCellIndexPath.section) {
    selectedCellIndexPath = nil;
  }
  if (previousSelectedIndexPath) { // <- reload previously selected cell (if not nil)
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousSelectedIndexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
  }
  if (selectedCellIndexPath)
  [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedCellIndexPath]
                   withRowAnimation:UITableViewRowAnimationAutomatic];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  if(section == 0){
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 105)];
    UIView *lineHeader = [[UIView alloc] initWithFrame:CGRectMake(20, 55, tableView.width-40, 0.5)];
    UILabel *instructionLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.width-40, 60)];
    instructionLabel.numberOfLines = 0;
    UILabel *levelNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 58, tableView.width-40, 40)];
    [APPSTYLE applyStyle:@"Login_Title" toLabel:levelNameLabel];
    [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:instructionLabel];
    levelNameLabel.text = [PROGRESS_MANAGER takeCurentLevelName:section];
    instructionLabel.text = Localized(@"T647");;
    [lineHeader setBackgroundColor:[APPSTYLE colorForType:@"Native_Pink_Color"]];
    [viewHeader setBackgroundColor:[UIColor clearColor]];
    [viewHeader addSubview:lineHeader];
    [viewHeader addSubview:levelNameLabel];
    [viewHeader addSubview:instructionLabel];
    NSMutableArray *LevelAchievementsArray = [arrayDataSource objectAtIndex:section];
    if([LevelAchievementsArray count] == 0){
      levelNameLabel.text = @"";
      [lineHeader removeFromSuperview];
      [levelNameLabel removeFromSuperview];
      //[viewHeader setFrame:CGRectMake(0, 0, tableView.width, 0)];
    }
    return viewHeader;
    
  }else{
    
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 55)];
    UIView *lineHeader = [[UIView alloc] initWithFrame:CGRectMake(20, 0, tableView.width-40, 0.5)];
    UILabel *levelNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, tableView.width-40, 40)];
    [APPSTYLE applyStyle:@"Login_Title" toLabel:levelNameLabel];
    levelNameLabel.text = [PROGRESS_MANAGER takeCurentLevelName:section];
    [lineHeader setBackgroundColor:[APPSTYLE colorForType:@"Native_Pink_Color"]];
    [viewHeader setBackgroundColor:[UIColor clearColor]];
    [viewHeader addSubview:lineHeader];
    [viewHeader addSubview:levelNameLabel];
    
    NSMutableArray *LevelAchievementsArray = [arrayDataSource objectAtIndex:section];
    if([LevelAchievementsArray count] == 0){
      levelNameLabel.text = @"";
      [lineHeader removeFromSuperview];
      [levelNameLabel removeFromSuperview];
      [viewHeader setFrame:CGRectMake(0, 0, tableView.width, 0)];
    }
    
    return viewHeader;
  }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
  if(section == 3){
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 33)];
    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tableView.width, 23)];
    [viewHeader setBackgroundColor:[UIColor  clearColor]];//old color Purple_Main_Color
    [colorView setBackgroundColor:[APPSTYLE colorForType:@"New_Purple_Color"]];//old color Purple_Main_Color
    [viewHeader addSubview:colorView];
    return viewHeader;
  }
  return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 0)];;

 
}
/*- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  static CGFloat previousOffset;
  CGRect rect = moduleView.frame;
  rect.origin.y += previousOffset - scrollView.contentOffset.y;
  previousOffset = scrollView.contentOffset.y;
  moduleView.frame = rect;
}*/

#pragma mark - Achievements TableViewCell Delegate

- (void)pressFBButton:(NSString *)fbLink{
  mySLComposerSheet = [[SLComposeViewController alloc] init];
  mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
  //[mySLComposerSheet setInitialText:@"Place Text Here"];
  [mySLComposerSheet addURL:[NSURL URLWithString:fbLink]];

  [self presentViewController:mySLComposerSheet animated:YES completion:nil];
}

- (void)pressMailButton:(NSInteger)levelId {
    
  UserDetailsViewController *userDetailsViewController = [[UserDetailsViewController alloc] initWithNibName:@"UserDetailsViewController" bundle:nil];
   //change this with levelId
  [userDetailsViewController setLevelId:levelId];
  [[APP_DELEGATE mainController] presentViewController:userDetailsViewController animated:YES completion:nil];
    
}

@end
