//
//  TutorialViewController.h
//  EnglishCourse
//
//  Created by Aleksandar Mitrovski on 7/9/18.
//  Copyright © 2018 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ImageRecognitionFramework/ImageRecognitionFramework.h>

@interface TutorialViewController : UIViewController<UIScrollViewDelegate>
    
@property(nonatomic, strong) CameraViewController *customCam;
    
- (void)showInView:(UIView *)aView animated:(BOOL)animated;

@end
