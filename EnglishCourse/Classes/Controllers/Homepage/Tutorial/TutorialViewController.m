//
//  TutorialViewController.m
//  EnglishCourse
//
//  Created by Aleksandar Mitrovski on 7/9/18.
//  Copyright © 2018 LaMark. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController () {
    UIView *main;
    bool isPresentingCam;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIPageControl *pageIndicator;
    
@end

@implementation TutorialViewController

    @synthesize customCam;
    
    
- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [super viewWillDisappear:animated];
}
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [super viewWillAppear:animated];
    
    if (isPresentingCam) {
        isPresentingCam = NO;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self buildTutorial];
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    isPresentingCam = NO;
    
    //[_pageIndicator addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
-(void) presentCam {
    isPresentingCam = YES;
    customCam = [[CameraViewController alloc] init];
    [customCam setLng:@"es"];
    [customCam setPhoneNum:[USER_MANAGER userMsisdn]];
    [self presentViewController:customCam animated:YES completion:nil];
}
    
#pragma mark displa/hide animations
    
- (void)showInView:(UIView *)aView animated:(BOOL)animated {
    dispatch_async(dispatch_get_main_queue(), ^{
        main = aView;
        [aView addSubview:self.view];
        if (animated) {
            [self showAnimate];
        }
    });
}
    
- (void)showAnimate {
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 0.95;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}
    
- (void)removeAnimate {
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            //[self.view removeFromSuperview];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}
    
#pragma mark -
#pragma mark init tutorial
    /**
     * Build the tutorial scroll view
     */
- (void) buildTutorial {
    _scroller.delegate = self;
    _scroller.pagingEnabled = YES;
    _scroller.showsHorizontalScrollIndicator = NO;
    _scroller.showsVerticalScrollIndicator = NO;
    [_scroller setFrame:CGRectMake(0, 0, LM_WIDTH, LM_DEMO_SCROLL_HEIGHT)];
    [_scroller setContentSize:CGSizeMake(LM_WIDTH * 2, LM_DEMO_SCROLL_HEIGHT)];
    
    
    UIImage *imgA = [UIImage imageNamed:@"tutorial_img_1"];
    UIImage *imgB = [UIImage imageNamed:@"tutorial_img_2"];
    
    UIImageView *holderA = [[UIImageView alloc] initWithImage:imgA];
    [holderA setOrigin:CGPointZero];
    [holderA setSize:_scroller.size];
    [_scroller addSubview:holderA];
    
    UIImageView *holderB = [[UIImageView alloc] initWithImage:imgB];
    [holderB setSize:_scroller.size];
    [holderB setOrigin:CGPointMake(LM_WIDTH, 0)];
    [_scroller addSubview:holderB];
    
}
#pragma mark -
#pragma mark Event Handlers
    
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = _scroller.frame.size.width;
    float fractionalPage = _scroller.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    _pageIndicator.currentPage = page;
}
- (IBAction)okPressed:(UIButton *)sender {
    //[self removeAnimate];
    [self presentCam];
}
    
    /**
     * Handle the changing of the page on the scroll view
     */
-(void) pageChanged {
    long pageNumber = _pageIndicator.currentPage;
    CGRect frame = _scroller.frame;
    frame.origin.x = frame.size.width * pageNumber;
    frame.origin.y = 0;
    [_scroller scrollRectToVisible:frame animated:YES];
}
    
@end
