//
//  ProfileView.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/1/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PieChart.h"

@interface ProfileView : UIView

-(void)setUI;
-(void)setData;

@property (nonatomic, strong) PieChart *pie;

@end
