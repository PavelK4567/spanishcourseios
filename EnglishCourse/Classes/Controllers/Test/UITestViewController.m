//
//  UIQuestionViewController.m
//  K1000
//
//  Created by Action-Item on 5/5/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "UITestViewController.h"
#import "TestLogic.h"
#import "LevelSelectionDetailsViewController.h"


@interface UITestViewController () {
    float screenWidth;
    int currentIndex;
}

@end

@implementation UITestViewController

@synthesize currentIndexPath = _currentIndexPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title =  Localized(@"T470");;
    }
    return self;
}

- (void)viewDidLoad
{
    FeedbackScreenisOpen = NO;
    [super viewDidLoad];
    [self reloadData];
    [self layoutData];
    
    _questionIndexLabel.textColor = [APPSTYLE colorForType:@"New_Purple_Color"];
  
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    currentIndex = 0;
    [_scrollView setScrollEnabled:NO];
    int page = [self flattenIndexPath:_currentIndexPath];
    if (page > 0) {
        
        CGRect frame = _scrollView.frame;
        frame.origin.x = screenWidth * page;
        
        [_scrollView scrollRectToVisible:frame animated:YES];
    }
}
    
-(void)viewWillDisappear:(BOOL)animated{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"YYYY.MM.dd.HH.mm.ss"];
  NSString *endDate =  [dateFormatter stringFromDate:[NSDate date]];
  if(!FeedbackScreenisOpen){
  [API_REQUEST_MANAGER placementTestForUser:[USER_MANAGER userMsisdnString]
                                  contentId:[REQUEST_MANAGER contentID]
                                    groupId:[REQUEST_MANAGER groupID]
                                 phoneModel:[[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]
                                     osType:@"IOS"
                                  osVersion:[[UIDevice currentDevice] systemVersion]
                                  completed:@"false"
                                      grade:@"0"
                           recommendedLevel:@"-1"
                                   dateTime:endDate
                            CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
                              
                              
                            }];
  }
  [super viewWillDisappear:animated];
}
- (NSIndexPath *)currentIndexPath
{
    return _currentIndexPath;
}

-(void)setCurrentIndexPath:(NSIndexPath *)currentIndexPath
{
    _currentIndexPath = currentIndexPath;

    //  if ([self currentIndexInFlattenQuestions] == [self totalNumberOfQuestions]-1) {
    //    self.navigationItem.rightBarButtonItem = 0;
    //  } else {
    //    self.navigationItem.rightBarButtonItem = BARBUTTON(NLS(@"OK"), @selector(openFeedbackScreen));
    //  }

//    [_scrollView scrollRectToVisible:CGRectMake([self flattenIndexPath:_currentIndexPath] * _scrollView.width, 0, _scrollView.width, _scrollView.height) animated:YES];

    CGRect frame = _scrollView.frame;
    int page = [self flattenIndexPath:_currentIndexPath];
    frame.origin.x = screenWidth * page;
    
    [_scrollView scrollRectToVisible:frame animated:YES];
    
    [self updateToolbarData];
    [self updateButtons];
}

- (void)reloadData
{
  _currentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
  //_selectedColorArray = [[NSArray alloc] initWithObjects:[UIColor colorWithHexString:@"68A7CC"], [UIColor colorWithHexString:@"FDD035"], [UIColor colorWithHexString:@"B4C64E"], nil];
  //_defaultColorArray = [[NSArray alloc] initWithObjects:[UIColor colorWithHexString:@"025C91"], [UIColor colorWithHexString:@"D68000"], [UIColor colorWithHexString:@"003B06"], nil];
    _defaultColorArray = [[NSArray alloc] initWithObjects:[UIColor whiteColor], [UIColor whiteColor], [UIColor whiteColor], nil];
    _selectedColorArray = [[NSArray alloc] initWithObjects:[APPSTYLE colorForType:@"Test_Select_Color"], [APPSTYLE colorForType:@"Test_Select_Color"], [APPSTYLE colorForType:@"Test_Select_Color"], nil];//[[NSArray alloc] initWithObjects:[APPSTYLE colorForType:@"Purple_Main_Color"], [APPSTYLE colorForType:@"Purple_Main_Color"], [APPSTYLE colorForType:@"Purple_Main_Color"], nil];
  
  TestLogic *tempTestLogic = [[TestLogic alloc]init];
  _testCategoryArray = [tempTestLogic filterQuestionsFromEachCategory:5];
}

- (void)removeAllSubviewsFromScroll {
  while (_scrollView.subviews.count) {
    UIView* child = _scrollView.subviews.lastObject;
    [child removeFromSuperview];
  }
}
- (void)layoutData
{
    [self removeAllSubviewsFromScroll];

    screenWidth = self.view.size.width;
    
    int i = 0;
    for (TestCategory *testCategory in _testCategoryArray)
    {
        for (Question *question in testCategory.questions)
        {
            UITestQuestionsView *testQuestionView = [UITestQuestionsView loadFromNib];
            [testQuestionView setTestCategory:testCategory
                                 withQuestion:question
                              andDefaultColor:[_defaultColorArray objectAtIndex:i%3]
                             andSelectedColor:[_selectedColorArray objectAtIndex:i%3]];
            testQuestionView.left = i * _scrollView.width;
            testQuestionView.height = _scrollView.height;
            [testQuestionView setFrame:CGRectMake(screenWidth * i, 0, screenWidth, _scrollView.size.height)];
            testQuestionView.delegate = self;
            [_scrollView addSubview:testQuestionView];
            testQuestionView.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];

            i++;
        }
    }

    //_scrollView.contentSize = CGSizeMake(i * _scrollView.width, _scrollView.height);
    _scrollView.contentSize = CGSizeMake(i * screenWidth, _scrollView.size.height);
    _scrollView.contentOffset = CGPointMake(0, 0);

    [self updateToolbarData];
    [self updateButtons];
}

- (int)correctAnswers
{
    int correctAnswers = 0;

    for (TestCategory *testCategory in _testCategoryArray)
    {
        for (Question *question in testCategory.questions)
        {
            if (question.userSelectedAnswer == [question correctAnswerIndex])
            {
                correctAnswers += 1;
            }
        }
    }

    return correctAnswers;
}

- (double)userScore
{
    if ([self totalNumberOfQuestions] == 0) return 0;

    return ((double)[self correctAnswers] / (double)[self totalNumberOfQuestions]) * 100.0;
}

- (double)userLevel
{
    double userTotalScore = [self userScore];
    if (userTotalScore >= 0 && userTotalScore <= 25)
    {
        return userLevelOne;
    }
    else if (userTotalScore >= 26 && userTotalScore <= 50)
    {
        return userLevelTwo;
    }
    else if (userTotalScore >= 51 && userTotalScore <= 70)
    {
        return userLevelThree;
    }
    else if (userTotalScore >= 71 && userTotalScore <= 90)
    {
        return userLevelFour;
    }
    else
    {
        return userLevelFive;
    }
}

- (int)totalNumberOfQuestions
{
    int i = 0;
    for (TestCategory *testCategory in _testCategoryArray)
    {
        i += testCategory.questions.count;
    }
    return i;
}

- (void)updateButtons
{
    int flattenIndex = [self currentIndexInFlattenQuestions];
    _previousQuestionButton.enabled = flattenIndex > 0;
    //_nextQuestionButton.enabled = flattenIndex < [self totalNumberOfQuestions]-1;
}

- (void)updateToolbarData
{
    _questionIndexLabel.text = [NSString stringWithFormat:@"%@ %d/%d", Localized(@"Question"), [self currentIndexInFlattenQuestions]+1, [self totalNumberOfQuestions]];
}

- (void)openFeedbackScreen
{
  
  NSArray *arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
  
  int score = [self userScore];
  float scoreStep = 100/[arrayCourseLevels count];
  int levelNumber = score/scoreStep;
  if(levelNumber >([arrayCourseLevels count]-1))
  levelNumber=([arrayCourseLevels count]-1);

  LevelData *level = [arrayCourseLevels objectAtIndex:levelNumber];
  
  LevelSelectionDetailsViewController *viewController = [LevelSelectionDetailsViewController new];
  viewController.levelData = level;
  viewController.levelIndex = levelNumber;
  [self.navigationController pushViewController:viewController animated:YES];
  
  
  NSString *levelIdString = [NSString stringWithFormat:@"%d",level.levelID];
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"YYYY.MM.dd.HH.mm.ss"];
  NSString *endDate =  [dateFormatter stringFromDate:[NSDate date]];
  
  FeedbackScreenisOpen = YES;
  
  [API_REQUEST_MANAGER placementTestForUser:[USER_MANAGER userMsisdnString]
                                  contentId:[REQUEST_MANAGER contentID]
                                    groupId:[REQUEST_MANAGER groupID]
                                 phoneModel:[[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]
                                     osType:@"IOS"
                                  osVersion:[[UIDevice currentDevice] systemVersion]
                                  completed:@"true"
                                      grade:[NSString stringWithFormat:@"%d",score]
                           recommendedLevel:levelIdString
                                   dateTime:endDate
   CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
   
   
   }];

  /*[API_REQUEST_MANAGER placementTestForUser:@"5511123456789" contentId:@"1" groupId:@"1" phoneModel:@"Samsung/S3" osType:@"Android" osVersion:@"4.1.3" completed:@"false" grade:@"5" recommendedLevel:@"1" dateTime:@"2014.12.17.11.11" CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
    
    
  }];*/
}

- (int)currentIndexInFlattenQuestions
{
    int index = [self flattenIndexPath:_currentIndexPath];
    return index;
}

- (int)flattenIndexPath:(NSIndexPath *)indexPath
{
    int sum = 0;
    for (int i=0; i<indexPath.section; i++)
    {
        TestCategory *category = _testCategoryArray[i];
        sum += category.questions.count;
    }

    return sum + indexPath.row;
}

- (NSArray *)flattenQuestions
{
    NSMutableArray *questions = [NSMutableArray array];

    for (TestCategory *testCategory in _testCategoryArray)
    {
        [questions addObjectsFromArray:testCategory.questions];
    }

    return questions;
}

- (NSIndexPath *)updateNextIndexPath:(NSIndexPath *)currentIndexPath
{
    TestCategory *currentCategory = _testCategoryArray[currentIndexPath.section];
    if (currentCategory.questions.count-1 == currentIndexPath.row)
    {
        // we are at the end of questions array inside current category
        return [NSIndexPath indexPathForRow:0 inSection:currentIndexPath.section + 1];
    }
    else
    {
        return [NSIndexPath indexPathForRow:currentIndexPath.row+1 inSection:currentIndexPath.section];
    }
}

- (NSIndexPath *)updatePreviousIndexPath:(NSIndexPath *)currentIndexPath
{
    TestCategory *currentCategory = _testCategoryArray[currentIndexPath.section];
    if (currentIndexPath.row == 0)
    {
        currentCategory = _testCategoryArray[currentIndexPath.section-1];
        return [NSIndexPath indexPathForRow:currentCategory.questions.count-1 inSection:currentIndexPath.section - 1];
    }
    else
    {
        return [NSIndexPath indexPathForRow:currentIndexPath.row-1 inSection:currentIndexPath.section];
    }
}

- (NSIndexPath *)indexPathFromFlattenIndex:(int)index
{
    int i = -1;
    int section = 0;
    int row = 0;

    BOOL found = NO;
    for (TestCategory *testCategory in _testCategoryArray)
    {
        row = 0;
        for (Question *question in testCategory.questions)
        {
            i++;
            if (i == index)
            {
                found = YES;
                break;
            }
            row++;
        }
        if (found)
            break;
        section++;
    }

    return [NSIndexPath indexPathForRow:row inSection:section];
}

/* UITestQuestionsView Delegates */

- (void)testQuestionsView:(UITestQuestionsView *)view answerSelected:(int)index
{
    // jump to next question
    if ([self currentIndexInFlattenQuestions] == [self totalNumberOfQuestions]-1)
    {
        // we are on last question. keren wanted it to jump to the feedback
        [self openFeedbackScreen];
        return;
    }
    [self setCurrentIndexPath:[self updateNextIndexPath:_currentIndexPath]];
}

/* End */

/* UIScrollView Delegates */

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int flattenIndexPath = floor(_scrollView.contentOffset.x / _scrollView.width);

//    _currentIndexPath = [self indexPathFromFlattenIndex:flattenIndexPath];
//
//    [self updateToolbarData];
//    [self updateButtons];

    //  if ([self currentIndexInFlattenQuestions] == [self totalNumberOfQuestions]-1) {
    //    self.navigationItem.rightBarButtonItem = 0;
    //  } else {
    //    self.navigationItem.rightBarButtonItem = BARBUTTON(NLS(@"OK"), @selector(openFeedbackScreen));
    //  }
}

/* End */

/* UIButton Delegates */

- (IBAction)nextQuestionClicked
{
    if ([self currentIndexInFlattenQuestions] == [self totalNumberOfQuestions]-1) {
        [self openFeedbackScreen];
        return;
    } else {
        _currentIndexPath = [self updateNextIndexPath:_currentIndexPath];
    }
    [self setCurrentIndexPath:_currentIndexPath];
}

- (IBAction)previousQuestionClicked
{
    if ([self currentIndexInFlattenQuestions] == 0) {
        return;
    } else {
        _currentIndexPath = [self updatePreviousIndexPath:_currentIndexPath];
    }

    [self setCurrentIndexPath:_currentIndexPath];
}

/* End */

@end
