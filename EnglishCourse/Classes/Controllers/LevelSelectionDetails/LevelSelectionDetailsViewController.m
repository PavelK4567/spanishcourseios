//
//  LevelSelectionDetailsViewController.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LevelSelectionDetailsViewController.h"
#import "LessonSelectionViewController.h"

@interface LevelSelectionDetailsViewController ()

@end

@implementation LevelSelectionDetailsViewController
- (void)configureUI
{
  [super configureUI];
  [APPSTYLE applyStyle:@"Label_Title_Puple" toLabel:titleLabel];
    titleLabel.textColor = [APPSTYLE colorForType:@"Purple_Main_Color"];
  [APPSTYLE applyStyle:@"Label_Title" toLabel:screenDescriptionLabel];
  self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
}
- (void)configureAppearance {
  
  [super configureAppearance];
  titleLabel.text = self.levelData.levelName;
  screenDescriptionLabel.text = self.levelData.levelDescription;
  [levelImageView setImageFromDocumentsResourceFile:self.levelData.image];
  [APPSTYLE applyTitle:Localized(@"T459") toButton:startButton];
  [APPSTYLE applyStyle:@"Button_Level_Selection" toButton:startButton];
  startButton.backgroundColor = [APPSTYLE colorForType:@"Purple_Main_Color"];
  startButton.layer.cornerRadius = 2.0;
  self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
}
- (void)configureNavigation {
  [super configureNavigation];
  self.navigationItem.title = Localized(@"T423");
  //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"color.png"] forBarMetrics:UIBarMetricsDefault];
  //[UIBarButtonItem.appearance setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];

}
- (void)loadData
{
  
    DLog(@"Level index - %ld", self.levelIndex);
    
}

- (IBAction)startTheLevel:(id)sender {
    
    [PROGRESS_MANAGER saveCurrentLevelIndex:self.levelIndex forUser:[USER_MANAGER userMsisdn]];
    [PROGRESS_MANAGER setCurrentLevelIndex:self.levelIndex];
    
    [APP_DELEGATE buildMainStackWithLevelIndex:self.levelIndex];
    
}

@end
