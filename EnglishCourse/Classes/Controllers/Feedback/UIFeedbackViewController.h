//
//  UIFeedbackViewController.h
//  K1000
//
//  Created by Action-Item on 5/7/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//


enum {
  userLevelOne,
  userLevelTwo,
  userLevelThree,
  userLevelFour,
  userLevelFive
};

typedef NSInteger UserLevel;

@interface UIFeedbackViewController : LMBaseViewController {
  IBOutlet UIImageView *_userLevelImageView;
  IBOutlet UILabel *_userNameLabel;
  IBOutlet UILabel *_userScoreLabel;
  IBOutlet UILabel *_scoreSummaryTitleLabel;
  IBOutlet UILabel *_scoreSummaryDescriptionLabel;
  IBOutlet UIButton *_doneButton;
  IBOutlet UIButton *_registerButton;

  int _userTotalScore;
  int _userLevel;
}

- (IBAction)doneButtonClicked:(UIButton *)sender;
- (IBAction)registerButtonClicked:(UIButton *)sender;

- (void)setUserScore:(double)score andLevel:(int)level;

@property(nonatomic, unsafe_unretained, readonly) double userScore;
@property(nonatomic, unsafe_unretained, readonly) int userLevel;

@end