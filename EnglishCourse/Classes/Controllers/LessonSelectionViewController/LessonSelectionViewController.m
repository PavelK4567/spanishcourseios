    //
//  ECLessonViewController.m
//  KMGame
//
//  Created by Dimitar Shopovski on 11/14/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "LessonSelectionViewController.h"
#import "ECLessonTableViewCell.h"
#import "UserStatistic.h"
#import "ProgressMenager.h"
#import "SimplePingHelper.h"


@interface LessonSelectionViewController () {

    NSInteger sectionCompare;
    NSInteger unitCompare;
    BOOL isChanged;
    
    NSInteger moduleAvailForGuest;
    NSInteger lessonAvailForGuest;
    
}
@end

@implementation LessonSelectionViewController
@synthesize maxModuleIndex, maxUnitIndex;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tbLevels.showsVerticalScrollIndicator = NO;
    self.tbLevels.separatorColor = [UIColor clearColor];
    [self.tbLevels registerNib:[UINib nibWithNibName:@"ECLessonTableViewCell" bundle:[NSBundle mainBundle]]
        forCellReuseIdentifier:@"Cell"];
    
    //[PROGRESS_MANAGER currentLevelIndex]
    
    self.arrayDataSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
    moduleView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 23, 0, 10, 250)];
    moduleView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:moduleView];
    [self makeModuleViews];
    
    [SyncService sharedInstance].delegate = self;
    
    NSDictionary *dictMaxValues = (NSDictionary *)[PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
    maxUnitIndex = [[dictMaxValues objectForKey:@"unitIndex"] integerValue];
    maxModuleIndex = [[dictMaxValues objectForKey:@"modulIndex"] integerValue];
    
}

- (void)setCompareProperties:(NSInteger)module unit:(NSInteger)unit {
    
    NSInteger numOfModules = [self.arrayDataSource count];
    Section *currentSection = (Section *)[self.arrayDataSource objectAtIndex:module];
    
    NSInteger numOfUnits = [currentSection.unitsArray count];
    
    if (unit+1 == numOfUnits && module+1 == numOfModules) {
        
        sectionCompare = module;
        unitCompare = unit;
        
    }
    else if (unit+1 == numOfUnits) {
        
        sectionCompare = module+1;
        unitCompare = 0;
    }
    else if (unit+1<numOfUnits) {
        
        sectionCompare = module;
        unitCompare = unit+1;
        
    }
    else {
        
        sectionCompare = module+1;
        unitCompare = 0;
        
    }
    
}

#pragma mark - select lesson

- (void)changeSelectedLesson {
    
//    DLog(@"changeSelectedLesson - level index dimitar - %ld", self.levelIndex);

    NSDictionary *dictMaxValues = (NSDictionary *)[PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
    maxUnitIndex = [[dictMaxValues objectForKey:@"unitIndex"] integerValue];
    maxModuleIndex = [[dictMaxValues objectForKey:@"modulIndex"] integerValue];
    
    DLog(@"===================================== Change selected lesson: Max index - %@", [PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]]);
    
    
    if (self.lsvcLevelIndex != [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]) {
        
        self.lsvcLevelIndex = [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]];
        [PROGRESS_MANAGER setCurrentLevelIndex:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
        
    }
    
    self.arrayDataSource = nil;
    self.arrayDataSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:self.lsvcLevelIndex];
    
    if([USER_MANAGER isGuest]) {
        
        NSIndexPath *maxAvailForGuest = [self findMaxValuesForGuest:[LMHelper getNumberOfGestCredits] arrayLessons:self.arrayDataSource];
        moduleAvailForGuest = maxAvailForGuest.section;
        lessonAvailForGuest = maxAvailForGuest.row;
    }
    
    NSInteger lastVisitedSection = [PROGRESS_MANAGER getLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger lastVisitedUnit = [PROGRESS_MANAGER getLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    
    DLog(@"===================================== Change selected lesson: Last visited - %ld %ld", lastVisitedSection, lastVisitedUnit);

    if (maxModuleIndex == lastVisitedSection && maxUnitIndex == lastVisitedUnit) {
        
        isChanged = YES;
        [self setCompareProperties:maxModuleIndex unit:maxUnitIndex];
    }
    else {
        
        isChanged = NO;
    }
    
    NSIndexPath *unitIndexPath = [NSIndexPath indexPathForRow:lastVisitedUnit inSection:lastVisitedSection];

    if ([self.tbLevels cellForRowAtIndexPath:unitIndexPath]) {
        
        [self.tbLevels reloadData];

        [self.tbLevels selectRowAtIndexPath:unitIndexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];

    }
    else {
        
        //wait for the row to be created
        [self performSelectorOnMainThread:@selector(reloadTableAfterDelay) withObject:nil waitUntilDone:YES];
        
    }
}

- (void)backFromFinishedLevelGoOnTop {
    
    NSDictionary *dictMaxValues = (NSDictionary *)[PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
    maxUnitIndex = [[dictMaxValues objectForKey:@"unitIndex"] integerValue];
    maxModuleIndex = [[dictMaxValues objectForKey:@"modulIndex"] integerValue];
    
        DLog(@"===================================== backFromFinishedLevelGoOnTop: Max index - %@", [PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]]);
    
    if (self.lsvcLevelIndex != [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]) {
        
        self.lsvcLevelIndex = [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]];
        [PROGRESS_MANAGER setCurrentLevelIndex:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    }
    
    self.arrayDataSource = nil;
    self.arrayDataSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:[PROGRESS_MANAGER currentLevelIndex]];
    
    if([USER_MANAGER isGuest]) {
        
        NSIndexPath *maxAvailForGuest = [self findMaxValuesForGuest:[LMHelper getNumberOfGestCredits] arrayLessons:self.arrayDataSource];
        moduleAvailForGuest = maxAvailForGuest.section;
        lessonAvailForGuest = maxAvailForGuest.row;
    }
    
    [self.tbLevels reloadData];
    [self makeModuleViews];
    
    
    NSInteger lastVisitedSection = [PROGRESS_MANAGER getLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger lastVisitedUnit = [PROGRESS_MANAGER getLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    //    DLog(@"===================================== Change selected lesson: Last visited - %ld %ld", lastVisitedSection, lastVisitedUnit);
    
    if (maxModuleIndex == lastVisitedSection && maxUnitIndex == lastVisitedUnit) {
        
        isChanged = YES;
        [self setCompareProperties:maxModuleIndex unit:maxUnitIndex];
    }
    else {
        
        isChanged = NO;
    }
    
    NSIndexPath *unitIndexPath = [NSIndexPath indexPathForRow:lastVisitedUnit inSection:lastVisitedSection];
    
    
    [self.tbLevels selectRowAtIndexPath:unitIndexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    
    
    
}

- (void)reloadTableAfterDelay {
    
    [self.tbLevels reloadData];
    
    NSInteger lastVisitedSection = [PROGRESS_MANAGER getLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger lastVisitedUnit = [PROGRESS_MANAGER getLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
        
    NSIndexPath *unitIndexPath = [NSIndexPath indexPathForRow:lastVisitedUnit inSection:lastVisitedSection];
    
//    if ([self.tbLevels cellForRowAtIndexPath:unitIndexPath]) {
    
//    UITableViewCell *cell = [self.tbLevels cellForRowAtIndexPath:unitIndexPath];
    
    Section *currentSection = (Section *)[self.arrayDataSource objectAtIndex:lastVisitedSection];
    Unit *unit = (Unit *)[currentSection.unitsArray objectAtIndex:lastVisitedUnit];

    
    if (unit) {
    
        [self.tbLevels selectRowAtIndexPath:unitIndexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];

    }

    
//    }

}



#pragma mark - Reload table view

- (void)reloadLessonSelectionViewController {
    
    NSDictionary *dictMaxValues = (NSDictionary *)[PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
    maxUnitIndex = [[dictMaxValues objectForKey:@"unitIndex"] integerValue];
    maxModuleIndex = [[dictMaxValues objectForKey:@"modulIndex"] integerValue];

    DLog(@"===================================== reloadLessonSelectionViewController: Max index - %@", [PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]]);
    
    if (self.lsvcLevelIndex != [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]) {
        
        self.lsvcLevelIndex = [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]];
        [PROGRESS_MANAGER setCurrentLevelIndex:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    }
    
    self.arrayDataSource = nil;
    self.arrayDataSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:[PROGRESS_MANAGER currentLevelIndex]];
    
    if([USER_MANAGER isGuest]) {
        
        NSIndexPath *maxAvailForGuest = [self findMaxValuesForGuest:[LMHelper getNumberOfGestCredits] arrayLessons:self.arrayDataSource];
        moduleAvailForGuest = maxAvailForGuest.section;
        lessonAvailForGuest = maxAvailForGuest.row;
    }
    
    [self.tbLevels reloadData];
    [self makeModuleViews];
    

    NSInteger lastVisitedSection = [PROGRESS_MANAGER getLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger lastVisitedUnit = [PROGRESS_MANAGER getLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
//    DLog(@"===================================== Change selected lesson: Last visited - %ld %ld", lastVisitedSection, lastVisitedUnit);

    NSIndexPath *unitIndexPath = [NSIndexPath indexPathForRow:lastVisitedUnit inSection:lastVisitedSection];
    
//    if ([self.tbLevels cellForRowAtIndexPath:unitIndexPath]) {
    
        [self.tbLevels selectRowAtIndexPath:unitIndexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        
//    }

    
}


#pragma mark - TableView
- (void)makeModuleViews {
    
    [self clearModuleView];
    
    NSInteger numberOfModule = [self.arrayDataSource count];
    NSInteger curentYPosition = 0;
    NSInteger numberOfLessonInModule = 1;
  
    Section *currentSection;

    for (int i = 0; i < numberOfModule; i++) {
      
        ECLessonTableViewCell *cell = [self.tbLevels dequeueReusableCellWithIdentifier:@"Cell"];
        
        currentSection = (Section *)[self.arrayDataSource objectAtIndex:i];
        numberOfLessonInModule = [currentSection.unitsArray count];
      
        UIView *tempModuleView = [[UIView alloc] initWithFrame:CGRectMake(0, 5+curentYPosition, 23, numberOfLessonInModule * cell.height)];
        tempModuleView.backgroundColor = [APPSTYLE colorForType:@"New_Purple_Color"];//old color Purple_Main_Color
        
        [moduleView addSubview:tempModuleView];
        
        UILabel *moduleLabel = [[UILabel alloc]initWithFrame:CGRectMake(-20, curentYPosition+45, 80, 23)];
        moduleLabel.center = tempModuleView.center;
        [moduleLabel setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
        moduleLabel.textColor = [UIColor whiteColor];
        moduleLabel.backgroundColor = [UIColor clearColor];
        moduleLabel.text = [NSString stringWithFormat:@"Módulo %d",i+1];
        [moduleView addSubview:moduleLabel];
        
        curentYPosition = curentYPosition + tempModuleView.frame.size.height + 5;
    }
}

- (void)clearModuleView {
    
    for (UIView *subview in moduleView.subviews) {
        
        [subview removeFromSuperview];
    }
}

#pragma mark - TableView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 5.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 91.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.arrayDataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    Section *currentSection = (Section *)[self.arrayDataSource objectAtIndex:section];

    return [currentSection.unitsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *strCell = @"Cell";
    
    ECLessonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCell];
    
    if (cell == nil) {
        
        cell = [ECLessonTableViewCell loadFromNib];
    }

    Section *currentSection = (Section *)[self.arrayDataSource objectAtIndex:indexPath.section];
    Unit *currentLesson = (Unit *)[currentSection.unitsArray objectAtIndex:indexPath.row];    
    
    double statisticValue = [PROGRESS_MANAGER getLessonProgression:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
    
//    NSLog(@"Progression za index row - (%ld %ld) - %f", indexPath.section, indexPath.row, statisticValue);
    
    ProgressCircleView *progressCircleView = (ProgressCircleView *)[cell.contentView viewWithTag:1];
    
    if (progressCircleView == nil) {
        
        [progressCircleView setIsProfileProgressCircle:NO];
        [progressCircleView drawGUI];

    }
    
    [progressCircleView changeProgress:statisticValue];
    [progressCircleView.imgBackground setImageFromDocumentsResourceFile:currentLesson.touchedImage];
   
    if(![USER_MANAGER isGuest]) {
        
        if (isChanged) {
            
            if (indexPath.section < sectionCompare || (indexPath.section == sectionCompare && indexPath.row <= unitCompare)) {
                
                //allowed
                [progressCircleView changeDictProgressColors: @{
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[APPSTYLE colorForType:@"LessonProgressFirstColor"],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[APPSTYLE colorForType:@"Purple_Main_Color"]
                                                                }];
                
            }
            else {
                
                //not allowed
                [progressCircleView changeDictProgressColors: @{    NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor lightGrayColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor lightGrayColor]
                                                                    }];
                [progressCircleView.imgBackground setImageFromDocumentsResourceFile:currentLesson.untouchedImage];
            }
        }
        else {
            
            if (indexPath.section < maxModuleIndex || (indexPath.section == maxModuleIndex && indexPath.row <= maxUnitIndex)) {
                
                //allowed
                [progressCircleView changeDictProgressColors: @{
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[APPSTYLE colorForType:@"LessonProgressFirstColor"],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[APPSTYLE colorForType:@"Purple_Main_Color"]
                                                                }];
                
            }
            else {
                
                //not allowed
                [progressCircleView changeDictProgressColors: @{    NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor lightGrayColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor lightGrayColor]
                                                                    }];
                [progressCircleView.imgBackground setImageFromDocumentsResourceFile:currentLesson.untouchedImage];
            }
        }
        
        
    
    }
    else {
        
        if (isChanged) {
            
            
            if ((indexPath.section < sectionCompare || (indexPath.section == sectionCompare && indexPath.row <= unitCompare))) {
                
                //allowed
                [progressCircleView changeDictProgressColors: @{
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[APPSTYLE colorForType:@"LessonProgressFirstColor"],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[APPSTYLE colorForType:@"Purple_Main_Color"]
                                                                }];
                
            }
            else {
                
                //not allowed
                [progressCircleView changeDictProgressColors: @{    NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor lightGrayColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor lightGrayColor]
                                                                    }];
                [progressCircleView.imgBackground setImageFromDocumentsResourceFile:currentLesson.untouchedImage];
            }
        }
        else {
            
            if ((indexPath.section < maxModuleIndex || (indexPath.section == maxModuleIndex && indexPath.row <= maxUnitIndex))) {
                
                //allowed
                [progressCircleView changeDictProgressColors: @{
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[APPSTYLE colorForType:@"LessonProgressFirstColor"],
                                                                NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[APPSTYLE colorForType:@"Purple_Main_Color"]
                                                                }];
                
            }
            else {
                
                //not allowed
                [progressCircleView changeDictProgressColors: @{    NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor lightGrayColor],
                                                                    NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor lightGrayColor]
                                                                    }];
                [progressCircleView.imgBackground setImageFromDocumentsResourceFile:currentLesson.untouchedImage];
            }
        }
        
        
    }

    cell.lblLevelName.frame = CGRectMake(100, 35, 146, 21);
    [cell.lblLevelName setNumberOfLines:0];
    cell.lblLevelName.text = [NSString stringWithFormat:@"%ld - %@", (long)[self getTheOrderOfLessonInTheLevel:indexPath.section unit:indexPath.row+1], currentLesson.name];
    [cell.lblLevelName sizeToFit];


    BOOL isLessonCompleted = [PROGRESS_MANAGER isLessonCompleted:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
    UIImageView *imgLessonStatus = (UIImageView *)[cell viewWithTag:3];
    
    if (isLessonCompleted) {
        
        imgLessonStatus.image = [UIImage imageNamed:@"ic_star"];
        cell.lblLevelName.textColor = [UIColor grayColor];
    }
    else {
        
        imgLessonStatus.image = [UIImage imageNamed:@"ic_star2"];
        cell.lblLevelName.textColor = [UIColor lightGrayColor];
    }
    
    if (cell.selectedBackgroundView.tag != 1) {
        
        UIView *customColorView = [UIView new];
        customColorView.tag = 1;
        customColorView.backgroundColor = [APPSTYLE colorForType:@"Lesson_Selection_Color"];
        cell.selectedBackgroundView =  customColorView;

    }
    
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(![USER_MANAGER isGuest]) {
        
        if (isChanged) {
            
            if (indexPath.section < sectionCompare || (indexPath.section == sectionCompare && indexPath.row <= unitCompare)) {
                
                //allowed
                Section *currentSection = (Section *)[self.arrayDataSource objectAtIndex:indexPath.section];
                Unit *currentLesson = (Unit *)[currentSection.unitsArray objectAtIndex:indexPath.row];
                
                LevelData *level       = DATA_SERVICE.dataModel.courseData.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
                Section *section       = level.sectionsArray[indexPath.section];
                Unit *unit             = section.unitsArray[indexPath.row];
                BaseInstance *instance = [unit.instancesArray lastObject];
                
                //    NSArray *dataModel = DATA_SERVICE.dataModel.courseData.levelsArray;
                //    UserStatistic *userstat = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
                
                //// if in the same module check last lesson visited +1 or >
                /// if different module
                
                
                BOOL isNextLessonVisited = [PROGRESS_MANAGER isUnitVisited:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]] section:indexPath.section unit:indexPath.row];
                
                
                if (isNextLessonVisited || instance.isInitial) {
                    
                    
                }
                else {
                    
                    if([USER_MANAGER isActive] && [USER_MANAGER canOpenLesson])
                    {
                        
                        NSString *unitIdString = [NSString stringWithFormat:@"%ld",currentLesson.unitID];
                        [API_REQUEST_MANAGER updateProgressForUser:[USER_MANAGER userMsisdnString]
                                                           groupID:[REQUEST_MANAGER groupID]
                                                         contentID:[REQUEST_MANAGER contentID]
                                                            unitID:unitIdString
                                                         unitScore:@"0"
                                                       unitAchived:@"false"
                                                          progress:@"0"
                                                   CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
                         {
                             if(responseDictionary && [responseDictionary objectForKey:kApiStatus]  && [API_REQUEST_MANAGER checkApiResponseStatus:[[responseDictionary objectForKey:kApiStatus] integerValue]])
                             {
                                 [USER_MANAGER setNumberOfCredits:[[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue]];
                             }
                             else
                             {
                                 /*if([ReachabilityHelper reachable]) {
                                  [UIAlertView alertWithCause:kAlertServerUnreachable];
                                  }
                                  else {
                                  [UIAlertView alertWithCause:kAlertNoConnection];
                                  }*/
                             }
                         }
                         ];
                    }
                    else
                    {
                        if(![USER_MANAGER canOpenLesson])
                            
                            if(!isNextLessonVisited || !instance.isInitial)
                            {
                                [UIAlertView alertWithCause:kAlertNoMoreCreditsForActiveUser];
                                return;
                            }
                        
                    }
                }
                
                if ([PROGRESS_MANAGER currentSectionIndex] != indexPath.section || [PROGRESS_MANAGER currentUnitIndex] != indexPath.row) {
                    
                    
                    [PROGRESS_MANAGER resetInstanceValues:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                    
                }
                
                NSArray *arrayInstances = [currentLesson instancesArray];
                
                if (!arrayInstances || [arrayInstances count] < currentLesson.numberOfInstances) {
                    
                    if (!arrayInstances) {
                        [SimplePingHelper ping:@"216.58.209.164" target:self sel:@selector(pingResult:)];
                        
                    } else if ([arrayInstances count] < currentLesson.numberOfInstances) {
                        if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                            [UIAlertView alertWithCause:kAlertOutOfStorage];
                        } else {
                            [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                        }
                    }
                    
                }
                else {
                    
                    LevelData *level       = DATA_SERVICE.dataModel.courseData.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
                    Section *section       = level.sectionsArray[indexPath.section];
                    Unit *unit             = section.unitsArray[indexPath.row];
                    BaseInstance *instance = [unit.instancesArray lastObject];
                    
                    DLog(@"CLICK CHECK -> UNIT ID:%d NEED INSTNACES:%d DOWNLOADED INSTNACES:%d FROM REQUESTED:%d", unit.unitID, unit.numberOfInstances, [unit.instancesArray count], [unit.instancesInfosArray count]);
                    
                    if (![PROGRESS_MANAGER isUnitAvailable:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row] && !instance.isInitial) {
                        
                        [PROGRESS_MANAGER unitInstancesDownloadCompleted:[PROGRESS_MANAGER currentLevelIndex] sectionIndex:indexPath.section unitIndex:indexPath.row];
                        
                        if (![PROGRESS_MANAGER isUnitAvailable:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row] && !instance.isInitial) {
                            if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                                [UIAlertView alertWithCause:kAlertOutOfStorage];
                            } else {
                                [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                            }
                            return;
                        }
                        
                    }
                    
                    [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.section];
                    [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.row];
                    
                    [APP_DELEGATE buildLessonStack:self.arrayDataSource withLevel:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                    
                    ///new save current moduleIndex and unitIndex
                    
                    [PROGRESS_MANAGER saveCurrentModulIndex:indexPath.section forUser:[USER_MANAGER userMsisdn]];
                    [PROGRESS_MANAGER saveCurrentUnitIndex:indexPath.row forUser:[USER_MANAGER userMsisdn]];
                }
                
                
                
            }
            else {
                
                //not allowed
                [UIAlertView alertWithCause:kAlertLockedLesson];
                
            }
        }
        else {
            
            if (indexPath.section < maxModuleIndex || (indexPath.section == maxModuleIndex && indexPath.row <= maxUnitIndex)) {
                
                //allowed
                Section *currentSection = (Section *)[self.arrayDataSource objectAtIndex:indexPath.section];
                Unit *currentLesson = (Unit *)[currentSection.unitsArray objectAtIndex:indexPath.row];
                
                LevelData *level       = DATA_SERVICE.dataModel.courseData.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
                Section *section       = level.sectionsArray[indexPath.section];
                Unit *unit             = section.unitsArray[indexPath.row];
                BaseInstance *instance = [unit.instancesArray lastObject];
                
                //    NSArray *dataModel = DATA_SERVICE.dataModel.courseData.levelsArray;
                //    UserStatistic *userstat = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
                
                //// if in the same module check last lesson visited +1 or >
                /// if different module
                
                
                BOOL isNextLessonVisited = [PROGRESS_MANAGER isUnitVisited:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]] section:indexPath.section unit:indexPath.row];
                
                
                if (isNextLessonVisited || instance.isInitial) {
                    
                    
                }
                else {
                    
                    if([USER_MANAGER isActive] && [USER_MANAGER canOpenLesson])
                    {
                        
                        NSString *unitIdString = [NSString stringWithFormat:@"%ld",currentLesson.unitID];
                        [API_REQUEST_MANAGER updateProgressForUser:[USER_MANAGER userMsisdnString]
                                                           groupID:[REQUEST_MANAGER groupID]
                                                         contentID:[REQUEST_MANAGER contentID]
                                                            unitID:unitIdString
                                                         unitScore:@"0"
                                                       unitAchived:@"false"
                                                          progress:@"0"
                                                   CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
                         {
                             if(responseDictionary && [responseDictionary objectForKey:kApiStatus]  && [API_REQUEST_MANAGER checkApiResponseStatus:[[responseDictionary objectForKey:kApiStatus] integerValue]])
                             {
                                 [USER_MANAGER setNumberOfCredits:[[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue]];
                             }
                             else
                             {
                                 /*if([ReachabilityHelper reachable]) {
                                  [UIAlertView alertWithCause:kAlertServerUnreachable];
                                  }
                                  else {
                                  [UIAlertView alertWithCause:kAlertNoConnection];
                                  }*/
                             }
                         }
                         ];
                    }
                    else
                    {
                        if(![USER_MANAGER canOpenLesson])
                            
                            if(!isNextLessonVisited || !instance.isInitial)
                            {
                                [UIAlertView alertWithCause:kAlertNoMoreCreditsForActiveUser];
                                return;
                            }
                        
                    }
                }
                
                if ([PROGRESS_MANAGER currentSectionIndex] != indexPath.section || [PROGRESS_MANAGER currentUnitIndex] != indexPath.row) {
                    
                    
                    [PROGRESS_MANAGER resetInstanceValues:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                    
                }
                
                NSArray *arrayInstances = [currentLesson instancesArray];
                
                if (!arrayInstances || [arrayInstances count] < currentLesson.numberOfInstances) {
                    
                    if (!arrayInstances) {
                        [SimplePingHelper ping:@"216.58.209.164" target:self sel:@selector(pingResult:)];
                        
                    } else if ([arrayInstances count] < currentLesson.numberOfInstances) {
                        
                        if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                            [UIAlertView alertWithCause:kAlertOutOfStorage];
                        } else {
                            [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                        }
                        
                    }
                    
                }
                else {
                    
                    LevelData *level       = DATA_SERVICE.dataModel.courseData.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
                    Section *section       = level.sectionsArray[indexPath.section];
                    Unit *unit             = section.unitsArray[indexPath.row];
                    BaseInstance *instance = [unit.instancesArray lastObject];
                    
                    DLog(@"CLICK CHECK -> UNIT ID:%d NEED INSTNACES:%d DOWNLOADED INSTNACES:%d FROM REQUESTED:%d", unit.unitID, unit.numberOfInstances, [unit.instancesArray count], [unit.instancesInfosArray count]);
                    
                    if (![PROGRESS_MANAGER isUnitAvailable:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row] && !instance.isInitial) {
                        
                        [PROGRESS_MANAGER unitInstancesDownloadCompleted:[PROGRESS_MANAGER currentLevelIndex] sectionIndex:indexPath.section unitIndex:indexPath.row];
                        
                        if (![PROGRESS_MANAGER isUnitAvailable:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row] && !instance.isInitial) {
                            
                            if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                                [UIAlertView alertWithCause:kAlertOutOfStorage];
                            } else {
                                [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                            }
                            return;
                        }
                        
                    }
                    
                    [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.section];
                    [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.row];
                    
                    [APP_DELEGATE buildLessonStack:self.arrayDataSource withLevel:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                    
                    ///new save current moduleIndex and unitIndex
                    
                    [PROGRESS_MANAGER saveCurrentModulIndex:indexPath.section forUser:[USER_MANAGER userMsisdn]];
                    [PROGRESS_MANAGER saveCurrentUnitIndex:indexPath.row forUser:[USER_MANAGER userMsisdn]];
                }
                
                
                
            }
            else {
                
                //not allowed
                [UIAlertView alertWithCause:kAlertLockedLesson];
                
            }
        }
        
        
        
    }
    else {
        
//        NSLog(@"Index - %ld %ld", indexPath.section, indexPath.row);
        
        if (indexPath.section > moduleAvailForGuest || (indexPath.section == moduleAvailForGuest && indexPath.row > lessonAvailForGuest)) {
            
            UIAlertView *tempAlert = [UIAlertView alertViewWithTitle:@""
                                                             message:Localized(@"T462")
                                                   cancelButtonTitle:Localized(@"T463")
                                                   otherButtonTitles:@[Localized(@"T511")]
                                                           onDismiss: ^(int buttonIndex) {
                                                               if (buttonIndex == -1){
                                                                   
                                                               }else if (buttonIndex == 0){
                                                                   //[APP_DELEGATE buildLoginStack];
                                                               }
                                                           }
                                                            onCancel: ^{
                                                                [APP_DELEGATE presentLoginStack];
                                                            }];
            [tempAlert show];
            
            
        }
        else {
            
            if (isChanged) {
                
                if (indexPath.section < sectionCompare || (indexPath.section == sectionCompare && indexPath.row <= unitCompare)) {
                    
                    if ([PROGRESS_MANAGER currentSectionIndex] != indexPath.section || [PROGRESS_MANAGER currentUnitIndex] != indexPath.row) {
                        
                        
                        [PROGRESS_MANAGER resetInstanceValues:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                        
                    }
                    
                    [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.section];
                    [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.row];
                    
                    [APP_DELEGATE buildLessonStack:self.arrayDataSource withLevel:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                    
                    [PROGRESS_MANAGER saveCurrentModulIndex:indexPath.section forUser:[USER_MANAGER userMsisdn]];
                    [PROGRESS_MANAGER saveCurrentUnitIndex:indexPath.row forUser:[USER_MANAGER userMsisdn]];
                    
                }
                else {
                    
                    [UIAlertView alertWithCause:kAlertLockedLesson];
                    
                    
                }
            }
            else {
                
                if (indexPath.section < maxModuleIndex || (indexPath.section == maxModuleIndex && indexPath.row <= maxUnitIndex)) {
                    
                    if ([PROGRESS_MANAGER currentSectionIndex] != indexPath.section || [PROGRESS_MANAGER currentUnitIndex] != indexPath.row) {
                        
                        
                        [PROGRESS_MANAGER resetInstanceValues:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                        
                    }
                    
                    [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.section];
                    [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex] index:indexPath.row];
                    
                    [APP_DELEGATE buildLessonStack:self.arrayDataSource withLevel:[PROGRESS_MANAGER currentLevelIndex] section:indexPath.section unit:indexPath.row];
                    
                    [PROGRESS_MANAGER saveCurrentModulIndex:indexPath.section forUser:[USER_MANAGER userMsisdn]];
                    [PROGRESS_MANAGER saveCurrentUnitIndex:indexPath.row forUser:[USER_MANAGER userMsisdn]];
                    
                }
                else {
                    
                    [UIAlertView alertWithCause:kAlertLockedLesson];
                    
                    
                }
            }
            
        }
        
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 5)];
    [viewHeader setBackgroundColor:[UIColor whiteColor]];
    
    return viewHeader;
    
}





#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  static CGFloat previousOffset;
  CGRect rect = moduleView.frame;
  rect.origin.y += previousOffset - scrollView.contentOffset.y;
  previousOffset = scrollView.contentOffset.y;
  moduleView.frame = rect;
}


#pragma mark - Orientation

-(BOOL)shouldAutorotate
{
    
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark - SyncServiceDelegate implementation

- (void)refreshUnitsData
{
    self.arrayDataSource = nil;
    self.arrayDataSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:[PROGRESS_MANAGER currentLevelIndex]];
    
    if([USER_MANAGER isGuest]) {
        
        NSIndexPath *maxAvailForGuest = [self findMaxValuesForGuest:[LMHelper getNumberOfGestCredits] arrayLessons:self.arrayDataSource];
        moduleAvailForGuest = maxAvailForGuest.section;
        lessonAvailForGuest = maxAvailForGuest.row;
    }

}

#pragma mark -
#pragma mark - get first lesson number

- (NSInteger)getTheNumberOfFirstLessonForLevel:(NSInteger)levelIndex {
    
    NSArray *arrayLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
    
    LevelData *level;
    
    NSInteger startNumber = 0;
    
    for (int i=0; i<levelIndex; i++) {
        
        level = [arrayLevels objectAtIndex:i];
        
        for (Section *section in [(LevelData *)level sectionsArray]) {
            
            startNumber+=section.unitsArray.count;
        }
    }
    
    return startNumber;
}


#pragma mark -
#pragma mark - Help method to return number of lesson

- (NSInteger)getTheOrderOfLessonInTheLevel:(NSInteger)section unit:(NSInteger)unit {
    
    if (section == 0) {
        
        return unit+[self getTheNumberOfFirstLessonForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];

    }
    
    NSInteger lessonOrderIndex = 0;
    Section *currentSection;
    
    for (int i=0; i<section; i++) {
        
        currentSection = (Section *)[self.arrayDataSource objectAtIndex:i];
        lessonOrderIndex += [currentSection.unitsArray count];
    }
    
    lessonOrderIndex+=unit;
    
    lessonOrderIndex+=[self getTheNumberOfFirstLessonForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
    return lessonOrderIndex;
}
#pragma mark SimplePingHelper
- (void)pingResult:(NSNumber*)success {
  /*
   if ([ReachabilityHelper reachable]) {
   
   } else {
   
   }
   */
  if (success.boolValue) {
      if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
          [UIAlertView alertWithCause:kAlertOutOfStorage];
      } else {
          [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
      }
  } else {
    [UIAlertView alertWithCause:kAlertNoConnection];
  }
}


#pragma mark - Convert integer to indexPath

- (NSIndexPath *)findMaxValuesForGuest:(NSInteger)maxAvail arrayLessons:(NSArray *)arrayLessons {
    
    NSInteger tempValue = kParam001;
    
    for (int i=0; i<arrayLessons.count; i++) {
        
        Section *currentSection = (Section *)[arrayLessons objectAtIndex:i];
        
        if (tempValue <= currentSection.unitsArray.count) {
            
            return [NSIndexPath indexPathForRow:tempValue-1 inSection:i];
        }
        
        tempValue-=currentSection.unitsArray.count;
        
    }

    return [NSIndexPath indexPathForRow:0 inSection:0];
}

@end
